FROM php:7.2-apache
WORKDIR /var/www/html/
RUN a2enmod rewrite
COPY source/ /var/www/html/
#ENTRYPOINT ["php","-q","index.php"]
