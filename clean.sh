# remove joe backup files
find . -type f -name "*~" -exec rm {} \;

git add source/application/core/
git add source/application/config/*.php
git add source/application/controllers/*.php
git add source/application/libraries/
git add source/application/models/
git add source/application/views/themes/sensorbucket/*.php
git add source/application/views/themes/sensorbucket/content/*
git add source/application/views/themes/sensorbucket/layout/*
git add source/assets/css/*.css
git add source/assets/javascript/*.js
