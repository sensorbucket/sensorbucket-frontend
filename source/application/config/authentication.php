<?php
  
  $config["auth"]          = array("page_template" => "inspinia_login", "types" => array("admin"));  
  $config["auth"]["admin"] = array("title" => "Provincie Zeeland - Beheer sensorbucket", "realm" => "<strong>Sensorbucket beheer</strong>", "template" => "login_inspinia_form"); // login_form

  $ipList               = array();
  $config["ip_access"]  = $ipList;
  
