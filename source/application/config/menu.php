<?php

  $menu            = array();
  /*
  $menu["kaarten"] = array("title" => "Kaarten", 
                           "icon"  => "fa fa-th-large", 
                           "items" => array("stdmaps"   => array("title" => "Standaard kaarten", "url" => "/admin/"),
                                            "storymaps" => array("title" => "Storymaps", "url" => "/admin/storymaps")));
                                            
  $menu["config"]  = array("title" => "Configuratie",
                           "icon"  => "fa fa-cogs",
                           "items" => array("users"       => array("title" => "Frontend gebruikers", "url" => "/admin/users"),
                                            "frameworks"  => array("title" => "Frameworks", "url" => "/admin/frameworks"),
                                            "servers"     => array("title" => "WMS servers", "url" => "/admin/servers"),
                                            "baselayers"  => array("title" => "Ondergronden", "url" => "/admin/baselayers")));                                            
  */

  $menu["dashboard"]       = array("title" => "Home",
                                   "icon"  => "fa fa-home",
                                   "href"  => "/dashboard/"); 

  $menu["organisation"]    = array("title" => "Organisaties",
                                   "icon"  => "fa fa-sitemap",
                                   "href"  => "/organisations/"); 

  $menu["locations"]       = array("title" => "Locaties",
                                   "icon"  => "fa fa-globe",
                                   "href"  => "/locations/");                                    

  $menu["sources"]         = array("title" => "Bronnen",
                                   "icon"  => "fa fa-server",
                                   "href"  => "/sources/");                                    
                                   
  $menu["devices"]         = array("title" => "Sensoren",
                                   "icon"  => "fa fa-gear",
                                   "href"  => "/devices/");                                    
  
  $config["nav"] = array("logo"  => "https://www.zeeland.nl/sites/zl-zeeland/themes/zeeland/images/logo-zeeland-kleur.svg",
                         "items" => $menu);
