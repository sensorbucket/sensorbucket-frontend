<?php

 /**
  *
  *  For each frontend domain create an entry with the hostname as key in $config["sites"]
  *  with the following values:
  *
  *  alias                  Name of the theme to use
  *  path                   Path under the views / assets folders for this theme
  *  drupal_site            Name of directory under the Drupal sites folder containing the backend code
  *  default_page           Name of the wombat page to fetch if we have no url
  *
  *
  *
  *
  *  $config["sites"]["<domain>"] = array("alias"        => "<themename>",
  *                                       "path"         => "themes/<themename>/",
  *                                       "drupal_site"  => "<dirname>",
  *                                       "default_page" => "home");
  *
  */
  
  $_sites = array("sensorbucket");
  
  foreach ($_sites AS $_site)
  {
    $config["sites"][$_site] = array("alias"       => "sensorbucket",
                                     "path"        => "themes/sensorbucket/",
                                     "drupal_site" => "layout");   
  }
  