<?php

  class Auth extends CI_Controller
  {
    private $m_authConfig;

    public function __construct()
    {
      parent::__construct();
      require_once(APPPATH.'/libraries/autoload.php');
      require_once(APPPATH.'/libraries/sensorbucket/class.authentication.inc');

      $this->config->load("authentication");
      $this->m_authConfig = $this->config->item('auth');
    }


    public function index()
    {
      echo "Fout in configuratie";
      exit;
    }


    public function login($type="")
    {
      // Check if type is valid (either admin or map)
      if (trim($type) === "" || !isset($this->m_authConfig[$type]))
      {
        echo "Fout in configuratie";
        exit;
      }
      else
      {
        $realm = $this->m_authConfig[$type]["realm"];
      }

      // Get page / meta instances
      $page = page::getInstance();
      $meta = Page_Meta::getInstance();

      // Set page template
      $page->setPageTemplate($this->m_authConfig["page_template"]);

      // Set title
      $title = $this->m_authConfig[$type]["title"];

      // Disable minifying
      $page->setMinify(false);

      // Standard metadata
      $meta->addItem("generator", "Wombat framework v1.25");
      $meta->addItem("author", "Wim Kosten / Provincie Zeeland");
      $meta->setTitle($title);
      $meta->setKeywords("");
      $meta->setDescription("");
      $meta->setRobots("noindex", "nofollow");

      // Assign title
      $page->addRegionContent("title", $title);

      // Set content
      $content = $this->load->themeContentView($this->m_authConfig[$type]["template"], array("title" => $title, "realm" => $realm, "item" => $type), true);

      // Render the page
      $page->addMetaData();
      $page->output($content);
    }


    public function validate()
    {
      $post = $this->input->post();

      if (!isset($post["item"]))
      {
        echo "Fout in configuratie";
        exit;
      }
      else
      {
        // TODO: authenticate / autorisation
        $post = $this->input->post();

        if (Authentication::validateCredentials($post["username"], $post["pass"]) === true)
        {
          header("location:/dashboard"); 
        }
        else
        {
          header("Location: /auth/login/admin");
        }

        exit;        
      }  

    }


    public function logout($type, $map="")
    {
     // require_once(APPPATH."libraries/authentication/class.authentication.inc");
     // Authentication::signOff($type, $map);

     if (isset($_SESSION["sensorbucket"]))
      {
        unset($_SESSION["sensorbucket"]);
      }

      header("location: /auth/login/admin");
      exit;
    }

  }