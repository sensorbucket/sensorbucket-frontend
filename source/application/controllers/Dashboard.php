<?php

  defined('BASEPATH') OR exit('No direct script access allowed');
  define('DISPLAY_DEBUG', false);

  // https://www.jqueryscript.net/table/jQuery-Plugin-For-Sortable-Orderable-Table-VanderTable.html

 /**
  *  Admin controller
  *
  *  Handle all admin actions
  *
  *  It extends from the OL_Admin_Controller, each controller which needs authentication should extend this OL_Admin_Controller
  *
  *  @author    Wim Kosten <w.kosten@zeeland.nl>
  *  @copyright Provincie Zeeland
  *
  */
  class Dashboard extends Sensorbucket_Admin_Controller
  {

   /**
    *  Public constructor
    *
    *  Init parent constructor and load needed files
    */
    public function __construct()
    {
      parent::__construct();
      require_once(APPPATH.'/libraries/autoload.php');
    }


   /**
    *  index method
    *
    *  Show an overview of defined maps, add/edit/delete maps, update layers etc.
    *
    *  @param void
    *  @return void
    *
    */
    public function index()
    {
      $this->initPage("Dashboard", "");
   
      $page    = page::getInstance();
      $content = $this->load->themeContentView("sensorbucket_dashboard", array("items" => array()), true);

      $page->setBreadCrumb(array("Home" => array("icon" => "home")));
      $page->addMetaData();
      $page->output($content);
    }




  }