<?php

  defined('BASEPATH') OR exit('No direct script access allowed');
  define('DISPLAY_DEBUG', false);

 /**
  *  Devices controller
  *
  *  Handle all device based actions
  *
  *  @author    Wim Kosten <w.kosten@zeeland.nl>
  *  @copyright Provincie Zeeland
  *
  */
  class Devices extends Sensorbucket_Admin_Controller
  {

   /**
    *  Public constructor
    *
    *  Init parent constructor and load needed files
    */
    public function __construct()
    {
      parent::__construct();
    }


   /**
    *  Default kitchensink method
    * 
    *  @param void 
    *  @return void
    * 
    */ 
    public function index()
    {
      $this->overview();
    }


   /**
    *  overview
    *
    *  Show an overview of known devices (sensors)
    *
    *  @param void
    *  @return void
    *
    *  TODO: check if user is allowed and should we filter on the organisation(s) this user belongs to ?
    */
    public function overview()
    {
      $this->initPage("Sensoren", "");
      $page    = page::getInstance();
      $data    = $this->sensorbucket->getDevices();
      $content = $this->load->themeContentView("sensorbucket_devices_overview", array("items" => $data, ), true);

      $page->addPageAction("Nieuwe sensor", "javascript:loadExtendedDataIntoModal(\"/devices/add\");", "btn-primary");
      $page->addMetaData();
      $page->output($content);
    }


   /**
    *  view 
    *
    *  View device configuration
    *
    *  @param integer $deviceID
    *  @return void
    *
    *  TODO: check if user is allowed !!
    */ 
    public function view($deviceID)
    {
      $buttons = array("<button type='button' class='btn btn-secondary' data-dismiss='modal'>Sluiten</button>");
      $data    = $this->sensorbucket->getDevice($deviceID);
      $content = "Geen gegevens beschikbaar";

      if (isset($data->name))
      {
        $title   = "Bekijk device ".$data->name;
        $content = $this->load->themeContentView("sensorbucket_devices_view", array("data" => $data, ), true);
      }
      else
      {
        $title = "Fout, onbekend device";
      }

      echo Admin_Tools::renderDynamicModal($title, $content, $buttons);
    }


   /**
    *  add
    *
    *  Add a new device
    *
    *  @param void
    *  @return void
    *
    *  TODO: check if user is allowed !!
    */    
    public function add()
    {
      $buttons = array("<button type='submit' class='btn btn-primary'>Opslaan</button>",
                       "<button type='button' class='btn btn-secondary' data-dismiss='modal'>Sluiten</button>");

      $metadata["locations"] = $this->sensorbucket->getLocations();
      $metadata["sources"]   = $this->sensorbucket->getSources();
      $metadata["dev_types"] = $this->sensorbucket->getDeviceTypes();
      $title                 = "Sensor toevoegen";
      $content               = $this->load->themeContentView("sensorbucket_devices_add", array("meta" => $metadata), true);

      echo Admin_Tools::renderDynamicModal($title, $content, $buttons);
    }


   /**
    *  edit
    *
    *  Edit device configuration
    *
    *  @param integer $deviceID
    *  @return void
    *
    *  TODO: check if user is allowed !!
    */    
    public function edit($deviceID)
    {
      $buttons = array("<button type='submit' class='btn btn-primary'>Opslaan</button>",
                       "<button type='button' class='btn btn-secondary' data-dismiss='modal'>Sluiten</button>");

      $data    = $this->sensorbucket->getDevice($deviceID);
      $content = "Geen gegevens beschikbaar";

      if (isset($data->name))
      {
        $metadata              = array();
        $metadata["locations"] = $this->sensorbucket->getLocations();
        $metadata["sources"]   = $this->sensorbucket->getSources();
        $metadata["dev_types"] = $this->sensorbucket->getDeviceTypes();

        $title   = "Bewerk device ".$data->name;
        $content = $this->load->themeContentView("sensorbucket_devices_edit", array("data" => $data, "meta" => $metadata), true);
      }
      else
      {
        $title   = "Fout, onbekend device";
        $buttons = array("<button type='button' class='btn btn-secondary' data-dismiss='modal'>Sluiten</button>");
      }

      echo Admin_Tools::renderDynamicModal($title, $content, $buttons);
    }


   /**
    *  delete
    *
    *  Delete device configuration
    *
    *  @param integer $deviceID
    *  @return void
    *
    *  TODO: check if user is allowed !!
    */    
    public function delete($deviceID)
    {
      $data = $this->sensorbucket->deleteDevice($deviceID);
      header("Location:/devices/overview");
      exit;
    }


  }