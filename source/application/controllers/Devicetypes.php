<?php

  defined('BASEPATH') OR exit('No direct script access allowed');
  define('DISPLAY_DEBUG', false);

 /**
  *  Devicetypes controller
  *
  *  Handle all devicetype based actions
  *
  *  @author    Wim Kosten <w.kosten@zeeland.nl>
  *  @copyright Provincie Zeeland
  *
  */
  class Devicetypes extends Sensorbucket_Admin_Controller
  {

   /**
    *  Public constructor
    *
    *  Init parent constructor and load needed files
    */
    public function __construct()
    {
      parent::__construct();
    }


   /**
    *  Default kitchensink method
    * 
    *  @param void 
    *  @return void
    * 
    */ 
    public function index()
    {
      $this->overview();
    }


   /**
    *  overview
    *
    *  Show an overview of known devicetypes (sensors)
    *
    *  @param void
    *  @return void
    *
    *  TODO: check if user is allowed and should we filter on the organisation(s) this user belongs to ?
    */
    public function overview()
    {
      $this->initPage("Sensorsoorten", "");
      $page    = page::getInstance();
      $data    = $this->sensorbucket->getDeviceTypes();
      $content = $this->load->themeContentView("sensorbucket_devicetypes_overview", array("items" => $data, ), true);

      $page->addPageAction("Nieuwe sensor", "javascript:loadExtendedDataIntoModal(\"/devicetypes/add\");", "btn-primary");
      $page->addMetaData();
      $page->output($content);
    }


   /**
    *  view 
    *
    *  View devicetype configuration
    *
    *  @param integer $deviceTypeID
    *  @return void
    *
    *  TODO: check if user is allowed !!
    */ 
    public function view($deviceTypeID)
    {
      $buttons = array("<button type='button' class='btn btn-secondary' data-dismiss='modal'>Sluiten</button>");
      $data    = $this->sensorbucket->getDeviceType($deviceTypeID);
      $content = "Geen gegevens beschikbaar";

      if (isset($data->name))
      {
        $title   = "Bekijk sensorsoort ".$data->name;
        $content = $this->load->themeContentView("sensorbucket_devicetypes_view", array("data" => $data, ), true);
      }
      else
      {
        $title = "Fout, onbekende sensorsoort";
      }

      echo Admin_Tools::renderDynamicModal($title, $content, $buttons);
    }


   /**
    *  add
    *
    *  Add a new devicetype
    *
    *  @param void
    *  @return void
    *
    *  TODO: check if user is allowed !!
    */    
    public function add()
    {
      $buttons = array("<button type='submit' class='btn btn-primary'>Opslaan</button>",
                       "<button type='button' class='btn btn-secondary' data-dismiss='modal'>Sluiten</button>");
      $title   = "Sensorsoort toevoegen";
      $content = $this->load->themeContentView("sensorbucket_devicetypes_add", array(), true);

      echo Admin_Tools::renderDynamicModal($title, $content, $buttons);
    }


   /**
    *  edit
    *
    *  Edit devicetype configuration
    *
    *  @param integer $deviceID
    *  @return void
    *
    *  TODO: check if user is allowed !!
    */    
    public function edit($deviceID)
    {
      $buttons = array("<button type='submit' class='btn btn-primary'>Opslaan</button>",
                       "<button type='button' class='btn btn-secondary' data-dismiss='modal'>Sluiten</button>");

      $data    = $this->sensorbucket->getDeviceType($deviceID);
      $content = "Geen gegevens beschikbaar";

      if (isset($data->name))
      {
        $title   = "Bewerk sensorsoort ".$data->name;
        $content = $this->load->themeContentView("sensorbucket_devicetypes_edit", array("data" => $data), true);
      }
      else
      {
        $title   = "Fout, onbekende sensorsoort";
        $buttons = array("<button type='button' class='btn btn-secondary' data-dismiss='modal'>Sluiten</button>");
      }

      echo Admin_Tools::renderDynamicModal($title, $content, $buttons);
    }


   /**
    *  delete
    *
    *  Delete devicetype configuration
    *
    *  @param integer $deviceID
    *  @return void
    *
    *  TODO: check if user is allowed !!
    */    
    public function delete($deviceID)
    {
      $data = $this->sensorbucket->deleteDeviceType($deviceID);
      header("Location:/devicetypes/overview");
      exit;
    }


  }