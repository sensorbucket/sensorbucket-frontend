<?php

  defined('BASEPATH') OR exit('No direct script access allowed');
  define('DISPLAY_DEBUG', false);

  /**
   *  Loader controller
   *
   *  Controller for bulkloading (concats to 1 big file) static files like CSS and Javascript files
   *
   *  @author    Wim Kosten <w.kosten@zeeland.nl>
   *  @copyright Provincie Zeeland
   *
   */
   class Loader extends CI_Controller
   {
     /**
      * @var m_fileRoots array Array with file roots for css/js files
      */
      private $m_fileRoots  = array();


     /**
      *  Public constructor
      *
      *  Init parent controller, load needed files and set fileroots depending on the theme used
      *
      *  @param void
      *  @return void
      *
      */
      public function __construct()
      {
        parent::__construct();
        
        @require_once(APPPATH."libraries/layout/class.page_theme.inc");
        @require_once(APPPATH."libraries/layout/class.bulk_loader.inc");

        $themeInfo = Page_Theme::getInstance()->getThemeInfo();
        $siteRoot  = FCPATH;

        $this->m_fileRoots["global_css"] = "{$siteRoot}assets/css/";
        $this->m_fileRoots["theme_css"]  = "{$siteRoot}{$themeInfo["assets_path"]}css/";
        $this->m_fileRoots["global_js"]  = "{$siteRoot}assets/javascript/";
        $this->m_fileRoots["theme_js"]   = "{$siteRoot}{$themeInfo["assets_path"]}javascript/";
      }


     /**
      *  _remap method
      *
      *  Kitchen-sink method (just like magic methods in PHP) for any method requested
      *  Init Bulk_Loader object, set properties, concat files from the params and send code
      *
      *  @example /loader/gcss/admin:bootstrap-min-3.3.7.css/admin:dashboard.css
      *
      *  @param $filetype string Filetype (gcss, gjs, tcss, tjs)
      *  @param array $params Array with passed params
      *  @return void
      *
      */
      public function _remap($filetype, $params=array())
      {
        // Create Bulk loader object and pass filetype and params (requested files)
        $bulkLoader = new Bulk_Loader($filetype, $params);
        
        // Set fileroots for each filetype (gcss, gjs, tcss, tjs)
        $bulkLoader->setFileRootAndTypes($this->m_fileRoots);
        
        // Enable GZIP compression
        $bulkLoader->setGZIP(true);
        
        // Get the code (either from files or from compressed cache version)
        $code = $bulkLoader->getCode();
        
        // Send the code
        $bulkLoader->sendCode($code);
      }
   }
   