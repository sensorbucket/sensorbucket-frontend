<?php

  defined('BASEPATH') OR exit('No direct script access allowed');
  define('DISPLAY_DEBUG', false);

 /**
  *  Locations controller
  *
  *  Handle all location based actions
  *
  *  @author    Wim Kosten <w.kosten@zeeland.nl>
  *  @copyright Provincie Zeeland
  *
  */
  class Locations extends Sensorbucket_Admin_Controller
  {

   /**
    *  Public constructor
    *
    *  Init parent constructor and load needed files
    */
    public function __construct()
    {
      parent::__construct();
    }


   /**
    *  Default kitchensink method
    * 
    *  @param void 
    *  @return void
    * 
    */ 
    public function index()
    {
      $this->overview();
    }


   /**
    *  overview
    *
    *  Show an overview of known locations
    *
    *  @param void
    *  @return void
    *
    *  TODO: check if user is allowed and should we filter on the organisation(s) this user belongs to ?
    */
    public function overview()
    {
      $this->initPage("Locaties", "");
      $page    = page::getInstance();
      $data    = $this->sensorbucket->getLocations();
      $devices = $this->sensorbucket->getDevicesByLocation(); 
      $content = $this->load->themeContentView("sensorbucket_locations_overview", array("items" => $data, "devices" => $devices), true);

      $page->addPageAction("Nieuwe locatie", "javascript:loadExtendedDataIntoModal(\"/locations/add\");", "btn-primary");
      $page->addMetaData();
      $page->output($content);
    }


   /**
    *  view 
    *
    *  View location configuration
    *
    *  @param integer $locationID
    *  @return void
    *
    *  TODO: check if user is allowed !!
    */ 
    public function view($locationID)
    {
      $buttons = array("<button type='button' class='btn btn-secondary' data-dismiss='modal'>Sluiten</button>");
      $data    = $this->sensorbucket->getLocation($locationID);
      $devices = $this->sensorbucket->getDevicesByLocation($locationID); 
      $content = "Geen gegevens beschikbaar";

      if (isset($data->name))
      {
        $title   = "Bekijk locatie ".$data->name;
        $content = $this->load->themeContentView("sensorbucket_locations_view", array("data" => $data, "devices" => $devices), true);
      }
      else
      {
        $title = "Fout, onbekende locatie";
      }

      echo Admin_Tools::renderDynamicModal($title, $content, $buttons);
    }


   /**
    *  view 
    *
    *  View devices for specified location
    *
    *  @param integer $locationID
    *  @return void
    *
    *  TODO: check if user is allowed !!
    */ 
    public function deviceview($locationID)
    {
      $buttons = array("<button type='button' class='btn btn-secondary' data-dismiss='modal'>Sluiten</button>");
      $data    = $this->sensorbucket->getLocation($locationID);
      $devices = $this->sensorbucket->getDevicesByLocation($locationID); 
      $content = "Geen gegevens beschikbaar";

      if (isset($data->name))
      {
        $title   = "Sensoren op locatie ".$data->name;
        $content = $this->load->themeContentView("sensorbucket_locations_devices_view", array("data" => $data, "devices" => $devices), true);
      }
      else
      {
        $title = "Fout, onbekende locatie";
      }

      echo Admin_Tools::renderDynamicModal($title, $content, $buttons);
    }


   /**
    *  add
    *
    *  Add a new location
    *
    *  @param void
    *  @return void
    *
    *  TODO: check if user is allowed !!
    */    
    public function add()
    {
      $buttons = array("<button type='submit' class='btn btn-primary'>Opslaan</button>",
                       "<button type='button' class='btn btn-secondary' data-dismiss='modal'>Sluiten</button>");

      $title   = "Locatie toevoegen";
      $data    = $this->sensorbucket->getLocation(1);
      $content = $this->load->themeContentView("sensorbucket_locations_add", array("data" => $data), true);

      echo Admin_Tools::renderDynamicModal($title, $content, $buttons);
    }


   /**
    *  edit
    *
    *  Edit location configuration
    *
    *  @param integer $locationID
    *  @return void
    *
    *  TODO: check if user is allowed !!
    */    
    public function edit($locationID)
    {
      $buttons = array("<button type='submit' class='btn btn-primary'>Opslaan</button>",
                       "<button type='button' class='btn btn-secondary' data-dismiss='modal'>Sluiten</button>");

      $data    = $this->sensorbucket->getLocation($locationID);
      $content = "Geen gegevens beschikbaar";

      if (isset($data->name))
      {
        $title   = "Bewerk locatie ".$data->name;
        $content = $this->load->themeContentView("sensorbucket_locations_edit", array("data" => $data), true);
      }
      else
      {
        $title   = "Fout, onbekende locatie";
        $buttons = array("<button type='button' class='btn btn-secondary' data-dismiss='modal'>Sluiten</button>");
      }

      echo Admin_Tools::renderDynamicModal($title, $content, $buttons);
    }


   /**
    *  delete
    *
    *  Delete location configuration
    *
    *  @param integer $$locationID
    *  @return void
    *
    *  TODO: check if user is allowed !!
    */    
    public function delete($locationID)
    {
      $data = $this->sensorbucket->deleteLocation($locationID);
      header("Location:/locations/overview");
      exit;
    }


  }