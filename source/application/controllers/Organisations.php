<?php

  defined('BASEPATH') OR exit('No direct script access allowed');
  define('DISPLAY_DEBUG', false);

 /**
  *  Organisation controller
  *
  *  Handle all Organisation related stuff
  *  It extends from the OL_Admin_Controller, each controller which needs authentication should extend this OL_Admin_Controller
  *
  *  @author    Wim Kosten <w.kosten@zeeland.nl>
  *  @copyright Provincie Zeeland
  *
  */
  class Organisations extends Sensorbucket_Admin_Controller
  {

   /**
    *  Public constructor
    *
    *  Init parent constructor and load needed files
    */
    public function __construct()
    {
      parent::__construct();
      require_once(APPPATH.'/libraries/autoload.php');
    }


   /**
    *  index method
    *
    *  Show an overview of known organisations
    *
    *  @param void
    *  @return void
    *
    */
    public function index()
    {
      $this->initPage("Organisaties", "");
   
      $page    = page::getInstance();
      $content = $this->load->themeContentView("sensorbucket_organisations_overview", array("data" => $this->m_API->getOrganisations()), true);

      $page->setBreadCrumb(array("Home" => array("icon" => "home")));
      $page->addMetaData();
      $page->output($content);
    }




  }