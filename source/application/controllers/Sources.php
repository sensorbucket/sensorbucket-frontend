<?php

  defined('BASEPATH') OR exit('No direct script access allowed');
  define('DISPLAY_DEBUG', false);

 /**
  *  Sources controller
  *
  *  Handle all source based actions
  *
  *  @author    Wim Kosten <w.kosten@zeeland.nl>
  *  @copyright Provincie Zeeland
  *
  */
  class Sources extends Sensorbucket_Admin_Controller
  {

   /**
    *  Public constructor
    *
    *  Init parent constructor and load needed files
    */
    public function __construct()
    {
      parent::__construct();
    }


   /**
    *  Default kitchensink method
    * 
    *  @param void 
    *  @return void
    * 
    */ 
    public function index()
    {
      $this->overview();
    }


   /**
    *  overview
    *
    *  Show an overview of known sources
    *
    *  @param void
    *  @return void
    *
    *  TODO: check if user is allowed and should we filter on the organisation(s) this user belongs to ?
    */
    public function overview()
    {
      $this->initPage("Bronnen", "");
      $page    = page::getInstance();
      $data    = $this->sensorbucket->getSources();
      $content = $this->load->themeContentView("sensorbucket_sources_overview", array("items" => $data, ), true);

      $page->addPageAction("Nieuwe bron", "javascript:loadExtendedDataIntoModal(\"/sources/add\");", "btn-primary");
      $page->addMetaData();
      $page->output($content);
    }


   /**
    *  view 
    *
    *  View source configuration
    *
    *  @param integer $dsourceID
    *  @return void
    *
    *  TODO: check if user is allowed !!
    */ 
    public function view($sourceID)
    {
      $buttons = array("<button type='button' class='btn btn-secondary' data-dismiss='modal'>Sluiten</button>");
      $data    = $this->sensorbucket->getSource($sourceID);
      $content = "Geen gegevens beschikbaar";

      if (isset($data->name))
      {
        $title   = "Bekijk bron ".$data->name;
        $content = $this->load->themeContentView("sensorbucket_sources_view", array("data" => $data, ), true);
      }
      else
      {
        $title = "Fout, onbekende bron";
      }

      echo Admin_Tools::renderDynamicModal($title, $content, $buttons);
    }


   /**
    *  add
    *
    *  Add a new source
    *
    *  @param void
    *  @return void
    *
    *  TODO: check if user is allowed !!
    */    
    public function add()
    {
      $buttons = array("<button type='submit' class='btn btn-primary'>Opslaan</button>",
                       "<button type='button' class='btn btn-secondary' data-dismiss='modal'>Sluiten</button>");
      $title   = "Bron toevoegen";
      $content = $this->load->themeContentView("sensorbucket_sources_add", array(), true);

      echo Admin_Tools::renderDynamicModal($title, $content, $buttons);
    }


   /**
    *  edit
    *
    *  Edit source configuration
    *
    *  @param integer $sourceID
    *  @return void
    *
    *  TODO: check if user is allowed !!
    */    
    public function edit($sourceID)
    {
      $buttons = array("<button type='submit' class='btn btn-primary'>Opslaan</button>",
                       "<button type='button' class='btn btn-secondary' data-dismiss='modal'>Sluiten</button>");

      $data    = $this->sensorbucket->getSource($sourceID);
      $content = "Geen gegevens beschikbaar";

      if (isset($data->name))
      {
        $title   = "Bewerk bron ".$data->name;
        $content = $this->load->themeContentView("sensorbucket_sources_edit", array("data" => $data), true);
      }
      else
      {
        $title   = "Fout, onbekende bron";
        $buttons = array("<button type='button' class='btn btn-secondary' data-dismiss='modal'>Sluiten</button>");
      }

      echo Admin_Tools::renderDynamicModal($title, $content, $buttons);
    }


   /**
    *  delete
    *
    *  Delete source configuration
    *
    *  @param integer $sourceID
    *  @return void
    *
    *  TODO: check if user is allowed !! / WHAT to do with devices that use this source ??
    */    
    public function delete($sourceID)
    {
      $data = $this->sensorbucket->deleteSource($sourceID);
      header("Location:/sources/overview");
      exit;
    }


  }