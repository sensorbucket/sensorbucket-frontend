<?php

defined('BASEPATH') OR exit('No direct script access allowed');
define('DISPLAY_DEBUG', false);

// https://www.jqueryscript.net/table/jQuery-Plugin-For-Sortable-Orderable-Table-VanderTable.html

/**
 *  Submit controller
 *
 *  Handle all admin submits
 *
 *  @author    Wim Kosten <w.kosten@zeeland.nl>
 *  @copyright Provincie Zeeland
 *
 */
class Submit extends Sensorbucket_Admin_Controller
{
    private $m_post;
    private $m_ref;


    /**
     *  Public constructor
     *
     *  Init parent constructor and load needed files
     */
    public function __construct()
    {
        parent::__construct();
        require_once(APPPATH . '/libraries/autoload.php');

        $this->m_post = $this->input->post();
        $this->m_ref  = (isset($_SERVER["HTTP_REFERER"])) ? $_SERVER["HTTP_REFERER"]:false;
    }


    /**
     *  dyn_modal_form
     * 
     *  Handle the dyn_modal_form AJAX posts
     * 
     *  @param void
     *  @return void
     * 
     */
    public function dyn_modal_form()
    {
        $response = dyn_Modal_Form_Response::getInstance();
        $response->setPostData($this->m_post);
        $response->setReferrer($this->m_ref);

        if (stripos($this->m_ref, "sensorbucket.nl") === false && stripos($this->m_ref, "localhost") === false)
        {
            $response->setError("Invalid request");

            header('Content-Type: application/json');
            echo json_encode((array)$response->getResponse());
            exit;
        }

        if (isset($this->m_post["action"]))
        {
            $action = trim($this->m_post["action"]);
            $response->setCaller($action);

            if (method_exists($this, $action))
            {
                $this->$action();
            }
            else
            {
                $response->setError("Method {$action} not found");
            }
        }
        else
        {
            $response->setError("Not clear what action");
        }

        header('Content-Type: application/json');
        echo json_encode((array)$response->getResponse());
        exit;
    }


    /**
     *  sensorbucket_locations_add
     * 
     *  Add a new location
     *
     *  @param void
     *  @return void 
     */
    private function sensorbucket_locations_add()
    {
        $posted   = $this->sensorbucket->addLocation($this->m_post);
        $response = dyn_Modal_Form_Response::getInstance();
  
        if ($posted === true)
        {
          //$response->setError("HTTP Post error");   
        }
        
        $response->setProcessedData($posted);
        $response->setPostEval("document.location.href='/locations/overview'");        
    }
    

    /**
     *  sensorbucket_devices_add
     * 
     *  Add a new sensorbucket device
     * 
     */
    private function sensorbucket_devices_add()
    {
        $posted   = $this->sensorbucket->addDevice($this->m_post);
        $response = dyn_Modal_Form_Response::getInstance();

        if ($posted === true)
        {
            $response->setPostEval("document.location.href='/devices/overview'");
        }
        else
        {
            $response->setError("HTTP Post error"); 
        }
    }


    private function sensorbucket_devicetypes_add()
    {
        $posted   = $this->sensorbucket->addDeviceType($this->m_post);
        $response = dyn_Modal_Form_Response::getInstance();

        if ($posted === true)
        {
            $response->setPostEval("document.location.href='/devicetypes/overview'");
        }
        else
        {
            $response->setError("HTTP Post error"); 
        }    
    }

}



/**
 *  dyn_Modal_Form_Response
 * 
 *  Class for return JSON responses
 *  
 *  @param void
 *  @return void
 * 
 */
class dyn_Modal_Form_Response
{
    private $m_response;
    private static $instance;


    public static function getInstance()
    {
        if (null === static::$instance)
        {
            static::$instance = new static();
        }

        return static::$instance;
    }


    private function __construct()
    {
        $this->m_response            = new StdClass();
        $this->m_response->post      = "";
        $this->m_response->ref       = "";
        $this->m_response->status    = "";
        $this->m_response->caller    = "";
        $this->m_response->post_eval = "";
        $this->m_response->p_data    = "";
    }

    public function setProcessedData($data)     {$this->m_response->p_data    = $data;}
    public function setPostData($data)          {$this->m_response->post      = $data;}
    public function setReferrer($referrer)      {$this->m_response->ref       = $referrer;}
    public function setStatus($status)          {$this->m_response->status    = $status;}
    public function setPostEval($code)          {$this->m_response->post_eval = $code;}
    public function setCaller($caller)          {$this->m_response->caller    = $caller;}
    public function setError($msg)              {$this->m_response->status    = "ERROR"; $this->m_response->form_error_message = $msg;}
    public function getResponse()               {return (array)$this->m_response;}

}
