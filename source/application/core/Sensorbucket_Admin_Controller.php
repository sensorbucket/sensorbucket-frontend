<?php

  class Sensorbucket_Admin_Controller extends CI_Controller
  {
      protected $m_menu;
      protected $m_API;

      public function __construct()
      {
        // Run parent
        parent::__construct();

        require_once(APPPATH.'libraries/autoload.php');
        require_once(APPPATH.'libraries/sensorbucket/class.authentication.inc');
        require_once(APPPATH."libraries/layout/class.page_menu.inc");

        // Load sensorbucket model
        $this->load->model("Sensorbucket_model", "sensorbucket", false);

        // Check for a login
        if (Authentication::getUser() === false)
        {
          header("location:/auth/login/admin");
          exit;
        }

        $this->m_API = sensorBucket_API::getInstance();
        $this->loadMenu();
      }


      /**
       *  initPage method
       *
       *  Default function for creating a page using the Page singleton
       *
       *  @internal
       *  @param $title string Title of the page
       *  @param $active string Active item for the left-menu
       *  @param $desc string Description of the page (meta-tag)
       *  @return void
       *
       */
      protected function initPage($title, $active, $desc="")
      {
          // Get page / meta instances
          $page      = page::getInstance();
          $meta      = Page_Meta::getInstance();
          $menu      = Page_Menu::getInstance();
          $pageTitle = "Provincie Zeeland - {$title}";

          if (!is_array($active))
          {
            $active = array("", "");
          }
          
          $menu->setActiveItem($active);

          //$page->setPageTemplate("sensorbucket_admin_page");
          $page->setPageTemplate("inspinia_admin");

          // Disable minifying
          $page->setMinify(false);

          // Standard metadata
          $meta->addItem("generator", "Wombat framework v1.25");
          $meta->addItem("author", "Wim Kosten / Provincie Zeeland");
          $meta->setTitle($pageTitle);
          $meta->setKeywords("");
          $meta->setDescription($desc);

          $page->addRegionContent("title", $title);
      }

      
      private function loadMenu()
      {
        $pageMenu = Page_Menu::getInstance();
        $config   = $this->config->load("menu");
        $nav      = $this->config->item("nav");
        $items    = (isset($nav["items"])) ? $nav["items"]:array();
        $logo     = (isset($nav["logo"]))  ? $nav["logo"]:"";
        
        $pageMenu->setMenuItems($items);
        $pageMenu->setLogo($logo);
      } 

  }
