<?php

 /**
  *  Sensorbucket_Loader
  *  Extends the loader so we can use theme-able views
  *
  *  @author Wim Kosten <w.kosten@zeeland.nl>
  *
  */

  class Sensorbucket_Loader extends CI_Loader
  {

    public function themeFileExists($view, $type)
    {
      $themeView  = APPPATH."views/".Page_Theme::getInstance()->getThemePath();
      $themeView .= "{$type}/{$view}.php";
      return is_file($themeView);
    }


    public function themeView($view, $vars=array())
    {
      $themeView  = Page_Theme::getInstance()->getThemePath();
      $themeView .= $view;
      return $this->view($themeView, $vars, true);
    }
    
    public function themeLayoutView($view, $vars=array())
    {
      $view = "layout/{$view}";
      return $this->themeView($view, $vars);
    }    
    
    public function themeContentView($view, $vars=array())
    {
      $view = "content/{$view}";
      return $this->themeView($view, $vars);
    }

    public function themeMapView($view, $vars=array())
    {
      $view = "map_specific/{$view}";
      return $this->themeView($view, $vars);
    }
    
    public function themeFeatureView($view, $vars=array())
    {
      $view = "featuredata/{$view}";
      return $this->themeView($view, $vars);
    }

    public function themeInfoView($view, $vars=array())
    {
      $view = "info/{$view}";
      return $this->themeView($view, $vars);
    }

    public function themeDrupalModel($loadDB=true)
    {
      $themeModel = Page_Theme::getInstance()->getThemeModel();

      if ($themeModel !== false)
      {
        $this->model("Drupal_model", "drupal_base", $loadDB);
        $this->model($themeModel, "drupal", $loadDB);
      }
      else
      {
        $this->model("Drupal_model", "drupal", $loadDB);
      }
    }

  }

