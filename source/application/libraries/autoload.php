<?php

  // Layout
  require_once(__DIR__."/layout/class.page_theme.inc");
  require_once(__DIR__."/layout/class.page_meta.inc");
  require_once(__DIR__."/layout/class.page.inc");
  require_once(__DIR__."/layout/class.table_generator.inc");
  require_once(__DIR__."/layout/class.form_generator.inc");
  require_once(__DIR__."/layout/class.ol_map.inc");

  // Services
  //require_once(__DIR__."/services/class.http_service.inc");

  // Sensorbucket
  require_once(__DIR__."/sensorbucket/class.admin_tools.inc");
  require_once(__DIR__.'/sensorbucket/class.sensorbucket_api.inc');
  //require_once(__DIR__."/sensorbucket/class.sensorbucket_api.inc");

