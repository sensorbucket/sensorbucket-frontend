<?php

  class Bulk_Loader
  {
    private $m_type; 
    private $m_paramsRaw;
    private $m_paramsParsed;
    private $m_fileRoots;
    private $m_useCache;
    private $m_cacheID;
    private $m_cacheFile;
    private $m_fileTypes;
    private $m_fileTimes;

    private $m_useGZIP = false;
    private $m_lang    = "en";
    private $m_eTag    = "";
    private $fileCode  = "";


   /**
    *
    *  
    *
    *  
    *
    *
    */  
    public function __construct($module, $params)
    {
      $this->m_fileRoots    = array();
      $this->m_fileTimes    = array();
      $this->m_fileTypes    = array();
      $this->m_useCache     = false;
      $this->m_type         = $module;
      $this->m_paramsRaw    = $params; 
      $this->m_paramsParsed = $this->parseParams($params);
    }


   /**
    *
    *  
    *
    *  
    *
    *
    */  
    public function setFileRootAndTypes($fileRoots)
    {
      $this->m_fileRoots = $fileRoots;
      $this->m_fileTypes = $this->setFileTypes();
    }


   /**
    *
    *  
    *
    *  
    *
    *
    */  
    public function setGZIP($gzip)
    {
      $this->m_useGZIP = $gzip; 
    }


   /**
    *
    *  
    *
    *  
    *
    *
    */  
    public function setLang($lang)
    {
      $this->m_lang = $lang;
    }


   /**
    *
    *  
    *
    *  
    *
    *
    */  
    public function getCode()
    {
      $baseDir = (isset($this->m_fileTypes[$this->m_type]["path"])) ? $this->m_fileTypes[$this->m_type]["path"]:false; 
      $code    = false;

      if ($baseDir !== false)
      {
        if ($this->m_useCache === true)
        {
          $this->m_cacheFile = $baseDir."cache_{$this->m_cacheID}.".$this->m_fileTypes[$this->m_type]["type"];

          if (is_file($this->m_cacheFile)) 
          {
            $code                = file_get_contents($this->m_cacheFile);
            $this->m_fileTimes[] = filemtime($this->m_cacheFile);
          }
          else
          {
            $code                = $this->compressData($this->readFiles($baseDir), $this->m_fileTypes[$this->m_type]["type"]);
            $this->m_fileTimes[] = time();
            file_put_contents($this->m_cacheFile, $code);
          }
        }
        else
        {
          $code = $this->readFiles($baseDir);
        }
      }

      $this->m_eTag = md5($code);
      return $code;
    }


   /**
    *
    *  
    *
    *  
    *
    *
    */
    public function sendCode($code)
    {
      if ($code !== false)
      {
        $mimeType = $this->m_fileTypes[$this->m_type]["mime"];

        // Start GZIP compression
        if ($this->m_useGZIP === true)
        {
          ob_start("ob_gzhandler");
        }

        // Wombat header
        header("Wombat-version: 1.2");

        // Language
        header("Content-Language: ".$this->m_lang);

        // MIME type
        header($mimeType);

        // Caching stuff
        header("Cache-Control: no-cache");

        echo trim($code);
        exit;
      }

      // Default sent a 404
      header("HTTP/1.0 404 Not found");
      exit;
    }  


   /**
    *
    *  
    *
    *  
    *
    *
    */  
    private function getLastModified()
    {
      $nTimes = count($this->m_fileTimes);

      if ($nTimes === 0)
      {
        return time(); 
      }
      elseif ($nTimes === 1)
      {
        return $this->m_fileTimes[0];
      }
      else
      {
        krsort($this->m_fileTimes, SORT_NUMERIC);
        return $this->m_fileTimes[0];
      }
    }


   /**
    *
    *  
    *
    *  
    *
    *
    */  
    private function readFiles($basePath)
    {
      clearstatcache();
      $code = "";

      foreach ($this->m_paramsParsed AS $file)
      {
        $file = str_replace("..", "", $file);

        if (substr($file, 0, 1) == "~")
        {
          $filePath = "/".str_replace("~", "/", substr($file, 1));
        }
        else
        {
          $filePath = $basePath.str_replace(":", "/", $file);
        }

        $baseName = basename($filePath);

        if (is_file($filePath))
        {
          $lastModified         = filemtime($filePath);
          $fileCode             = "\n\n";
          $fileCode            .= "/**\n";
          $fileCode            .= " * File ...........: {$baseName}\n";
          //$fileCode            .= " * Path ...........: {$filePath}\n";
          $fileCode            .= " * Size ...........: ".filesize($filePath)." bytes\n";
          $fileCode            .= " * Last modified ..: ".date("d-m-Y H:i:s", $lastModified)."\n";
          $fileCode            .= " */\n";
          $fileCode            .= file_get_contents($filePath);
          $this->m_fileTimes[]  = $lastModified;
        }
        else
        {
          $fileCode  = "\n\n";
          $fileCode .= "/**\n";
          $fileCode .= " * File ...........: {$baseName} NOT FOUND!!!\n";
          //$fileCode .= " * Path ...........: {$filePath}\n";
          $fileCode .= " */\n";
        }

        $code .= $fileCode;
      }

      return $code;
    }



   /**
    *
    *  
    *
    *  
    *
    *
    */  
    private function parseParams($params)
    {
      $filter = array();

      foreach ($params AS $param)
      {
        if (stripos($param, "cache_id") === false)
        {
          $filter[] = $param;
        }
        else
        {
          @list($cache, $cacheID) = explode(":", trim($param));

          if (!is_null($cacheID))
          {
            $this->m_cacheID  = $cacheID;
            $this->m_useCache = true;
          }
        }
      }

      return $filter;
    }


   /**
    *
    *  
    *
    *  
    *
    *
    */  
    private function setFileTypes()
    {
      $types         = array();
      $types["gcss"] = array("mime" => "Content-type: text/css"       , "type" => "css", "path" => $this->m_fileRoots["global_css"]);
      $types["gjs"]  = array("mime" => "Content-type: text/javascript", "type" => "js" , "path" => $this->m_fileRoots["global_js"]);
      $types["tcss"] = array("mime" => "Content-type: text/css"       , "type" => "css", "path" => $this->m_fileRoots["theme_css"]);
      $types["tjs"]  = array("mime" => "Content-type: text/javascript", "type" => "js" , "path" => $this->m_fileRoots["theme_js"]);
      return $types;
    }


   /**
    *
    *  
    *
    *  
    *
    *
    */  
    private function compressData($content, $type)
    {
      $host["js"]  = "https://javascript-minifier.com/raw";
      $host["css"] = "https://cssminifier.com/raw";
      $url         = $host[$type];
      $postdata = array('http' => array('method'  => 'POST',
                                        'header'  => 'Content-type: application/x-www-form-urlencoded',
                                        'content' => http_build_query( array('input' => $content))));
      return file_get_contents($url, false, stream_context_create($postdata));
    }

  }
