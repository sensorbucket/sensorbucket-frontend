<?php

class Form_Generator
{
    /*
    $mform->addTextField("vessel_imo", "IMO", $info[0]["vessel_imo"], array("req" => false, "maxsize" => 100));
    $mform->addTextField("vessel_email", "email", "mail", array("req" => false, "subclass" => "email"));
    $mform->addTextField("vessel_tel", "tel", "mail", array("req" => false, "subclass" => "tel"));
    $mform->addTextField("vessel_url", "url", "mail", array("req" => false, "subclass" => "url"));
    $mform->addTextField("vessel_password", "pwd", "mail", array("req" => false, "subclass" => "password"));
    $mform->addTextField("vessel_number", "number", "mail", array("req" => false, "subclass" => "number"));
    $mform->addTextFieldWithOptions("vessel_lookup", "subtype", "iets", array("req" => false, "subclass" => "text", "button_type" => "btn-primary", "button_text" => "Opzoeken"));

    $mform->addDateTimeField("dt-local", "Date/time", NULL, array("req" => false, "subclass" => "datetime-local"));
    $mform->addDateTimeField("dt", "Date", NULL, array("req" => false, "subclass" => "date"));
    $mform->addDateTimeField("m", "Month", NULL, array("req" => false, "subclass" => "month"));
    $mform->addDateTimeField("w", "Week", NULL, array("req" => false, "subclass" => "week"));
    $mform->addDateTimeField("t", "Time", NULL, array("req" => false, "subclass" => "time"));

    $mform->addLocationField("location_field", "Positie", array(lat, lon), array("req" => false))
    */

    private $m_formClass  = "m-form m-form--fit m-form--label-align-right";
    private $m_focusField = "";
    private $m_additions  = array("css" => array(), "js" => array(), "docready" => array());
    private $m_buttons    = array();
    private $m_required   = array();
    private $m_encType    = "";
    private $m_formID;
    private $m_formAction;
    private $m_formMethod;
    private $m_fields;
    private $m_colLabel;
    private $m_colData;



    public function __construct($id, $action, $method="post")
    {
        $this->m_formID     = $id;
        $this->m_formAction = $action;
        $this->m_formMethod = $method;
        $this->m_fields     = array("visible" => array(), "hidden" => array(), "required" => array());
        $this->m_colLabel   = 2;
        $this->m_colData    = 10;
    }


    public function setFormClass($class)
    {
        $this->m_formClass = $class;
    }


    public function setLabelWidth($width)
    {
        $this->m_colLabel = (int)$width;
        $this->m_colData  = 12-$this->m_colLabel;
    }


    public function setFocusField($id)
    {
        $this->m_focusField = $id;
    }


    public function setRequired($id, $options)
    {
        if (isset($options["req"]) && $options["req"] === true)
        {
            $this->m_required[$id] = true;
        }
    }


    public function getRenderedField($id)
    {
        if (isset($this->m_fields["visible"][$id]))
        {
            return $this->m_fields["visible"][$id];
        }
        return false;
    }


    public function addHiddenField($id, $value)
    {
        $this->m_fields["hidden"][$id] = "<input type='hidden' name='{$id}' id='{$id}' value=\"{$value}\">\n";
    }


    public function addDummyField($id, $label, $value, $options=array())
    {
        // We'll use a hidden field to comply with the label for
        $style = (isset($options["maxsize"])) ? "style='max-width:{$options["maxsize"]}px;'":"";
        $id    = "dummy_{$id}";
        $code  = "<span class='form-control m-input--dummy' {$style}>{$value}</span><input type='hidden' name='{$id}' id='{$id}' value=''>\n";
        $this->m_fields["visible"][$id] = array("label" => $label, "code" => $code);
    }


    public function addCodeField($id, $label, $code, $options=array())
    {
        // We'll use a hidden field to comply with the label for
        $style  = (isset($options["maxsize"])) ? "style='max-width:{$options["maxsize"]}px;'":"";
        $id     = "code_{$id}";
        $code  .= "<input type='hidden' name='{$id}' id='{$id}' value=''>\n";
        $this->m_fields["visible"][$id] = array("label" => $label, "code" => $code);
    }


    public function addTextField($id, $label, $value, $options = array("req" => false, "subclass" => "text"))
    {
        $req      = (isset($options["req"]) && $options["req"] === true) ? "required":"";
        $subclass = (isset($options["subclass"])) ? $options["subclass"]:"text";
        $style    = (isset($options["maxsize"])) ? "style='max-width:{$options["maxsize"]}px;'":"";
        $this->m_fields["visible"][$id] = array("label" => $label, "code" => "<input type='{$subclass}' class='form-control m-input' {$style} id='{$id}' name='{$id}' data-required-message='Dit veld is verplicht' value=\"{$value}\" {$req}>");
        $this->setRequired($id, $options);
    }


    public function addTextFieldWithOptions($id, $label, $value, $options = array("req" => false, "subclass" => "text", "button_type" => "btn-secondary", "button_text" => "GO", "button_handler" => ""))
    {
        $req      = (isset($options["req"]) && $options["req"] === true) ? "required":"";
        $subclass = (isset($options["subclass"])) ? $options["subclass"]:"text";
        $style    = (isset($options["maxsize"])) ? "style='max-width:{$options["maxsize"]}px;'":"";
        $bText    = (isset($options["button_text"])) ? $options["button_text"]:"...";
        $bType    = (isset($options["button_type"])) ? $options["button_type"]:"btn-metal";
        $bHandler = (isset($options["button_handler"])) ? $options["button_handler"]:"";
        $code     = "<div class='input-group'>\n";
        $code    .= " <input type='{$subclass}' class='form-control m-input' {$style} id='{$id}' name='{$id}' value=\"{$value}\" {$req}>\n";
        $code    .= " <div class='input-group-append'>\n";
        $code    .= " <button class='btn {$bType}' type='button' {$bHandler}>{$bText}</button>\n";
        $code    .= " </div>\n";
        $code    .= "</div>\n";

        $this->m_fields["visible"][$id] = array("label" => $label, "code" => $code);
        $this->setRequired($id, $options);
    }


    public function addTextFieldWithDropdown($id, $label, $value, $dropdownOptions, $dropdownDefault, $options=array())
    {
        $req      = (isset($options["req"]) && $options["req"] === true) ? "required":"";
        $subclass = (isset($options["subclass"])) ? $options["subclass"]:"text";
        $style    = (isset($options["maxsize"])) ? "style='max-width:{$options["maxsize"]}px;'":"";
        $bText    = (isset($options["button_text"])) ? $options["button_text"]:"IMO";
        $bType    = (isset($options["button_type"])) ? $options["button_type"]:"btn-primary";
        $hidden   = "<input type='hidden' id='{$id}_options' name='{$id}_options' value=\"{$dropdownDefault}\">\n";
        $code     = "<div class='input-group'>\n";
        $code    .= "  <input type='text' class='form-control' {$style} id='{$id}' name='{$id}' value=\"{$value}\" {$req}>\n";
        $code    .= "  <div class='input-group-append'>\n";
        $code    .= "    <button type='button' id='{$id}_button' class='btn {$bType}' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>{$bText}</button>\n";
        $code    .= "    <div class='dropdown-menu'>\n";

        foreach ($dropdownOptions AS $dropdownValue => $dropdownTitle)
        {
            $code    .= "      <a class='dropdown-item' href='javascript:setTextFieldWithDropdownValues(\"{$id}\", \"{$dropdownValue}\");'>{$dropdownTitle}</a>\n";
        }

        $code    .= "    </div>\n";
        $code    .= "  </div>\n";
        $code    .= "</div>\n";
        $code    .= $hidden;

        $this->m_fields["visible"][$id] = array("label" => $label, "code" => $code);
        $this->setRequired($id, $options);
    }


    public function addSearchField($id, $label, $value, $options=array())
    {
        $bText    = (isset($options["button_text"])) ? $options["button_text"]:"Zoeken";
        $bType    = (isset($options["button_type"])) ? $options["button_type"]:"btn-primary";
        $style    = (isset($options["maxsize"])) ? "style='max-width:{$options["maxsize"]}px;'":"";
        $onclick  = (isset($options["onclick"])) ? $options["onclick"]:"alert(\"Button click\")";

        $divWidth = $options["maxsize"]+77;
        $divStyle = (isset($options["maxsize"])) ? "style='max-width:{$divWidth}px; height:200px; border:1px solid #EEE; padding:15px; overflow:auto;'":"";
        $code     = "<div class='input-group'>\n";
        $code    .= " <input type='text' class='form-control m-input' {$style} id='{$id}' name='{$id}' value=\"{$value}\">\n";
        $code    .= " <div class='input-group-append'>\n";
        $code    .= " <button class='btn {$bType}' type='button' onclick='{$onclick}'>{$bText}</button>\n";
        $code    .= " </div>\n";
        $code    .= "</div>\n";
        $code    .= "<div id='search_result_{$id}' {$divStyle}></div>";

        $this->m_fields["visible"][$id] = array("label" => $label, "code" => $code);
        $this->setRequired($id, $options);
    }


    public function addTextArea($id, $label,$value, $options = array("req" => false, "rows" => 5))
    {
        $style  = (isset($options["maxsize"]))   ? "style='max-width:{$options["maxsize"]}px;'":"";
        $rows   = (isset($options["rows"]))      ? (int)$options["rows"]:5;
        $maxLen = (isset($options["maxlength"])) ? "maxlength='".(int)$options["maxlength"]."'":"";
        $code   = "<textarea class='form-control m-input' {$style} id='{$id}' name='{$id}' {$maxLen} rows='{$rows}'>{$value}</textarea>";

        if (isset($options["show_chars_left"]) && $options["show_chars_left"] === true && isset($options["maxlength"]) && (int)$options["maxlength"] > 0)
        {
//        $code .= "<div id='counter_{$id}'>Resterende tekens: {$options["maxlength"]}</div>";
        }

        $this->m_fields["visible"][$id] = array("label" => $label, "code" => $code);
        $this->setRequired($id, $options);
    }


    public function addTagField($id, $label, $values, $options = array())
    {
        $code = "<input type='text' id='{$id}' name='{$id}' class='form-control m-input' value='{$values}'>";
        $code = (isset($options["maxsize"])) ? "<div style='border:0px solid red; max-width:{$options["maxsize"]}px;'>{$code}</div>":$code;

        $this->m_fields["visible"][$id]            = array("label" => $label, "code" => $code);

        // Add stuff to the page instance
        $page     = Page::getInstance();
        $docReady = "\$('#{$id}').tagsInput({width:'auto', 'defaultText':'Nieuw'});";

        $page->addDocumentReadyCode($docReady);
        $page->addStylesheet("tagsinput.css");
        $page->addJavascript("tagsinput.js", "END_BODY");
    }


    public function addDateTimeField($id, $label, $dateTime, $options = array("req" => false, "subclass" => "datetime-local"))
    {
        $req      = (isset($options["req"]) && $options["req"] === true) ? "required":"";
        $subclass = (isset($options["subclass"])) ? $options["subclass"]:"datetime-local";
        $style    = (isset($options["maxsize"])) ? "style='max-width:{$options["maxsize"]}px;'":"";

        if ($style === "")
        {
            $style = "style='max-width:160px;'";

            if ($subclass === "time")
            {
                $style = "style='max-width:100px;'";
            }
        }


        if (is_null($dateTime) || trim($dateTime) === "")
        {
            $stamp = time();
        }
        else
        {
            $stamp = strtotime($dateTime);
        }

        switch ($subclass)
        {
            case "datetime-local":
                $value = date("Y-m-d", $stamp)."T".date("H:i:s", $stamp);  // T13:45:00
                break;

            case "date":
                $value = date("Y-m-d", $stamp);
                break;

            case "month":
                $value = date("Y-m", $stamp);
                break;

            case "week":
                $value = date("Y", $stamp)."-W".date("W", $stamp);
                break;

            case "time":
                $value = date("H:i:s", $stamp);
                break;
        }

        $this->m_fields["visible"][$id] = array("label" => $label, "code" => "<input type='{$subclass}' class='form-control m-input' {$style} id='{$id}' name='{$id}' value=\"{$value}\" {$req}>");
        $this->setRequired($id, $options);
    }


    public function addSelectField($id, $label, $values, $default, $options = array("req" => false, "subclass" => "select", "maxsize" => 250))
    {
        // https://developer.snapappointments.com/bootstrap-select/
        $subclass   = (isset($options["subclass"])) ? $options["subclass"]:"select";
        $style      = (isset($options["maxsize"])) ? "style='width:{$options["maxsize"]}px'":"style='width:350px;'";
        $liveSearch = (isset($options["livesearch"])) ? "true":"false";
        $items      = (isset($options["maxitems"])) ? (int)$options["maxitems"]:8;
        $code       = "";

        switch ($subclass)
        {
            case "select":
                $code  = "<select class='form-control m-bootstrap-select m_selectpicker' name='{$id}' id='{$id}' data-width='350px' data-size='{$items}' data-live-search='{$liveSearch}' {$style}>\n";

                foreach ($values AS $key => $caption)
                {
                    $selected  = ($default === $key) ? "selected='selected'":"";
                    $code     .= "<option value=\"{$key}\" {$selected}>{$caption}</option>\n";
                }

                $code .= "</select>\n";
                break;
        }

        $this->m_fields["visible"][$id] = array("label" => $label, "code" => $code);

        if (!isset($this->m_additions["docReady"]["m_select"]))
        {
            $this->m_additions["docReady"]["m_select"] = true;

            $page     = Page::getInstance();
            $docReady = '$(".m_selectpicker").selectpicker({dropupAuto:false});';
            $page->addDocumentReadyCode($docReady);
        }
    }


    public function addCheckBox($id, $label, $value, $checked=false, $options=array())
    {
        $style = (isset($options["style"])) ? $options["style"]:"m-checkbox--square";
        $check = ($checked === true || (int)$checked === 1) ? "checked='checked'":"";
        $code  = "<label class='m-checkbox {$style}'>\n";
        $code .= " <input id='{$label}' name='{$id}' class='mt-checkbox' type='checkbox' value='{$value}' {$check}>\n";
        $code .= " <span class='mt-checkbox-span'></span>\n";
        $code .= "</label>\n";

        $this->m_fields["visible"][$id] = array("label" => $label, "code" => $code);
    }


    public function addFileUpload($id, $label, $options=array())
    {
        if (isset($options["multiple"]) && $options["multiple"] === true)
        {
            $multiple  = "multiple='multiple'";
            $divWidth  = (isset($options["maxsize"])) ? "max-width:{$options["maxsize"]}px;'":"";
            $divHeight = (isset($options["maxheight"])) ? "{$options["maxheight"]}px":"100px";
            $fileList  = "<div id='{$id}_filelist' style='overflow-y:auto; padding:15px; border:1px solid #EEE; height:{$divHeight}; {$divWidth}'>Geen bestanden geselecteerd</div>";
            $action    = "onChange='makeFileList(\"{$id}\");'";
            $name      = "{$id}[]";
        }
        else
        {
            $multiple = "";
            $fileList = "";
            $action   = "";
            $name     = $id;
        }

        $style = (isset($options["maxsize"])) ? "style='max-width:{$options["maxsize"]}px;'":"";
        $code  = "<input id='{$id}' name='{$name}' type='file' class='form-control m-input' {$style} {$multiple} {$action}>\n";
        $code .= $fileList;

        $this->m_fields["visible"][$id] = array("label" => $label, "code" => $code);

        // Set form encoding type
        $this->m_encType = "multipart/form-data'";
    }


    public function addLocationField($id, $label, $settings=array(), $options=array())
    {
      $mapWidth  = (isset($options["maxwidth"]))  ? $options["maxwidth"]:"100%";
      $mapHeight = (isset($options["maxheight"])) ? $options["maxheight"]:"300px";
      $lat       = (isset($settings["lat"]))      ? $settings["lat"]:51.5006462;
      $lon       = (isset($settings["lon"]))      ? $settings["lon"]:3.6152966;
      $title     = (isset($settings["name"]))     ? $settings["name"]:"Default locatie";
      $map       = new OL_Map($lat, $lon, "map_{$id}");
      $map->addLocation($id, $lat, $lon, "location", $title);

      if (isset($settings["callback"]))
      {
        $map->setClickCallBack($settings["callback"]);
      }

      $code  = "<div id='map_{$id}' style='height:{$mapHeight}; width:{$mapWidth}; border:1px solid black;'></div>";
      $code .= "<div id='map_{$id}_container'><strong>lat:</strong> <span id='map_{$id}_lat'>{$lat}</span>&nbsp;|&nbsp;<strong>lon:</strong> <span id='map_{$id}_lon'>{$lon}</span></div>";
      $code .= "<input type='hidden' id='map_{$id}_lat_field' name='map_{$id}_lat_field' value='{$lat}'>";     
      $code .= "<input type='hidden' id='map_{$id}_lon_field' name='map_{$id}_lon_field' value='{$lon}'>";     
      $code .= "<script type='text/javascript'>setTimeout(() => {".$map->render(10)."}, 250);</script>";

      $this->m_fields["visible"][$id] = array("label" => $label, "code" => $code);  
    }


    public function addButton($id, $label, $style="btn-primary", $type="submit", $options=array())
    {
        $action = (isset($options["action"])) ? "onclick=\"{$options["action"]}\"":"";
        $target = (isset($options["target"])) ? "target='{$options["target"]}'":"";
        $button = "<button id='{$id}' name='{$id}' type='{$type}' class='btn {$style}' {$action} {$target}>{$label}</button>";

        $this->m_buttons[$id] = $button;
    }


    public function addRowDevider($id, $label="")
    {
        $this->m_fields["visible"][$id] = array("label" => $label, "code" => "", "devider" => true);
    }


    public function renderForm($renderFormTags=true)
    {
        $enc  = ($this->m_encType !== "") ? "enctype='{$this->m_encType}'":"";
        $code = "";

        if ($renderFormTags === true)
        {
            $code = "<form class='{$this->m_formClass}' id='{$this->m_formID}' name='{$this->m_formID}' action='{$this->m_formAction}' method='$this->m_formMethod' {$enc}>\n";
        }

        // Hidden fields
        foreach ($this->m_fields["hidden"] AS $id => $fieldCode)
        {
            $code .= $fieldCode;
        }

        // Visible fields
        foreach ($this->m_fields["visible"] AS $id => $fieldData)
        {
            if (isset($fieldData["devider"]))
            {
                $code .= "<div class='m-form_devider'><span class='m-form_devider_label'>{$fieldData["label"]}</span></div>";
            }
            else
            {
                $code .= $this->renderFormRow($id, $fieldData["label"], $fieldData["code"]);
            }
        }

        $code .= $this->renderFormButtons();

        if ($renderFormTags === true)
        {
            $code .= "</form>";
        }

        // If we have a focus field, set focus
        if ($this->m_focusField !== "")
        {
            $code .= "<script>";
            $code .= "var focusField = document.{$this->m_formID}.{$this->m_focusField};  focusField.focus();\n";
            $code .= "</script>\n";
        }

//      $page     = Page::getInstance();
//      $docReady = "\$('input[required]').on('invalid', function() {this.setCustomValidity($(this).data('required-message'));});";
        return $code;
    }


    public function getAdditions()
    {
        return $this->m_additions;
    }


    private function renderFormRow($id, $label, $fieldCode)
    {
        $code  = "<div class='form-group m-form__group row m-form__group-nopadding'>\n";
        $code .= " <label for='{$id}' class='col-{$this->m_colLabel} col-form-label col-form-label__bold'>{$label}</label>\n";
        $code .= " <div class='col-{$this->m_colData}'>\n";
        $code .= $fieldCode."\n";
        $code .= " </div>\n";
        $code .= "</div>\n";

        return $code;
    }


    private function renderFormButtons()
    {
        $code = "";

        if (count($this->m_buttons) > 0)
        {
            $code  = "<div class='form-group m-form__group row' style='border-top:1px solid #EEE; margin-top:5px;'>\n";
            $code .= " <div class='col-{$this->m_colLabel} col-form-label col-form-label__bold'>&nbsp;</div>\n";
            $code .= " <div class='col-{$this->m_colData}'>\n";
            $code .= "<span class='pull-right'>".join("&nbsp;", $this->m_buttons)."</span>\n";
            $code .= " </div>\n";
            $code .= "</div>\n";
        }

        return $code;
    }

}