<?php
/**
 * Created by JetBrains PhpStorm.
 * User: maritimephoto
 * Date: 01-11-13
 * Time: 01/11/2013 13:40
 * To change this template use File | Settings | File Templates.
 */




class FormTools
{
  private $m_formMeta         = array();
  private $m_formFields       = array();
  private $m_formButtons      = array();
  private $m_formFieldsHidden = array();
  private $m_focusField       = "";
  private $m_isRequired       = false;
  private $m_target           = "";
  private $m_buttonAlign      = "right";
  private $m_multipleJs       = "";
  private $m_autoFillJS       = "";
  private $m_renderFormTag    = true;
  private $m_buttonFooterMsg  = "";
  private $m_submitCode       = "";
  private $m_tabs             = array();
  private $m_formSubmitAction = "";
  private $m_labelWidth       = "20%";
  private $m_requiredFields   = array();
  private $m_onClick          = array();


  public function __construct($name, $action, $method="post")
  {
    $this->m_formMeta["name"]     = $name;
    $this->m_formMeta["method"]   = $method;
    $this->m_formMeta["action"]   = $action;
    $this->m_formMeta["encoding"] = "";
  }


  public function setLabelWidth($width)
  {
    $this->m_labelWidth = $width;
  }


  public function setButtonFooterMsg($msg)
  {
    $this->m_buttonFooterMsg = $msg;
  }

  public function alignButtons($align)
  {
    $this->m_buttonAlign = $align;
  }


  public function renderFormTag($render)
  {
    $this->m_renderFormTag = $render;
  }
  
  public function setModal()
  {
    $this->m_target = "target='_parent'";
  }


  public function enableFileUpload($uploads)
  {
    if ($uploads === true)
    {
      $this->setEncoding("multipart/form-data");
    }
    else
    {
      $this->setEncoding("");
    }
  }


  public function setEncoding($encoding)
  {
    $this->m_formMeta["encoding"] = $encoding;
  }


  public function setFocusField($field)
  {
    $this->m_focusField = $field;
  }


  public function addRequiredField($field)
  {
    $this->m_requiredFields[] = $field;
  }


  public function addOnClick($field, $onclickCode)
  {
    $this->m_onClick[$field] = $onclickCode;
  }


  public function hiddenField($name, $value)
  {
    $this->m_formFieldsHidden[$name] = "<input type='hidden' name='{$name}' id='{$name}' value='{$value}'>";
  }

  public function readonlyField($label, $value)
  {
    $this->m_formFields[$label] = $value;
  }

  public function addDevider($id, $caption="", $style="")
  {
    $this->m_formFields[$id] = "devider|{$caption}|{$style}";
  }

  public function inputField($label, $name, $value="", $size="", $required=false)
  {
    $label = ($required === true) ? "{$label}&nbsp;<span style='color:red; font-weight:bold;'>*</span>":$label;
    $style = ($size !== "") ? "style='width:{$size}';":"";
    $req   = ($required === true) ? "required":"";
    $this->m_formFields[$label] = '<input class="inputDefault" '.$style.' autocomplete="off" type="text" id="'.$name.'" name="'.$name.'" value="'.$value.'" '.$req.'>'."\n";
  }


  public function passwordField($label, $name, $value, $size="")
  {
    $style = ($size !== "") ? "style='width:{$size}';":"";
    $this->m_formFields[$label] = '<input class="inputDefault" '.$style.' autocomplete="off" type="password" id="'.$name.'" name="'.$name.'" value="'.$value.'">'."\n";
  }


  public function textArea($label, $name, $value, $width=400, $height=250)
  {
    $this->m_formFields[$label] = "<textarea class='inputDefault inputTextarea_{$height}' style='width:{$width}px; height:{$height}px;' name='{$name}' id='{$name}'>{$value}</textarea>\n";
  }


  public function checkbox($label, $name, $checked)
  {
    $checked = ($checked === true) ? 'checked="checked"':"";
    $this->m_formFields[$label] = '<input class="inputDefault" type="checkbox" id="'.$name.'" name="'.$name.'" value="1" '.$checked.'>'."\n";
  }


  public function inputWithPicklist($label, $name, $value="", $size="", $pickListOptions=array())
  {
    $required = false;
    $label    = ($required === true) ? "{$label}&nbsp;<span style='color:red; font-weight:bold;'>*</span>":$label;
    $style    = ($size !== "") ? "style='width:{$size}';":""; 

    //javascript:modalBox("/calendar/add", "iframe", "Item toevoegen", 400, 600);
    $action = $pickListOptions["action"];

    $input  = '<input class="inputDefault" '.$style.' type="text" id="'.$name.'" name="'.$name.'" value="'.$value.'">';
    $code   = "<div>";
    $code  .= "<div style='float:left'>{$input}</div>";
    $code  .= "<div style='float:left'>&nbsp;&nbsp;";
    $code  .= "<button type='button' name='{$name}_picklist' id='{$name}_picklist' onclick=\"{$action}\" class='btn blue'>{$pickListOptions["title"]}</button>";
    $code  .= "</div>";
    $code  .= "</div>";

    $this->m_formFields[$label] = $code;
  }


  public function codeField($label, $code)
  {
    $this->m_formFields[$label] = $code;
  }


  public function selectField($label, $name, $options, $default="", $size="", $addNew = array())
  {
    $style = ($size !== "") ? "style='width:{$size}';":"";
    $code  = "<select name='{$name}' id='{$name}' class='inputDefault inputSelect' {$style}>\n";

    foreach ($options AS $key => $value)
    {
      $selected = ($key == $default) ? "selected='selected'":"";
      $code    .= " <option value='{$key}' {$selected}>{$value}</option>\n";
    }

    $code .= "</select>\n";

    if (count($addNew) > 0)
    {
      $orgCode = $code;
      $code    = "<div>";
      $code   .= "<div style='float:left'>{$orgCode}</div>";
      $code   .= "<div style='float:left'>&nbsp;&nbsp;";

    }

    $this->m_formFields[$label] = $code;
  }


  public function dependencySelectField($label, $name, $options, $default="", $onchange, $size="")
  {
    $style = ($size !== "") ? "style='width:{$size}';":"";
    $code  = "<select name='{$name}' id='{$name}' class='inputDefault inputSelect' {$style} onchange='{$onchange}'>\n";

    foreach ($options AS $key => $value)
    {
      $selected = ($key == $default) ? "selected='selected'":"";
      $code    .= " <option value='{$key}' {$selected}>{$value}</option>\n";
    }

    $code .= "</select>\n";

    $this->m_formFields[$label] = $code;
  }



  public function autoFillField($label, $name, $options, $default="", $size="")
  {
    $code = "<select id='{$name}'name='{$name}[]' multiple class='demo-default' style='width:70%' />";

    foreach ($options AS $key => $value)
    {
      $selected = ($key == $default) ? "selected='selected'":"";
      $code    .= " <option value='{$key}' {$selected}>{$value}</option>\n";
    }

    $code .= "</select>\n";

    $this->m_formFields[$label] = $code;

    $page = page::getInstance();
    $page->addDocumentReadyCode($this->autoFill($name));
  }


  public function fileUpload($label, $name, $multiple=false, $showFiles=false, $fillInfo=array())
  {
    $this->enableFileUpload(true);

    $code   = "";
    $accept = (isset($fillInfo["accept"])) ? "accept='{$fillInfo["accept"]}'":"";

    if (isset($fillInfo["url"]) && trim($fillInfo["url"]) !== "")
    {
      if (isset($fillInfo["preview"]) && $fillInfo["preview"] === true)
      {
        $width = (isset($fillInfo["width"])) ? "style='width:{$fillInfo["width"]}px; border:1px solid #ccc;'":"style='border:1px solid #ccc;'";
        $code  = "<img {$width} src='{$fillInfo["url"]}'><br>".basename($fillInfo["url"])."<br><br>";
      }
      else
      {
        $urlTitle = (isset($fillInfo["title"])) ? $fillInfo["title"]:basename($fillInfo["url"]);
        $code     = "<span class='label label-primary'><a class='link_white normal' target='_formattachment' href='{$fillInfo["url"]}'>{$urlTitle}</a></span><br/><br/>";
      }
    }

    if ($multiple === true)
    {
      $this->m_multipleJs = $this->fileUploadScript($name);
      $code .= "<input id='{$name}' name='{$name}[]' type='file' multiple='multiple' {$accept} onChange='makeFileList(\"{$name}\");'>\n";
      $code .= "<br/><div id='{$name}_lijst' name='{$name}_lijst'>Geen bestanden geselecteerd</div>";
    }
    else
    {
      $code .= "<input id='{$name}' name='{$name}' type='file' {$accept}>\n";
    }

    $this->m_formFields[$label] = $code;
  }

  
  private function autoFill($field)
  {
    return "\$('#{$field}').selectize({maxItems:2});";
  }

  private function fileUploadScript($field)
  {
    $script  = "<script type='text/javascript'>\n";
    $script .= "function makeFileList(field)\n";   
    $script .= "{\n";
    $script .= " var ulItem = field+'_lijst'\n";
    $script .= " var input  = document.getElementById(field);\n";
    $script .= " var list   = document.getElementById(ulItem);\n";
    $script .= " while (list.hasChildNodes()) {list.removeChild(list.firstChild);}\n";
    $script .= " for (var x = 0; x < input.files.length; x++)\n";
    $script .= " {\n";
    $script .= "  var li = document.createElement('li');\n";
    $script .= "  li.innerHTML = 'File ' + (x + 1) + ':  ' + input.files[x].name;\n";
    $script .= "  list.appendChild(li);\n";
    $script .= " }\n";
    $script .= "}\n"; 
    $script .= "</script>\n";

    return $script;
  }


  public function _submitButton($caption, $name, $color="default", $action="")
  {
    if ($action == "")
    {
      $code = "<button type='submit' name='{$name}' id='{$name}' class='btn {$color}'>{$caption}</button>";
    }
    else
    {
      $onclick = 'onclick="'.$action.';"';
      $code = "<button type='button' name='{$name}' id='{$name}' class='btn {$color}' {$onclick}>{$caption}</button>";
    }

    $this->m_formButtons[$name] = $code;
  }


  public function submitButton($caption, $name, $color="default", $action="", $enabled=true)
  {
    $disabled = ($enabled === true) ? "":"disabled='disabled'";

    if ($action == "")
    {
      $onclick = "";

      if (count($this->m_requiredFields) > 0)
      {
        $fields  = join("|", $this->m_requiredFields);
        $click   = (isset($this->m_onClick[$name])) ? $this->m_onClick[$name].";":"";
        $onclick = 'onclick="'.$click.'validateForm(\''.$this->m_formMeta["name"].'\', \''.$fields.'\'); return false;"';
      }

      $code = "<button type='submit' name='{$name}' id='{$name}' class='btn {$color}' {$onclick} {$disabled}>{$caption}</button>";
    }
    else
    {
      $onclick = 'onclick="'.$action.';"';
      $code = "<button type='button' name='{$name}' id='{$name}' class='btn {$color}' {$onclick} {$disabled}>{$caption}</button>";
    }

    $this->m_formButtons[$name] = $code;
  }


  public function renderSubmitButtons()
  {
    return $this->m_submitCode."\n</form>";
  }


  public function addTab($tabID, $tabName)
  {
    $this->m_tabs[$tabID]["title"] = $tabName;
    $this->m_tabs[$tabID]["items"] = array();
  }


  public function addItemToTab($tabID, $item)
  {
    $this->m_tabs[$tabID]["items"][] = $item;
  }


  public function setSubmitAction($action)
  {
    $this->m_formSubmitAction = $action;
  }


  public function renderForm($renderButtons=true)
  {
    $enc     = ($this->m_formMeta["encoding"] === "") ? "":"enctype='{$this->m_formMeta["encoding"]}'";
    $target  = ($this->m_target !== "") ? $this->m_target:"";
    $submit  = (trim($this->m_formSubmitAction) !== "") ? "onsubmit=\"{$this->m_formSubmitAction}\"":"";
    $tabHtml = "";

    if ($this->m_renderFormTag === true)
    {
      $html = "<form {$target} name='{$this->m_formMeta["name"]}' id='{$this->m_formMeta["name"]}' method='{$this->m_formMeta["method"]}' action='{$this->m_formMeta["action"]}' {$enc} {$submit}>\n";
    }
    else
    {
      $html = "";
    }

    /* Render hidden fields */
    foreach ($this->m_formFieldsHidden AS $field)
    {
      $html .= $field;
    }


    // Render tabs
    if (count($this->m_tabs) > 0)
    {
      $html .= $this->renderTabForm();
    }
    else
    {
      /* Render 'normal' fields */
      $table = new Table_Generator();
      $table->setCaptionFieldSettings($this->m_labelWidth, "caption");

      foreach ($this->m_formFields AS $label => $field)
      {
        if (stripos($field, "devider|") === false)
        {
          $table->addDataRow(array($label, $field));
        }
        else
        {
          list($devider, $caption, $options) = explode("|", $field);
          $caption = ($caption == "") ? "&nbsp;":$caption;
          $style   = ($options === "") ? "style='background-color:#f0f0f0;'":"{$options}";
          $tableRow = new Table_DataRow();
          $tableRow->addCol($caption, "colspan='2' {$style}");
          $table->addDataRowByCode($tableRow->getRow());
        }
      }

      if ($this->m_buttonAlign == "formfield")
      {
        $buttonCode = "";

        foreach ($this->m_formButtons AS $button)
        {
          $buttonCode .= $button."&nbsp;";
        }

        $table->addDataRow(array("&nbsp;", $buttonCode));
      }

      $html .= $table->renderTable();
    }


    /* Render buttons */
    $buttonHtml = "";

    if ($this->m_buttonAlign == "left" || $this->m_buttonAlign == "center" || $this->m_buttonAlign == "right")
    {
      $buttonHtml .= "<div align='{$this->m_buttonAlign}' style='border-top:1px solid #ccc; padding-{$this->m_buttonAlign}:0px; padding-top:10px; padding-bottom:10px;'>";

      if ($this->m_buttonAlign === "right" && $this->m_buttonFooterMsg !== "")
      {
        $buttonHtml .= "<div align='left' style='float:left;' id='footerMessage'>{$this->m_buttonFooterMsg}</div>";
      }

      foreach ($this->m_formButtons AS $button)
      {
        $buttonHtml .= $button."&nbsp;";
      }
      $buttonHtml .= "</div>";
      $this->m_submitCode = $buttonHtml;
    }

    if ($renderButtons === true)
    {
      $html .= $buttonHtml;
    }
    else
    {
      $this->renderFormTag(false);
    }

    if ($this->m_renderFormTag === true)
    {
      $html  .= "</form>";
    }

    if ($this->m_focusField !== "")
    {
      $html .= "<script language='javascript'>";
      $html .= "  var elem = document.{$this->m_formMeta["name"]}.{$this->m_focusField};";
      $html .= "  elem.focus();";
      $html .= "</script>\n";
    }

    $html .= $tabHtml;

    $html .= $this->m_multipleJs;
    $html .= $this->m_autoFillJS;

    return $html;
  }


  private function renderTabForm()
  {
    $tabHtml = "<ul class='nav nav-tabs'>";
    $active  = "class='active'";

    foreach ($this->m_tabs AS $tabID => $tabData)
    {
      $tabHtml .= "<li {$active} role='presentation'><a aria-controls='{$tabID}' role='tab' href='#{$tabID}' data-toggle='tab'>{$tabData["title"]}</a></li>";
      $active   = "";
    }

    $tabHtml .= "</ul><br/>";
    $tabHtml .= "<div class='tab-content clearfix'>";
    $active   = "active";

    foreach ($this->m_tabs AS $tabID => $tabData)
    {
      $tabHtml .= "<div role='tabpanel' class='tab-pane {$active}' id='{$tabID}'>";

      $table = new Table_Generator();
      $table->setCaptionFieldSettings("20%", "caption");

      foreach ($tabData["items"] AS $item)
      {
        $field = $this->m_formFields[$item];
        //$table->addDataRow(array($item, $field));

        if (stripos($field, "devider|") === false)
        {
          $table->addDataRow(array($item, $field));
        }
        else
        {
          list($devider, $caption, $options) = explode("|", $field);
          $caption = ($caption == "") ? "&nbsp;":$caption;
          $style   = ($options === "") ? "style='background-color:#f0f0f0;'":"{$options}";
          $tableRow = new Table_DataRow();
          $tableRow->addCol($caption, "colspan='2' {$style}");
          $table->addDataRowByCode($tableRow->getRow());
        }

      }

      if ($this->m_buttonAlign == "formfield")
      {
        $buttonCode = "";

        foreach ($this->m_formButtons AS $button)
        {
          $buttonCode .= $button."&nbsp;";
        }

        $table->addDataRow(array("&nbsp;", $buttonCode));
      }

      $tabHtml .= $table->renderTable();
      $tabHtml .= "</div>";
      $active   = "";
    }

    $tabHtml .= "</div>";

    return $tabHtml;
  }


}
