<?php

 /**
  *  OL_Map
  *
  *  Stripped down version of our OL4 implementation in kaarten.zeeland.nl
  *
  *  @author Wim Kosten <w.kosten@zeeland.nl>
  *
  */
  class OL_Map
  {
    private $m_Lat;
    private $m_Lon;
    private $m_locations;
    private $m_containerID   = "map";
    private $m_clickCallback = "";


    public function __construct($lat, $lon, $containerID="map")
    {
      $this->m_locations   = array();
      $this->m_Lat         = $lat;
      $this->m_Lon         = $lon;
      $this->m_containerID = $containerID;
    }


    public function addLocation($id, $lat, $lon, $type, $name)
    {
      $this->m_locations[$id] = array("lat" => $lat, "lon" => $lon, "type" => $type, "name" => $name);
    }


    public function setClickCallBack($functionName)
    {
      $this->m_clickCallback = $functionName;
    }
    
    
    public function render($zoom=15)
    {
      $code  = $this->renderMap($zoom);

      if (count($this->m_locations) > 0)
      {
        $code .= $this->renderLocations();
      }

      return $code;
    }


    private function renderMap($zoom)
    {
      $center = $pos = $this->m_Lon.", ".$this->m_Lat;

      $code  = "var baselayer_source = new ol.source.XYZ({url: 'https://geodata.nationaalgeoregister.nl/tiles/service/wmts/brtachtergrondkaart/EPSG:3857/{z}/{x}/{y}.png', crossOrigin: 'anonymous'});\n";
      $code .= "var baselayer        = new ol.layer.Tile({title: 'NL Maps', type: 'base', visible: true, preload: 0, source: baselayer_source});\n";
      $code .= "var vectorSource     = new ol.source.Vector({});\n";
      $code .= "var vectorLayer      = new ol.layer.Vector({source: vectorSource});\n";
      $code .= "var clickSource      = new ol.source.Vector({});\n";
      $code .= "var clickLayer       = new ol.layer.Vector({source: clickSource, visible: true});\n";
      $code .= "var center           = ol.proj.transform([".$center."], 'EPSG:4326', 'EPSG:3857');\n";
      $code .= "var zoom             = {$zoom};\n";
      $code .= "var view             = new ol.View({projection: 'EPSG:3857', center: center, zoom: zoom});\n";
      $code .= "var map              = new ol.Map({target: '{$this->m_containerID}', layers: [baselayer, vectorLayer, clickLayer], view: view, logo : false, controls : ol.control.defaults({zoom: true, attribution: false, rotate: false}).extend([])});\n";
      $code .= "map.addControl(new ol.control.ScaleLine({units: 'metric'}));\n";

      if ($this->m_clickCallback !== "")
      {
        $code .= "map.on('singleclick', function(evt)\n";
        $code .= "{\n";
        $code .= " var latLon = ol.proj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326');\n";
        $code .= "{$this->m_clickCallback}(latLon, evt);\n";
        $code .= "});\n";
      } 


      //$code .= "map.on('singleclick', function(evt){var latLon = ol.proj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326'); console.log(latLon);});\n";

      return $code;
    }
    

    private function renderLocations()
    {
      $code  = "var lat;\n";
      $code .= "var lon;\n";
      $code .= "var pos;\n";
      $code .= "var point;\n";
      $code .= "var name;\n";
      $code .= "var img;\n";
      $code .= "var size;\n";
      $code .= "var imgIcon;\n";
      $code .= "var label;\n";
      $code .= "var text;\n";
      $code .= "var style;\n";
      $code .= "var iconFeature;\n";

      foreach ($this->m_locations AS $locationID => $locationData)
      {
        $code .= "lat         = parseFloat({$locationData["lat"]});\n";
        $code .= "lon         = parseFloat({$locationData["lon"]});\n";
        $code .= "pos         = ol.proj.transform([lon, lat], 'EPSG:4326', 'EPSG:3857');\n";
        $code .= "point       = new ol.geom.Point(pos);\n";
        $code .= "name        = \"{$locationData["name"]}\";\n";
        $code .= "img         = '/assets/images/sensor_location_marker_red_24.png';\n";
        $code .= "size        = [24, 24];\n";
        $code .= "imgIcon     = new ol.style.Icon({src: img, anchor: [0.5, 0.5], anchorXUnits : 'fraction', anchorYUnits : 'fraction'});\n";
        $code .= "label       = name;\n";
        $code .= "text        = new ol.style.Text({text: name, font: '12px arial', textAlign: 'left', offsetX: 15, offsetY:5, stroke: new ol.style.Stroke({color: '#666'})});\n";
        $code .= "style       = new ol.style.Style({image: imgIcon, text: text});\n";
        $code .= "iconFeature = new ol.Feature({geometry: point, name: 'feature_{$locationID}'});\n";
        $code .= "iconFeature.setStyle(style);\n";
        $code .= "vectorSource.addFeature(iconFeature);\n";
        $code .= "console.log('Adding location '+name);\n";
      }

      return $code;
    }

  }