<?php

  /**
   *  Page
   *  Class for rendering a page
   *
   *
   */
  class Page
  {
    private static $instance;

    private $m_pageLayout    = "";
    private $m_minify        = true;
    private $m_lastModified  = 0;
    private $m_expiry        = 0;
    private $m_uri           = "";
    private $m_regions       = array();
    private $m_layoutData    = array();
    private $m_CI            = NULL;
    private $m_cssFiles      = array();
    private $m_cssLinks      = array();
    private $m_jsFiles       = array("HEAD" => array(), "END_BODY" => array());
    private $m_jsLinks       = array();
    private $m_metaData      = "";
    private $m_docReadyCode  = "";
    private $m_response      = 200;
    private $m_contentRegion = "content";
    private $m_themeInfo     = array();
    private $m_headers       = array();
    private $m_returnOutput  = false;
    private $m_pageData      = array();
    private $m_pageActions   = array();


   /**
     *  getInstance
     *
     *  Get the instance of this class
     *
     *  @param void
     *  @return Page
     *
     */
    public static function getInstance()
    {
      if (null === static::$instance)
      {
        static::$instance = new static();
      }

      return static::$instance;
    }    


   /**
     *  private constructor
     *
     *  Init class  
     *
     *  @param void
     *  @return void
     *
     */
    private function __construct()
    {
      $this->m_CI           = &get_instance();
      $this->m_themeInfo    = Page_Theme::getInstance()->getThemeInfo();
      $this->m_lastModified = time();
      $this->m_expiry       = time()+86400;
    }
 

   /**
     *  private clone
     *
     *  Disable cloning
     *
     *  @param void
     *  @return void
     *
     */
    private function __clone()
    {        
    }


    public function outputContent($output)
    {
      $this->m_returnOutput = $output;
    }


    public function setMinify($minify)
    {
      $this->m_minify = $minify;
    }


    public function setLastModified($time)
    {
      $this->m_lastModified = $time();
    }


   /**
     *  setPageTemplate
     *
     *  Set the page template to use as defined in config/layout.php
     *
     *  @param string $page ID of the layout
     *  @param string $autoLoad Optionally load the regions from the configfile.
     *  @return void
     *
     */
    public function setPageTemplate($page, $autoLoad=true)
    {
      $this->m_pageLayout = $page;

      if ($autoLoad === true)
      {
        // Clean some arrays
        $this->m_cssFiles = array("theme" => array(), "global" => array());
        $this->m_cssLinks = array();
        $this->m_jsFiles  = array("HEAD" => array(), "END_BODY" => array());

        // Add the regions
        if (isset($this->m_themeInfo["theme_settings"]["templates"][$page]))
        {
          $layoutInfo = $this->m_themeInfo["theme_settings"]["templates"][$page];

          foreach ($layoutInfo["regions"] AS $regionName => $regionInfo)
          {
            $this->addRegion($regionName, $regionInfo["view"]);
            $this->addRegionData($regionName, $regionInfo["data"]);

            if (isset($regionInfo["content_region"]) && $regionInfo["content_region"] === true)
            {
              $this->setContentRegion($regionName);
            }
          }
        }

        // Add the CSS files
        if (!is_null($layoutInfo) && isset($layoutInfo["css"]))
        {
          foreach ($layoutInfo["css"] AS $cssFile)
          {
            $this->addStylesheet($cssFile);
          }
        }

        // Add the Javascript files
        if (!is_null($layoutInfo) && isset($layoutInfo["scripts"]))
        {
          $scripts = $layoutInfo["scripts"];

          if (isset($scripts["head"]))
          {
            foreach ($scripts["head"] AS $headJS)
            {
              $this->addJavascript($headJS, "HEAD");
            }
          }

          if (isset($scripts["endbody"]))
          {
            foreach ($scripts["endbody"] AS $endJS)
            {
              $this->addJavascript($endJS, "END_BODY");
            }
          }
        }
      }

    }


   /**
     *  setStatusHeader
     *
     *  Set the page template to use as defined in config/layout.php
     *
     *  @param string $page ID of the layout
     *  @param string $autoLoad Optionally load the regions from the configfile.
     *  @return void
     *
     */
    public function setStatusHeader($status)
    {
      $this->m_response = $status;
    }


   /**
     *  addRegion
     *
     *  Add a region and it's optional view
     *
     *  @param string $regionName Name of the region
     *  @param string $view Optional view file (if not specified, data just used without rendering through a view)
     *  @return void
     *
     */
    public function addRegion($regionName, $view)
    {
      if (!isset($this->m_regions[$regionName]))
      {
        $this->m_regions[$regionName] = array("view" => $view, "data" => array(), "content" => "");
      }
    }


   /**
     *  addRegionData
     *
     *  Add data to a region
     *
     *  @param string $regionName Name of the region
     *  @param string $dataName Name of the dataitem
     *  @param any $regionData Data to add (any type)
     *  @return void
     *
     */
    public function addRegionData($regionName, $regionData)
    {
      $this->m_regions[$regionName]["data"] = $this->m_regions[$regionName]["data"]+$regionData;
    }


   /**
     *  addRegionContent
     *
     *  Add the content for a region
     *
     *  @param string $regionName Name of the region
     *  @param string $dataName Name of the dataitem
     *  @param any $regionData Data to add (any type)
     *  @return void
     *
     */
    public function addRegionContent($regionName, $content)
    {
      $this->m_regions[$regionName]["content"] = $content;
    }


    public function addLayoutData($key, $data)
    {
      $this->m_layoutData[$key] = $data;
    }


    public function addGlobalPageData($data)
    {
      $this->m_pageData = $data;
    }


    public function getGlobalPageData()
    {
      return $this->m_pageData;
    }


    public function addPageAction($title, $action, $buttonType="btn-primary")
    {
      $this->m_pageActions[] = array("title" => $title, "action" => $action, "button_type" => $buttonType);
    }


    public function setBreadCrumb($items)
    {
      $this->m_layoutData["breadcrumb"] = $items;
    }


   /**
     *  getRegions
     *
     *  Get the regions 
     *
     *  @param void
     *  @return array
     *
     */
    public function getRegions()
    {
      return $this->m_regions;
    }


    /**
     *  getRegionData
     *
     *  Get all of specific data from a region
     *
     *  @param string $region Name of the region
     *  @param string $dataName Optional dataname (if not specified all regiondata is returned)
     *  @return array
     *
     */
    public function getRegionData($region, $dataName="")
    {
      if (isset($this->m_regions[$region]))
      {
        if ($dataName !== "" && isset($this->m_regions[$region]["data"][$dataName]))
        {
          return $this->m_regions[$region]["data"][$dataName];
        }
        else
        {
          return $this->m_regions[$region]["data"];
        }
      }

      return array();
    }


    /**
     *  cleanRegion
     *
     *  Empty the data and content parts of the specified region
     *
     *  @param string $region Name of the region
     *  @return void
     *
     */
    public function cleanRegion($region, $keep=array())
    {
      $data = array();

      if (isset($this->m_regions[$region]))
      {
        if (count($keep) > 0)
        {
          foreach ($keep AS $savedItem)
          {
            if (isset($this->m_regions[$region]["data"][$savedItem]))
            {
              $data[$savedItem] = $this->m_regions[$region]["data"][$savedItem];
            }
          }
        }

        unset($this->m_regions[$region]["data"]);

        $this->m_regions[$region]["data"]    = $data;
        $this->m_regions[$region]["content"] = ""; 
      }
    }


    public function setContentRegion($regionName)
    {
      $this->m_contentRegion = $regionName;
    }

    public function getContentRegionName()
    {
      return $this->m_contentRegion;
    }


    /**
     *  setRegionView
     *
     *  Override the wombat view for a region
     *
     *  @param string $region Name of the region
     *  @param string $view Name of the view
     *  @return void
     *
     */
    public function setRegionView($region, $view)
    {
      if (isset($this->m_regions[$region]))
      {
        $this->m_regions[$region]["view"] = $view;
      }
    }


    /**
     *  addStylesheet
     *
     *  Add a stylesheet reference
     *
     *  @param string $file CSS file
     *  @return void
     *
     */
    public function addStylesheet($file)
    {
      if (stripos($file, "http") !== false)
      {
        $this->m_cssLinks[] = $file;
      }
      else
      {
        if (stripos($file, "theme://") !== false)
        {
          $this->m_cssFiles["theme"][] = str_replace("theme://", "", $file);
        }
        else
        {
          $this->m_cssFiles["global"][] = $file;
        }
      }
    }


    /**
     *  addJavascript
     *
     *  Add a javascript file to either the head or the end of the body  
     *
     *  @param string $script File to use
     *  @param string $location Location for the JS file (either "HEAD" or "END_BODY")
     *  @return void
     *
     */
    public function addJavascript($script, $location="HEAD")
    {
      if (stripos($script, "http") !== false)
      {
        $this->m_jsLinks[$location][] = $script;
      }
      else
      {
        if (stripos($script, "theme://") !== false)
        {
          $this->m_jsFiles[$location]["theme"][] = str_replace("theme://", "", $script);
        }
        else
        {
          $this->m_jsFiles[$location]["global"][] = $script;
        } 
      }
    }


    /**
     *  addDocumentReadyCode
     *
     *  Add a document ready code
     *
     *  @param string $code Code to add
     *  @return void
     *
     */
    public function addDocumentReadyCode($code)
    {
      $this->m_docReadyCode .= $code;
    }


    /**
     *  addMetaData
     *
     *  Add the metadata
     *
     *  @param string $metadata Metadata as created with the page_meta class
     *  @return void
     *
     */
    public function addMetaData()
    {
      $meta = Page_Meta::getInstance();

      // Additional mobile settings
      if (isset($this->m_themeInfo["theme_settings"]["mobile"]))
      {
        foreach ($this->m_themeInfo["theme_settings"]["mobile"] AS $metaItem => $metaValue)
        {
          $metaValue = str_replace("[ASSETS_THEME_PATH]", $this->m_themeInfo["assets_path"], $metaValue);
          $meta->addItem($metaItem, $metaValue);
        }
      }

      // Icons (favicon, apple touch etc)
      if (isset($this->m_themeInfo["theme_settings"]["icons"]))
      {
        $icons = $this->m_themeInfo["theme_settings"]["icons"];

        // Apple touch
        if (isset($icons["apple"]))
        {
          foreach ($icons["apple"] AS $size => $iconValue)
          {
            // <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
            $iconValue = str_replace("[ASSETS_THEME_PATH]", $this->m_themeInfo["assets_path"], $iconValue); 
            $meta->addIconRel("apple-touch-icon", $iconValue, $size);
          }
        }

        // Android icon
        if (isset($icons["android"]))
        {
          foreach ($icons["android"] AS $size => $iconValue)
          {
            $iconValue = str_replace("[ASSETS_THEME_PATH]", $this->m_themeInfo["assets_path"], $iconValue); 
            $meta->addIconRel("icon", $iconValue, $size);
          }
        }
 
        // favicon
        if (isset($icons["favicon"]))
        {
          foreach ($icons["favicon"] AS $size => $iconValue)
          {
            $iconValue = str_replace("[ASSETS_THEME_PATH]", $this->m_themeInfo["assets_path"], $iconValue); 
            $meta->addIconRel("icon", $iconValue, $size);
          }
        }
      }

//      $meta->addItem("og:title", $meta->getTitle());
//      $meta->addItem("twitter:title", $meta->getTitle());

      $this->m_metaData  = $meta->getMetaItems();

      // Autofill the title region
      $titleData             = $this->getRegionData("title");
      $titleData["title"]    = $meta->getTitle();
      $titleData["subtitle"] = $meta->getSubTitle();
      $this->addRegionData("title", $titleData);
    }   


    public function addHeader($header)
    {
      $this->m_headers[] = $header;
    }


    public function setExpiry($expiry)
    {
      $this->m_expiry = $expiry;
    }



    /**
     *  renderView
     *
     *  Get the view 
     *
     *  @param string $type Type of content
     *  @param string $prefix Prefix for the view file (eg drupal_)
     *  @return string
     *
     */    
    public function getView($type, $prefix="drupal--")
    {
      $viewName = $prefix.$type;
      $viewFile = APPPATH."views/content/{$viewName}.php";

      if (is_file($viewFile))
      {
        d_info("Found type-specific view: {$viewName} ({$viewFile})");
        return "content/{$viewName}";
      }

      d_info("Using wombat view (generic D7)");
      return "content/drupal";
    }


    /**
     *  renderRegion
     *
     *  Render the specified region
     *
     *  @param array $regionData Array with regiondata
     *  @return string
     *
     */
    private function renderRegion($regionData)
    {
      if (isset($regionData["view"]) && trim($regionData["view"]) !== "")
      {
        $themePath = $this->m_themeInfo["theme_path"];
        $viewFile  = VIEWPATH.$themePath."layout/{$regionData["view"]}.php";

        if (is_file($viewFile))
        {
          return $this->m_CI->load->themeLayoutView($regionData["view"], array("data" => $regionData["data"], "content" => $regionData["content"]), true);
        }
      }

      // No view specified, just return the content (if any)
      return (isset($regionData["content"])) ? $regionData["content"]:"";
    }


    /**
     *  renderCSS
     *
     *  Render the loader entry for CSS files
     *
     *  @param void
     *  @return string
     *
     */
    private function renderCSS()
    {
      $css = "";

      if (count($this->m_cssLinks) > 0)
      {
        $css = "\n  <!-- External stylesheets -->\n";
        foreach ($this->m_cssLinks AS $cssLink)
        {
          $css .= '  <link rel="stylesheet" type="text/css" href="'.$cssLink.'" />'."\n";
        }
        $css .= "  <!-- /External stylesheets -->\n";
      }

      // Global stylesheets
      if (isset($this->m_cssFiles["global"]) && count($this->m_cssFiles["global"]) > 0 && trim($this->m_cssFiles["global"][0]) !== "")
      {
        $css   .= "\n  <!-- Global stylesheets -->\n";
        $loader = "/loader/gcss/".join("/", $this->m_cssFiles["global"]);
        $css   .= '  <link href="'.$loader.'" rel="stylesheet" type="text/css"/>'."\n";
        $css   .= "  <!-- /Global stylesheets -->\n";
      }

      if (isset($this->m_cssFiles["theme"]) && count($this->m_cssFiles["theme"]) > 0 && trim($this->m_cssFiles["theme"][0]) !== "")
      {
        $css   .= "\n  <!-- Theme stylesheets -->\n";
        $loader = "/loader/tcss/".join("/", $this->m_cssFiles["theme"]);
        $css   .= '  <link href="'.$loader.'" rel="stylesheet" type="text/css"/>'."\n";
        $css   .= "  <!-- /Theme stylesheets -->\n";
      }

      return $css;
    }

   
    /**
     *  renderJS
     *
     *  Render the loader entry for JS files
     *
     *  @param array $jsFiles Array with files
     *  @return string
     *
     */
    private function renderJS($jsFiles, $type)
    {
      $jsLoader   = array();
      $type       = strtoupper($type);
      $jsExternal = "";

      if (isset($this->m_jsLinks[$type]) && count($this->m_jsLinks[$type]) > 0)
      {
        $jsExternal = "\n  <!-- External Javascript -->\n";
        foreach ($this->m_jsLinks[$type] AS $jsLink)
        {
          $jsExternal .= '  <script src="'.$jsLink.'" type="text/javascript"></script>'."\n";
        }
        $jsExternal .= "  <!-- /External Javascript -->\n\n";
      }

      // Global Javascript files
      if (isset($jsFiles["global"]) && count($jsFiles["global"]) > 0 && trim($jsFiles["global"][0]) !== "")
      {
        $rawJsFiles = array();
        $phpJsFiles = array();

        foreach ($jsFiles['global'] AS $jsFile)
        {
          $pos = stripos($jsFile, "?");

          if ($pos !== false)
          {
            $jsFile = substr($jsFile, 0, $pos);
          } 

          $php = stripos($jsFile, ".php");
 
          if ($pos !== false)
          {
            $phpJsFiles[] = $jsFile;
          }
          else
          {
            $rawJsFiles[] = $jsFile;
          }
        }

        //$loader     = "/loader/gjs/".join("/", $jsFiles["global"]);

        // Add raw JS files
        $loader     = "/loader/gjs/".join("/", $rawJsFiles);
        $jsLoader[] = '  <script src="'.$loader.'" type="text/javascript"></script>';

        // Add the PHP files
        $siteRoot  = CONFIG_BASE_DOMAIN.CONFIG_JS_URL;

        foreach ($phpJsFiles AS $phpFile)
        {
          $phpFilePath = $siteRoot.str_replace(":", "/", $phpFile);
          $jsLoader[]  = '  <script src="'.$phpFilePath.'" type="text/javascript"></script>';
        }
      }

      // Theme-specific Javascript files
      if (isset($jsFiles["theme"]) && count($jsFiles["theme"]) > 0 && trim($jsFiles["global"][0]) !== "")
      {
        $loader     = "/loader/tjs/".join("/", $jsFiles["theme"]);
        $jsLoader[] = '  <script src="'.$loader.'" type="text/javascript"></script>';
      }

      $code = $jsExternal;
     
      if (count($jsLoader) > 0)
      {
        $code .= "  <!-- Javascript -->\n";
        $code .= join("\n", $jsLoader);
        $code .= "\n  <!-- /Javascript -->\n";
      }

      return $code;
    }


    /**
     *  renderDocumentReadyCode
     *
     *  Render the documentready code
     *
     *  @param void
     *  @return string
     *
     */
    private function renderDocumentReadyCode()
    {
      if (trim($this->m_docReadyCode) !== "")
      {
        $script  = '<script type="text/javascript">'."\n";
        $script .= 'jQuery(document).ready(function()'."\n";
        $script .= '{'."\n";
        $script .= $this->m_docReadyCode;
        $script .= '});'."\n";
        $script .= '</script>'."\n";

        return $script;
      }

      return "";
    }



    /**
     *  renderDebug
     *
     *  Render the debug info
     *
     *  @param void
     *  @return string
     *
     */    
    private function renderDebug()
    {
      require_once(__DIR__ . "/class.table_generator.inc");
    
      $debug = Wombat_Debug::getInstance();

      if ($debug->show() === true)
      {
        $lines = $debug->getDebugLines();
        return $this->m_CI->load->view("/layout/debug", array("debug_lines" => $lines), true);
      }

      return "";
    }


    /**
     *  sendHttpHeaders
     *
     *  Send HTTP headers
     *
     *  @param void
     *  @return void
     *
     */    
    private function sendHttpHeaders()
    {
      $cacheAge = $this->m_expiry-time();

      $this->m_CI->output->set_header("Sensorbucket: 0.1");
      $this->m_CI->output->set_header("Content-Language: nl");
      $this->m_CI->output->set_header("Content-Type: text/html; charset=ISO-8859-1");
      $this->m_CI->output->set_header("Pragma: public");

      // Security stuff
      $this->m_CI->output->set_header("Referrer-Policy: strict-origin");
      $this->m_CI->output->set_header("X-Frame-Options: DENY");
      $this->m_CI->output->set_header("X-Xss-Protection: 1; mode=block");
      $this->m_CI->output->set_header("X-Content-Type-Options: nosniff");
      $this->m_CI->output->set_header("Strict-Transport-Security:max-age=31536000");

      // Send Content-Security-Policy header
      $this->setContentSecurityPolicy();
    }


    /**
     *  setContentSecurityPolicy()
     *
     *  Send Content-Security-Policy header
     *
     *  @param void
     *  @return void
     *
     */
    private function setContentSecurityPolicy()
    {
      // https://content-security-policy.com/
      $contentSecurity      = "Content-Security-Policy: ";
      $parts["default-src"] = "https";
      $parts["connect-src"] = "'self' *.zeeland.nl *.cyclomedia.com";
      $parts["script-src"]  = "'self' 'unsafe-inline' 'unsafe-eval' code.jquery.com cdnjs.cloudflare.com siteimproveanalytics.com *.bootstrapcdn.com *.zeeland.nl *.fontawesome.com www.amcharts.com cdn.jsdelivr.net";
      $parts["font-src"]    = "'self' *.bootstrapcdn.com fonts.googleapis.com fonts.gstatic.com *.zeeland.nl streetsmart.cyclomedia.com *.cloudflare.com";
      $parts["style-src"]   = "'self' 'unsafe-inline' *.bootstrapcdn.com fonts.googleapis.com cdn.jsdelivr.net code.jquery.com cdnjs.cloudflare.com *.zeeland.nl *.fontawesome.com cdn.jsdelivr.net";
      $parts["img-src"]     = "* data:";
      $parts["frame-src"]   = "*.zeeland.nl";
      $parts["object-src"]  = "'none'";

      foreach ($parts AS $part => $setting)
      {
        $contentSecurity .= "{$part} {$setting}; ";
      }

      $this->m_CI->output->set_header($contentSecurity);
    }    


    private function setHPKP()
    {
      $hpkp   = array();
      $hpkpPath = APPPATH."../hpkp/";
      $main   = file_get_contents($hpkpPath."main-pin.hash");
      $backup = file_get_contents($hpkpPath."backup-pin.hash");

      if ($main !== false && strlen($main) > 0)
      {
        $main   = trim((string)$main);
        $hpkp[] = 'pin-sha256="'.$main.'"';
      }

      if ($backup !== false && strlen($backup) > 0)
      {
        $backup = trim((string)$backup);
        $hpkp[] = 'pin-sha256="'.$backup.'"';
      }

      if (count($hpkp) > 0)
      {
        $hpkp[] = "max-age=2592000; preload";
        $this->m_CI->output->set_header('Public-Key-Pins: '.join("; ", $hpkp));
      }
      else
      {
        $this->m_CI->output->set_header('X-HPKP-Wombat: ERROR');
      }
    }


    /**
     *  renderProfilerData
     *
     *  Render the profilerdata into a nice panel
     *
     *  @param string $buffer Profilercode
     *  @return string
     *
     *  Note: called from the pz_output_callback function in the extended PZ_Output class in application/core (_display() function)
     *
     */    
    public function renderProfilerData($buffer)
    {
      $startPos     = stripos($buffer, "[START_PROFILER]");
      $endPos       = stripos($buffer, "[END_PROFILER]");
      $profilerHTML = "";

      if ($startPos !== false && $endPos !== false)
      {
        $profilerCode = substr($buffer, $startPos, ($endPos-$startPos));
        $profilerCode = str_replace(array("[START_PROFILER]", "[END_PROFILER]"), array("", ""), $profilerCode);
        $profilerHTML = bootstrap::bootstrap_panel($profilerCode, "Profiler info", "info");
        $buffer       = substr($buffer, 0, $startPos).substr($buffer, $endPos+14);
      }

      return str_replace("[PROFILER_DATA]", $profilerHTML, $buffer);
    }


    /**
     *  tagParser
     *
     *  Parse any tags in a string
     *
     *  @param string $content Content to parse
     *  @return string
     *
     */
    public function tagParser($content)
    {
      return $content;

      require_once(__DIR__ . "/../parsers/tags/class.tagparser.inc");
      $parser  = new tagParser();
      return $parser->parseContent($content);
    }


    /**
     *  output
     *
     *  Render the output from all regions into the template
     *
     *  @param string $content Actual content
     *  @return void
     *
     */
    public function output($content, $template="")
    {
      $output    = "<br>";
      $rendered  = array();
      $debugHTML = "";
      $manifest  = "";

      $this->m_pageData["page_actions"] = $this->m_pageActions;

      // Parse the content through our tagparser
      $content = $this->tagParser($content);

      // Add the content to the content region
      $this->addRegionContent($this->m_contentRegion, $content);

      // Render the HEAD section items
      $css      = $this->renderCSS();
      $headJS   = "\n".$this->renderJS($this->m_jsFiles["HEAD"], "head");

      // Assemble HEAD part
      $headCode  = $this->m_metaData;
      $headCode .= $css;
      $headCode .= $headJS;

      // Render the END_BODY section
      $endBodyJS   = $this->renderJS($this->m_jsFiles["END_BODY"], "endbody");
      $docReady    = $this->renderDocumentReadyCode();
      $endBodyCode = $endBodyJS.$docReady;

      // Render the normal regions
      foreach ($this->m_regions AS $region => $regionData)
      {
        $rendered[$region] = $this->renderRegion($regionData);
      }

      // If we have a manifest setting, add this
      if (isset($this->m_themeInfo["theme_settings"]["manifest_file"]))
      {
        $manifest = str_replace("[ASSETS_THEME_PATH]", "/assets/{$this->m_themeInfo["theme_path"]}", $this->m_themeInfo["theme_settings"]["manifest_file"]);
      }

      // Render the regions into the page template
      $output = $this->m_CI->load->themeLayoutView($this->m_pageLayout, array("layoutdata" => $this->m_layoutData, "regions" => $rendered, "manifest" => $manifest), true);

      // Render the debug if enabled
      // if (DISPLAY_DEBUG === true)
      // {
      //   $debugHTML = $this->renderDebug();
      // }

      // Replace the [HEAD], [END_BODY] and [DEBUG_DATA]
      // Note: [PROFILER_DATA] is replaced from the extended MY_Output class in application/core
      $output = str_replace("[HEAD]", $headCode, $output);
      $output = str_replace("[END_BODY]", $endBodyCode, $output);
      $output = str_replace("[DEBUG_DATA]", $debugHTML, $output);

      // Send the HTTP headers
      if ($this->m_returnOutput === false)
      {
        $this->sendHttpHeaders();
      }
     
      // Minify the while bunch
      $version = 2;

      if ($this->m_minify === true)
      { 
        require_once(__DIR__."/class.html_minifier.inc");
        $output = Html_Minifier::minify($output, false, false);
      }

      if ($this->m_returnOutput === true)
      {
        return $output;
      }
      else
      {
        // Send the output
        $this->m_CI->output->set_output($output);
      }
    }
  }  