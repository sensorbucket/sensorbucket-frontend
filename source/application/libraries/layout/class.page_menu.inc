<?php

  class Page_Menu
  {
    private $m_CI;
    private $m_active = "";
    private $m_items  = array();
    private $m_logo   = "";
    private $m_view   = "inspinia_admin_menu";
    private static $instance;


    public static function getInstance()
    {
      if (null === static::$instance)
      {
        static::$instance = new static();
      }

      return static::$instance;
    }


    private function __construct()
    {
      $this->m_CI = &get_instance();
    }


    private function __clone()
    {
    }


    public function setMenuItems($items)
    {
      $this->m_items = $items;
    }


    public function setLogo($logo)
    {
      $this->m_logo = $logo;
    }


    public function setView($view)
    {
      $this->m_view = $view;
    }


    public function setActiveItem($item)
    {
      $this->m_active = $item;
    }


    public function render()
    {
      return $this->m_CI->load->themeLayoutView($this->m_view, array("logo" => $this->m_logo, "items" => $this->m_items, "active" => $this->m_active), true);
    }
  }
