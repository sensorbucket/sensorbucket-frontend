<?php

  class Page_Meta
  {
    private static $m_instance = NULL;
    private $m_meta            = array();
    private $m_openGraph       = array();
    private $m_linkRelItems    = array();
    private $m_iconRelItems    = array();


    /**
     * Get Instance of Page_Meta
     *
     * @param bool|TRUE $mobile
     * @return Page_Meta
     */
    public static function getInstance()
    {
      if(!self::$m_instance OR is_null(self::$m_instance))
      {
        self::$m_instance = new Page_Meta();
      }
      return self::$m_instance;
    }


    private function __clone()
    {
    }


    public function __construct()
    {
      //$this->m_meta["charset"]     = "ISO-8859-15"; //"utf-8";
      $this->m_meta["title"]       = "";
      $this->m_meta["subtitle"]    = "";
      $this->m_meta["keywords"]    = "";
      $this->m_meta["description"] = "";
      $this->m_meta["robots"]      = "index, follow";
    }

    public function setTitle($title, $subTitle="")
    {
      $this->m_meta["title"]    = $title;
      $this->m_meta["subtitle"] = $subTitle;
    }


    public function getTitle()
    {
      return $this->m_meta["title"];
    }


    public function getSubTitle()
    {
      return $this->m_meta["subtitle"];
    }


    public function setRobots($index, $follow)
    {
      $this->m_meta["robots"] = "{$index}, {$follow}";
    }

    public function setKeywords($keywords)
    {
      $this->m_meta["keywords"] = $keywords;
    }

    public function setDescription($desc)
    {
      $this->m_meta["description"] = $desc;
    }

    
    public function getDescription()
    {
      return $this->m_meta["description"];
    }


    public function addItem($name, $content)
    {
      $this->m_meta[$name] = $content;
    }


    public function addOpenGraphItem($name, $content)
    {
      // http://ogp.me/

      $this->m_openGraph[$name] = $content;

      if ($name === "og:image")
      {
        $imgInfo = @getImageSize($content);

        if ($imgInfo !== false)
        {
          $this->m_openGraph["og:image:width"] = $imgInfo[0];
          $this->m_openGraph["og:image:height"] = $imgInfo[1];
        }
      }
    }


    public function addLinkRel($name, $value)
    {
      if (trim($value) !== "")
      {
        $this->m_linkRelItems[$name] = $value;
      }
    }

  
    public function addIconRel($type, $icon, $size="-")
    {
       $this->m_iconRelItems[$type][$size] = $icon;
    }


    public function getItem($name)
    {
      if (isset($this->m_meta[$name]))
      {
        return $this->m_meta[$name];
      }
      return "";
    }


    public function getMetaItems()
    {
      $code  = "<title>{$this->m_meta["title"]}</title>\n";

      foreach ($this->m_meta AS $name => $content)
      {
        $code .= "  <meta name='{$name}' content='{$content}'>\n";
      }

      foreach ($this->m_openGraph AS $name => $content)
      {
        $code .= "  <meta property='{$name}' content='{$content}'>\n";
      }

      foreach ($this->m_linkRelItems AS $name => $value)
      {
        $code .= '  <link rel="'.$name.'" href="'.$value.'" />'."\n";
      }

      foreach ($this->m_iconRelItems AS $type => $sizes)
      {
        foreach ($sizes AS $size => $icon)
        {
          $code .= '  <link rel="'.$type.'" sizes="'.$size.'" href="'.$icon.'" />'."\n";
        }
      }

      return $code;
    }

   
    public function getStructuredData()
    {
      $code  = '<script type="application/ld+json">'."\n";
      $code .= '{'."\n";
      $code .= '"@context": "http://schema.org/",'."\n";
      $code .= '"@type": "Product",'."\n";
      $code .= '"name": "'.$this->getTitle().'",'."\n";
      //$code .= '"image": "https://cdn.maritimephoto.com/image/web/2727/M645-021-11-olympic-dream.jpg",'."\n";
      $code .= '"description": "Maritimephoto.com is a small family based maritime photography team, established in 1959."'."\n";
      $code .= '}'."\n";
      $code .= '}'."\n";
      $code .= '</script>'."\n";
      return $code;
    }



  }
