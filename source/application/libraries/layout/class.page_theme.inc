<?php

/**
 *  Page
 *  Class for rendering a page
 *
 *
 */
 class Page_Theme
 {
    private static $instance;

    private $m_CI          = NULL;
    private $m_theme       = "";
    private $m_themeDir    = "";
    private $m_themeFile   = "";
    private $m_themeInfo   = array();
    private $m_themeModel  = false;
    private $m_drupalPath  = "";
    private $m_drupalSite  = "";
    private $m_defaultPage = "home";



   /**
    *  getInstance
    *
    *  Get the instance of this class
    *
    * @param void
    * @return Page
    *
    */
    public static function getInstance()
    {
      if (null === static::$instance)
      {
        static::$instance = new static();
      }

      return static::$instance;
    }


   /**
    *  private constructor
    *
    *  Init class
    *
    * @param void
    * @return void
    *
    */
    private function __construct()
    {
      $this->m_CI = &get_instance();
      $this->m_CI->load->config("sites");

      $host = "sensorbucket";

      //$host = getenv("SENSORBUCKET_HOST_NAME");

      //if ($host === false)
      // {
      //  $host = $_SERVER["HTTP_HOST"];
      // }

      $config = $this->m_CI->config->item("sites");

      if (isset($config[$host]))
      {
        $this->m_theme     = (isset($config[$host]["alias"])) ? $config[$host]["alias"]:"";
        $this->m_themeDir  = (isset($config[$host]["path"]))  ? $config[$host]["path"]:"";
        $this->m_themeFile = VIEWPATH.$this->m_themeDir."theme.inc.php";

        // Themeconfig
        if (is_file($this->m_themeFile))
        {
          $this->loadThemeConfig();
        }

        // Dedicated model for this theme
        $themeModel = APPPATH."models/Drupal_{$this->m_theme}_model.php";

        if (is_file($themeModel))
        {
          $this->m_themeModel = "Drupal_{$this->m_theme}_model";
        }

        // Drupal path for this site
        //$drupalBase         = Wombat_Config::getInstance()->getConfig("backend_root");
        //$this->m_drupalPath = $drupalBase."/sites/".$config[$host]["drupal_site"]."/";
        //$this->m_drupalSite = $config[$host]["drupal_site"];

        // Default page if no page was found on the url
        if (isset($config[$host]["default_page"]))
        {
          $this->m_defaultPage = $config[$host]["default_page"];
        }
      }
    }


   /**
    *  private clone
    *
    *  Disable cloning
    *
    * @param void
    * @return void
    *
    */
    private function __clone()
    {
    }


   /**
    *  loadThemeConfig
    *
    *  Load the theme config from the file
    *
    *  @param void
    *  @return void
    *
    */
    private function loadThemeConfig()
    {
      require($this->m_themeFile);

      if (isset($layout))
      {
        $this->m_themeInfo = $layout;
      }
    }


    public function getThemeInfo()
    {
      return array("theme_name"     => $this->m_theme, 
                   "theme_path"     => $this->m_themeDir,
                   "assets_path"    => "/assets/{$this->m_theme}/",
                   "theme_file"     => $this->m_themeFile, 
                   "theme_settings" => $this->m_themeInfo, 
                   "theme_model"    => $this->m_themeModel);
    }


    public function getThemeName()
    {
      return $this->m_theme;
    }


    public function getThemePath()
    {
      return $this->m_themeDir;
    }


    public function getThemeModel()
    {
      return $this->m_themeModel;
    }

    public function getDrupalSite()
    {
      return $this->m_drupalSite;
    }

    public function getDrupalSitePath()
    {
      return $this->m_drupalPath;
    }

    public function getDrupalImageThemeDir()
    {
      return "/assets/themes/{$this->m_theme}/d_images/";
    }


    public function getDefaultPage()
    {
      return $this->m_defaultPage;
    }


    public function getThemePageInfo($page)
    {
      if (isset($this->m_themeInfo["templates"][$page]))
      {
        return $this->m_themeInfo["templates"][$page];
      }
      return array();
    }

 }
