<?php

class Table_Generator
{
    private $m_headerFields;
    private $m_rows;
    private $m_tableType;
    private $m_htmlCode;
    private $m_captionFieldWidth;
    private $m_captionFieldClass;


    public function __construct()
    {
        $this->m_tableCaption      = "";
        $this->m_headerFields      = array();
        $this->m_rows              = array();
        $this->m_tableType         = "table table-bordered table-striped";
        $this->m_htmlCode          = "<div>\n";
        $this->m_captionFieldWidth = "";
        $this->m_captionFieldClass = "";
        $this->m_thClass           = "mp-table-header--grey";
        $this->m_id                = "tg_table";
        $this->m_name              = "tg_table";
        $this->m_incrementRowID    = true;
        $this->m_rowIncrementValue = 1;
        $this->m_searchOptions     = array("searchable" => false, "placeholder" => "Zoeken ...");
    }


    public function setIdAndName($id, $name)
    {
        $this->m_id   = $id;
        $this->m_name = $name;
    }


    public function setTableType($type)
    {
        $this->m_tableType = $type;
    }


    public function setSearchableOptions($searchOptions)
    {
        $this->m_searchOptions = $searchOptions;

        if (!isset($this->m_searchOptions["placeholder"]))
        {
            $this->m_searchOptions["placeholder"] = "Zoeken ...";
        }

        if (!isset($this->m_searchOptions["field"]))
        {
            $this->m_searchOptions["field"] = 0;
        }
    }


    public function setHeaderClass($class)
    {
        $this->m_thClass = $class;
    }


    public function setCaptionFieldSettings($width="", $class="")
    {
        $this->m_captionFieldWidth = $width;
        $this->m_captionFieldClass = $class;
    }

    public function setHeaderFields($fields)
    {
        $this->m_headerFields = $fields;
    }


    public function addFullRow($caption, $colspan=0, $type="active")
    {
        $id = "";

        if ($this->m_incrementRowID === true)
        {
            $id = " id='tr_{$this->m_rowIncrementValue}'";
            $this->m_rowIncrementValue++;
        }

        $colspan = ($colspan == 0) ? count($this->m_headerFields):$colspan;
        $html    = "<tr class='{$type}'{$id}>";
        $html   .= "<td colspan='{$colspan}'>";
        $html   .= $caption;
        $html   .= "</td>";
        $html   .= "</tr>";

        $this->m_rows[] = $html;
    }


    public function addTableCaption($caption, $colspan=0, $style="")
    {
        $colspan = ($colspan == 0) ? count($this->m_headerFields):$colspan;
        $html    = "<tr {$style} >";
        $html   .= "<td colspan='{$colspan}'>";
        $html   .= $caption;
        $html   .= "</td>";
        $html   .= "</tr>";

        $this->m_tableCaption = $html;
    }


    public function addDataRow($cols, $options="")
    {
        $id = "";

        if ($this->m_incrementRowID === true)
        {
            $id = " id='tr_{$this->m_rowIncrementValue}'";
            $this->m_rowIncrementValue++;
        }

        if ($options !== "")
        {
            $html = "<tr style='{$options}'{$id}>\n";
        }
        else
        {
            $html = "<tr{$id}>\n";
        }
        $nCol = 0;

        if ($this->m_captionFieldWidth !== "")
        {
            $td = "<td style='width:{$this->m_captionFieldWidth}'";
        }
        else
        {
            $td = "<td";
        }

        if ($this->m_captionFieldClass !== "")
        {
            $td .= " class='{$this->m_captionFieldClass}'>";
        }
        else
        {
            $td .= ">";
        }

        foreach ($cols AS $col)
        {
            $html .= "{$td}{$col}</td>\n";
            $td    = "<td>";
        }

        $html .= "</tr>\n";

        $this->m_rows[] = $html;
    }


    public function addDataRowByCode($html)
    {
        $this->m_rows[] = $html;
    }


    public function renderTable()
    {
        $id    = (trim($this->m_id) !== "")   ? " id='{$this->m_id}'":"";
        $name  = (trim($this->m_name) !== "") ? " name='{$this->m_name}'":"";
        $html  = $this->m_htmlCode;
        $html .= $this->renderSearchForm();
        $html .= "<table class='{$this->m_tableType}'{$id}{$name}>\n";
        $html .= $this->m_tableCaption;
        $html .= $this->renderHeader();
        $html .= $this->renderItems();
        $html .= "</table>\n";
        $html .= "</div>";

        return $html;
    }


    private function renderHeader()
    {
        if (count($this->m_headerFields) > 0)
        {
            $id    = (trim($this->m_id) !== "")   ? " id='thead_{$this->m_id}'":"";
            $html  = "<thead{$id}>\n";
            $html .= "<tr>\n";

            foreach ($this->m_headerFields AS $field)
            {
                $class = ($this->m_thClass !== "") ? " class='{$this->m_thClass}'":"";
                $html .= "<th{$class}>{$field}</th>\n";
            }

            $html .= "</tr>\n";
            $html .= "</thead>\n";
        }
        else
        {
            $html = "";
        }
        return $html;
    }


    public function renderItems()
    {
        $html = "";

        if (count($this->m_rows) > 0)
        {
            $id   = (trim($this->m_id) !== "")   ? " id='tbody_{$this->m_id}'":"";
            $html = "<tbody{$id}>\n".join("\n", $this->m_rows)."</tbody>\n";
        }

        return $html;
    }


    private function renderSearchForm()
    {
        $code = "";

        if (isset($this->m_searchOptions["searchable"]) && $this->m_searchOptions["searchable"] === true)
        {
            $code  = "<form id='table_search_{$this->m_id}'>";
            $code .= "<input type='text' id='table_search_field_{$this->m_id}' class='table-search-field form-control m-input text' onkeyup='filterTable(\"{$this->m_id}\", {$this->m_searchOptions["field"]})' placeholder='{$this->m_searchOptions["placeholder"]}'>";
            $code .= "<input type='submit' style='display:none;'>";
            $code .= "</form><br>";
        }

        return $code;
    }

}

class Table_DataRow
{
    private $m_html = "";

    public function __construct($options="")
    {
        $this->m_html = "<tr {$options}>\n";
    }

    public function addCol($caption, $options="")
    {
        $this->m_html .= "<td {$options}>{$caption}</td>\n";
    }

    public function getRow()
    {
        return $this->m_html."</tr>\n";
    }

}