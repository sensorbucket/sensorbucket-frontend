<?php

  class Admin_Actions
  {
    private $m_actions    = array();
    private $m_controller = "";
    private $m_icons      = array("view"   => array("title" => "Bekijken"   , "icon" => "fa-eye", "modal" => true),
                                  "edit"   => array("title" => "Aanpassen"  , "icon" => "fa-edit", "modal" => true), 
                                  "delete" => array("title" => "Verwijderen", "icon" => "fa-trash", "modal" => false));


    public function __construct($controller)
    {
      $this->m_controller = $controller;
    }

   
    public function addAction($action, $id="")
    {
      if (isset($this->m_icons[$action]))
      {
        if ($this->m_icons[$action]["modal"] === true)
        {
          $code = "<a class='action_href' title='{$this->m_icons[$action]["title"]}' href='javascript:loadExtendedDataIntoModal(\"/{$this->m_controller}/{$action}/{$id}\");'><span  class='fa {$this->m_icons[$action]["icon"]}'></span></a>";  
        }  
        else
        {
          $code = "<a class='action_href' title='{$this->m_icons[$action]["title"]}' href='/{$this->m_controller}/{$action}/{$id}'><span class='fa {$this->m_icons[$action]["icon"]}'></span></a>";
        }
      }

      $this->m_actions[] = $code;
    }


    public function render()
    {
      $code  = "<span class='actions nowrap'>";
      $code .= join("&nbsp;", $this->m_actions);
      $code .= "</span>";        

      $this->m_actions = array();
      return $code;
    }

  }