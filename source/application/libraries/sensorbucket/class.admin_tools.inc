<?php

  class Admin_Tools
  {

    public static function renderActions($actions)
    {
      $code  = "<span class='actions nowrap'>";
      $code .= join("&nbsp;", $actions);
      $code .= "</span>";

      return $code;
    }

    public static function renderDynamicModal($title, $content, $buttons=array())
    {
      $code  = "<div class='modal-header'>\n";
      $code .= "  <h5 class='modal-title' id='dyn_modal_extended_title'>{$title}</h5>\n";
      $code .= "</div>\n";
      $code .= "<div id='dyn_modal_body_extended' class='modal-body'>\n";
      $code .= $content."\n";
      $code .= "</div>\n";
      $code .= "<div id='dyn_modal_footer_extended' class='modal-footer'>\n";

      if (count($buttons) === 0)
      {
        $code .= "<button type='button' class='btn btn-primary' data-dismiss='modal'>Sluiten</button>\n";
      }
      else
      {
        $code .= join("", $buttons);
      }

      $code .= "</div>\n";

      return $code;
    }

  }