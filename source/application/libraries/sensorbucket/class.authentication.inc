<?php

  class Authentication
  {

     public static function validateCredentials($user, $password)
     {
       // Call the API to validate the credentials  
       $api  = sensorBucket_API::getInstance();
       $auth = true; //$api->authenticate($user, $password);
       $m    = array("u" => $user, "name" => $user, "jwt" => "");

       if ($auth !== false)
       {
         $m["jwt"] = $auth;
         self::setAuthCookie($user, $m);
         return true;
       }

       return false;
     }


     public static function getUser()
     {
       if (isset($_SESSION["sensorbucket"]))
       {
         $outerIP     = (isset($_SERVER["REMOTE_ADDR"])) ? md5($_SERVER["REMOTE_ADDR"]):"";
         $innerIP     = (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) ? md5($_SERVER["HTTP_X_FORWARDED_FOR"]):"";
         $agent       = md5(serialize(self::getBrowserConfig()));
         $check       = date("Ymd").crc32($outerIP.$innerIP.$agent);
         $sessionData = $_SESSION["sensorbucket"];

         if ($outerIP === $sessionData["outer_ip"] && $innerIP === $sessionData["inner_ip"] && $agent === $sessionData["agent"] && $check === $sessionData["token"])
         {
           return $_SESSION["sensorbucket"];  
         }
       }  

       return false;      
     }


     public static function getToken()
     {
       $user = self::getUser();

       if ($user !== false && isset($user["jwt"]))
       {
         return $user["jwt"];
       }

       return "";
     }


     public function updateToken($token)
     {

     }
     

     private static function setAuthCookie($user, $meta)
     {
       $outerIP    = md5($_SERVER["REMOTE_ADDR"]);
       $innerIP    = (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) ? md5($_SERVER["HTTP_X_FORWARDED_FOR"]):"";
       $agent      = md5(serialize(self::getBrowserConfig()));
       $check      = date("Ymd").crc32($outerIP.$innerIP.$agent);

       $_SESSION["sensorbucket"] = array("uid"       => md5($user), 
                                         "name"      => $meta["name"], 
                                         "outer_ip"  => $outerIP, 
                                         "inner_ip"  => $innerIP, 
                                         "agent"     => $agent, 
                                         "token"     => $check,
                                         "jwt"       => $meta["jwt"]);
       session_commit();
     }


     private static function getBrowserConfig()
     {
       if (isset($_SERVER["HTTP_USER_AGENT"]))
       {
         $browser['browser_name_pattern'] = $_SERVER["HTTP_USER_AGENT"];
         $browser['detected']             = true;
       }
       else
       {
         $browser = array('browser_name_pattern' => 'cli', 'platform' => 'unknown', 'detected' => false);
       }

       return $browser;
     }
     


  }
