<?php

  require_once(__DIR__."/class.sensorbucket_guzzle.inc");

  /**
   *  sensorBucket_API
   * 
   *  Guzzle based wrapped for sensorBucket API calls
   *
   *  @author Wim Kosten <w.kosten@zeeland.nl>
   *
   */
  class sensorBucket_API
  {
    private static $s_instance = NULL;
    private $m_apiClient       = NULL;
    private $m_bearerToken     = "";

   /**
    *  getInstance
    *
    *  Get an instance of this class (as it is a singleton)
    *  
    *  @param void
    *  @return object sensorBucket_API object
    * 
    */
    public static function getInstance()
    {
     if (static::$s_instance === NULL) 
     {
       static::$s_instance = new static();
     }

     return static::$s_instance;
    }


   /**
    *  constructor
    *
    *  Create new sensorbucket guzzle object
    *
    *  @param void
    *  @return void
    *
    */
    private function __construct()
    {
      $this->m_apiClient = new sensorBucket_Guzzle();
    }


   /**
    *  __clone
    *
    *  Disable __clone this object
    *
    *  @param void
    *  @return void
    *
    */
    private function __clone()  {}


   /**
    *  __wakeup
    *
    *  Disable __wakeup this object
    *
    *  @param void
    *  @return void
    */    
    private function __wakeup() {}


   /**
    *  authenticate
    *
    *  Authenticate a user using email / password
    *
    *  @param string $email
    *  @param string $password
    *  @return string
    *
    */
    public function authenticate($email, $password)
    {
      $this->m_bearerToken = $this->m_apiClient->authenticate($email, $password);
      return $this->m_bearerToken;  
    }


   /**
    *  getBearerToken()
    *
    *  Get the authentication token
    *
    *  @param void
    *  @return string
    *
    */
    public function getBearerToken()
    {
      return $this->m_bearerToken;
    }

   /**
    *  getHeaders
    *
    *  Get last HTTP response headers
    *
    *  @param void
    *  @return array
    *
    */
    public function getHeaders()
    {
      return $this->m_apiClient->getLastHeaders();
    }


   /**
    *  getLastError
    *
    *  Get last exception
    *
    *  @param void
    *  @return string
    *
    */
    public function getLastError()
    {
      return $this->m_apiClient->getLastError();
    }


   /**
    *  getOrganisations
    *
    *  Get list of known organisations
    *
    *  @param void
    *  @return mixed array with JSON objects or false if not exists
    *
    */
    public function getOrganisations()
    {
      return $this->m_apiClient->httpGet("api/v1/organisations");
    }    


   /**
    *  getOrganisationDetails
    *
    *  Get info about an organisation
    *
    *  @param integer $organisationID
    *  @return mixed JSON object or false if not exists
    *
    */
    public function getOrganisationDetails($organisationID)
    {
      return $this->m_apiClient->httpGet("api/v1/organisations/{$organisationID}");
    }


   /**
    *  addOrganisation
    *
    *  Add a new organisation
    *
    *  @param JSON $data JSON object with data
    *  @return boolean
    *
    */
    public function addOrganisation($data)
    {
      $result = $this->m_apiClient->httpPost("api/v1/organisations", $data, "POST");
    } 


   /**
    *  updateOrganisation
    *
    *  Update an existing organisation
    *
    *  @param integer $organisationID
    *  @param array $newData Array with data
    *  @return boolean
    *
    */    
    public function updateOrganisation($organisationID, $newData)
    {

    }


   /**
    *  deleteOrganisation
    *
    *  Delete an existing organisation
    *
    *  @param integer $organisationID
    *  @return boolean
    *
    */    
    public function deleteOrganisation($organisationID)
    {
      return $this->m_apiClient->httpDelete("api/v1/organisations/{$organisationID}");
    }    


  }