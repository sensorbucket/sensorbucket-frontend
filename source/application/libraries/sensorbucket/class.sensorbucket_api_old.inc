<?php

 /**
  *  Class Sensorbucket_API
  *
  *  Wrapper for sensorbucket API
  *
  *  @author  Wim Kosten (w.kosten@zeeland.nl>
  *
  */
  class Sensorbucket_API
  {
    //private $m_apiEndpoint = "http://cluster.multiflexmeter.net/api/v1/"; // http://sensorbucket.nl/v1/devices?expand=
    private $m_apiEndPoint = "http://sensorbucket.nl/v1/"; //devices?expand=source,location,deviceType"
    private $m_token       = "";
    private $m_http        = null;
    private $m_ready       = false;

    private static $instance;


    /**
     *  getInstance
     *
     *  Get the instance of this class
     *
     *  @param void
     *  @return Page
     *
     */
    public static function getInstance()
    {
      if (null === static::$instance)
      {
          static::$instance = new static();
      }

      return static::$instance;
    }


    /**
     *  private constructor
     *
     *  Init class
     *
     *  @param void
     *  @return void
     *
     */
    private function __construct()
    {
      try
      {
        $this->m_http = new HTTP_Service();
        $this->m_http->initialise();

        if ($this->m_http->is_available())
        {
          $this->m_http->setIncludeResponseHeaders(true);
          $this->m_http->setAgent('Sensorbucket_API');

          $this->m_ready = true;
          $this->m_token = authentication::getToken();
        }
      }
      catch (Exception $ex)
      {

      }
    }


    /**
     *  private clone
     *
     *  Disable cloning
     *
     *  @param void
     *  @return void
     *
     */
    private function __clone()
    {
    }


    /**
     *  getLocations
     *
     *  Get all locations
     *
     *  @param void
     *  @return array
     *
     */
    public function getLocations()
    {
      return $this->apiRequest("locations");
    }


    /**
     *  getSources
     *
     *  Get all sources
     *
     *  @param void
     *  @return array
     *
     */
    public function getSources()
    {
      return $this->apiRequest("sources?expand=sourceType");
    }


    /**
     *  getDevices
     *
     *  Get all devices
     *
     *  @param void
     *  @return array
     *
     */
    public function getDevices()
    {
      return $this->apiRequest("devices?expand=source,location,deviceType");
    }


    /**
     *  getDevice
     *
     *  Get data for a specific device (TODO: API does not yet support filtering oid)
     *
     *  @param integer $deviceID ID of the device
     *  @return array
     *
     */
    public function getDevice($deviceID)
    {
      return $this->apiRequest("devices/{$deviceID}?expand=source,location,deviceType");
    }


    /**
     *  addDevice
     *
     *  Add a new device
     *
     *  @param void
     *  @return array
     *
     */
    public function addDevice($postData)
    {
      $json = array("name"                 => $postData["device_name"],
                    "deviceType"           => $postData["device_type"],
                    "source"               => $postData["source_type"],
                    "location"             => $postData["location"],
                    "deviceConfiguration"  => array(),
                    "sourceConfiguration"  => array("dev_eui" => $postData["dev_eui"]));

      return $this->postRequest("devices", json_encode($json));
    }


    public function deleteDevice($deviceID)
    {
      return $this->postRequest("/devices/{$deviceID}", "", "DELETE");
    }


    /**
     *  getDeviceTypes
     *
     *  Get all deviceTypes
     *
     *  @param void
     *  @return array
     *
     */
    public function getDeviceTypes()
    {
      return $this->apiRequest("deviceTypes");
    }


    /**
     *  apiRequest
     *
     *  Wrapper for HTTP API requests / parsing the JSON data
     *
     *  @param string $request Request to do (without base url)
     *  @param mixed $default Default value to return if 404
     *  @return mixed
     *
     */
    private function apiRequest($request, $default=array())
    {
      $bearer = "bearer:".$this->m_token;
      $url     = $this->m_apiEndPoint.$request;
      $headers = array($bearer);
      $data    = $this->m_http->get($url, $headers);

      if ($this->m_http->getHttpResponseCode() === 200)
      {
        return $this->parseApiData($data);
      }

      return $default;
    }


    private function postRequest($request, $postData, $method="POST")
    {
      // "Content-Type: multipart/form-data"
      // "Content-Type: application/json"

      $bearer  = "bearer:".$this->m_token;
      $url     = $this->m_apiEndPoint.$request;   
      $headers = array($bearer, "Content-Type: application/json");

      $this->m_http->setPostMethod($method);
      $posted  = $this->m_http->post($url, $postData, true, $headers);
      $this->m_http->setPostMethod("POST");

      return $posted;
      return ((int)$this->m_http->getHttpResponseCode() === 200);
    }


    /**
     *  parseApiData
     *
     *  Parse the JSON data
     *
     *  @param object $data JSON object
     *  @param multiple $default
     *  @return any
     */
    private function parseApiData($data, $default=array())
    {
      try
      {
        return json_decode($data);
      }
      catch (Exception $ex)
      {
        return $default;
      }
    }

  }
