<?php

// Load Guzzle using the autoload
 require_once(APPPATH."/third_party/guzzle/vendor/autoload.php");

 // Load Guzzle parts
 use GuzzleHttp\Client;
 

/**
 *  sensorBucket_Guzzle
 *
 *  Wrapper around the Guzzle HTTP client
 *
 *  @author Wim Kosten <w.kosten@zeelandnl>
 *
 */
 class sensorBucket_Guzzle
 {
   private $m_client   = NULL;
   private $m_token    = "";
   private $m_headers  = array();
   private $m_error    = "";
   private $m_ready    = false;
   private $m_response = NULL;


   /**
    *  Constructor
    *
    *  Create Guzzle HTTP client
    *
    *  @param void
    *  @return void
    *
    */
   public function __construct()
   {
     try 
     {
       $domain         = CONFIG_BASE_DOMAIN;
       $this->m_client = new GuzzleHttp\Client(['base_uri' => $domain]);
       $this->m_ready  = true;
     }
     catch (Exception $ex)
     {
       $this->m_error = $ex->getMessage();
     }
   } 


   /**
    *  getLastError
    *
    *  Get the last (exception) error  
    *
    *  @param void
    *  @return string
    *
    */
   public function getLastError()
   {
     return $this->m_error;  
   }


   /**
    *  setBearerToken
    *
    *  Set the token as received from the authentication service
    *
    *  @param string $token
    *  @return void
    *
    */
   public function setBearerToken($token)
   {
     $this->m_token = $token;       
   }


   /**
    *  getLastHeaders
    *
    *  Get the HTTP headers from the last HTTP request
    *
    *  @param void
    *  @return array()
    *
    */
   public function getLastHeaders()
   {
     return $this->m_headers;
   } 


   /**
    *  getLastResponse
    *
    *  Get the HTTP response (Guzzle object) from the last HTTP request
    *
    *  @param void
    *  @return object
    *
    */
   public function getLastResponse()
   {
     return $this->m_response;
   } 


   /**
    *  authenticate
    *
    *  Authenticate a user with email/password at the authentication service
    *
    *  @param string $email
    *  @param string $password
    *  @return mixed JWT token (string) if succesful authenticated or false if not authenticated
    *
    */
   public function authenticate($email, $password)
   {
     $headers  = ['Content-Type' => 'application/json'];
     $jsonData = ['email' => $email, 'password' => $password];
     $url      = CONFIG_BASE_DOMAIN."auth";

     try
     {
       $response = $this->parseHttpResponse($this->m_client->post($url, ['headers' => $headers, 'json' => $jsonData])); 

       if (isset($response["content"]))
       {
         try
         {
           $json = json_decode($response["content"]);

           if (isset($json->token))
           {
             $this->setBearerToken($json->token);
             return $json->token;  
           }
         }  
         catch (Exception $ex)
         {
           $this->setBearerToken("");
           $this->m_error = $ex->getMessage();
         }
       }
     }
     catch (Exception $ex)
     {
       $this->setBearerToken("");
       $this->m_error = $ex->getMessage();
     }
        
     return false;
   }


   /**
    *  httpGet
    *
    *  Wrapper for Guzzle HTTP/GET requests
    *
    *  @param string $request (full url or url-part if using the base url)
    *  @param mixed $default Optional value to return if status was not 200
    *  @return mixed JSON object if HTTP/200 or false 
    *
    */
   public function httpGet($request, $default=array())
   {
     $token           = Authentication::getToken();
     $this->m_headers = array();

     try
     {
       $headers  = ['Authorization' => 'Bearer '.$token];
       $response = $this->m_client->request('GET', $request, ['headers' => $headers]);
       
       if ($response->getStatusCode() === 200)
       {
         $this->m_headers = $response->getHeaders();
         $responseBody    = $response->getBody();
         return $this->parseApiData($responseBody->getContents());
       } 
  
       return $default;
     }
     catch (Exception $ex)
     {
       $this->m_error = $ex->getMessage();
       return false;
     }
   } 


   /**
    *  httpDelete
    *
    *  Wrapper for Guzzle HTTP/DELETE requests
    *
    *  @param string $request Request to do (without base url)
    *  @return boolean
    *
    */    
   public function httpDelete($request)
   {
     $token           = Authentication::getToken();
     $this->m_headers = array();

     try
     {
       $headers         = ['Authorization' => 'Bearer '.$token];
       $response        = $this->m_client->request('DELETE', $request, ['headers' => $headers]);
       $this->m_headers = $response->getHeaders();

       return ($response->getStatusCode() === 200);
     }
     catch (Exception $ex)
     {
       $this->m_error = $ex->getMessage();
       return false;
     }
   }


   /**
    *  httpPost
    *
    *  Wrapper for Guzzle HTTP/POST, PUT & PATCH requests
    *
    *  @param string $url URL to POST to
    *  @param mixed $postData Data to send
    *  @param string $method Optional method if not POST
    *  @return mixed
    *
    */
   public function httpPost($url, $postData, $method="POST")
   {
     $token           = Authentication::getToken();
     $this->m_headers = array();

     try
     {
       $this->m_response = $this->m_client->request($method, $url, ['body' => $postData, 'headers' => ['Authorization' => 'Bearer '.$token, "Content-Type" => "application/json"]]);
       $this->m_headers  = $this->m_response->getHeaders();

       return ($this->m_response->getStatusCode() === 200);
     }
     catch (Exception $ex)
     {
       $this->m_error = $ex->getMessage();
       return false;
     }
   } 


   /**
    *  parseApiData
    *
    *  Parse the JSON data
    *
    *  @param object $data JSON object
    *  @param multiple $default
    *  @return any
    * 
    */
   private function parseApiData($data, $default=array())
   {
     try
     {
       return json_decode($data);
     }
     catch (Exception $ex)
     {
       return $default;
     }
   }


   /**
    *  parseApiData
    *
    *  Parse the JSON data
    *
    *  @param object $data JSON object
    *  @param multiple $default
    *  @return any
    * 
    */
   private function parseHttpResponse($response)
   {
     $validStatus    = array(200, 201);  
     $httpStatusCode = $response->getStatusCode();
     $httpBody       = $response->getBody();
     $httpHeaders    = $response->getHeaders();
     $contents       = (string)$httpBody;

     if (in_array($httpStatusCode, $validStatus))
     {
       return ['content' => $contents, 'status' => $httpStatusCode];  
     }

     return false;
   }


 }