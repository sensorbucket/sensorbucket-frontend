<?php

  require_once(dirname(__FILE__)."/class.service_interface.inc");

 /**
  *  HTTP_Service
  *
  *  cURL based service for doing a HTTP GET/POST requests
  *
  *  @author     Wim Kosten <w.kosten@zeeland.nl>
  *  @version    $Revision: 185 $
  *  $Id: class.http_service.inc 185 2012-05-04 11:24:29Z wim $
  * 
  */
  class HTTP_Service implements Service_Interface
  {
    private $m_curl               = NULL;
    private $m_agent              = "";
    private $m_cookie             = "";
    private $m_cookieJar          = "";
    private $m_responseHeaders    = false;
    private $m_receivedHeaders    = array();
    private $m_user               = "";
    private $m_pass               = "";
    private $m_auth               = NULL;
    private $m_response           = array();
    private $m_responseCode       = "";
    private $m_peerVerification   = 1;
    private $m_hostVerification   = 2;
    private $m_connTimeout        = 3;
    private $m_dataTimeout        = 3;
    private $m_hasTimeout         = false;
    private $m_defaultPostHeaders = array("Content-type:application/x-www-form-urlencoded");
    private $m_defaultPostMethod  = "POST";

   /**
    *  destructor
    *
    *  Destructor which destructs the cURL instance
    *
    *  @param void
    *  @param void
    *
    */     
    public function __destruct()
    {
      if (!is_null($this->m_curl) && $this->m_curl !== false)
      {
        curl_close($this->m_curl);
      }
    }



   /**
    *  is_available
    *
    *  Check if curl is initialised.
    *  
    *  @param void
    *  @return boolean
    *
    */
    public function is_available()
    {
      return (!is_null($this->m_curl) && $this->m_curl !== false);
    }


   /**
    *  initialise 
    *
    *  Initialise the curl instance 
    *
    *  @param void 
    *  @return void
    * 
    */
    public function initialise()
    {
      if ($this->m_curl == NULL)
      {
        $this->m_curl = curl_init();

        if ($this->m_curl === false)
        {
          $this->m_errors[] = "Error initialising cURL";
        }
      }
    }


   /**
    *  get
    *   
    *  Perform a HTT/GET on the specified url with optional HTTP authentication 
    *
    *  @param string $url Url to fetch
    *  @return string Contents as returned from the HTTP/GET
    * 
    */
    public function get($url, $headers = array(), $method="GET")
    {
      /* Url to fetch */
      curl_setopt($this->m_curl, CURLOPT_URL, $url);

      /* Get the result from the HTTP/GET */
      curl_setopt($this->m_curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->m_curl, CURLOPT_FOLLOWLOCATION, true); 

//      curl_setopt($this->m_curl, CURLOPT_SSLVERSION, 6);

      /* SSL verification */
      curl_setopt($this->m_curl, CURLOPT_SSL_VERIFYPEER, $this->m_peerVerification);
      curl_setopt($this->m_curl, CURLOPT_SSL_VERIFYHOST, $this->m_hostVerification);

      /* Use authentication? */
      if (!is_null($this->m_auth) && ($this->m_user != "" && $this->m_pass != ""))
      {
        curl_setopt($this->m_curl, CURLOPT_USERPWD, "{$this->m_user}:{$this->m_pass}");
        curl_setopt($this->m_curl, CURLOPT_HTTPAUTH, $this->m_auth); //CURLAUTH_BASIC);
      }

      /* Timeout */
      curl_setopt($this->m_curl, CURLOPT_TIMEOUT, $this->m_dataTimeout);
      curl_setopt($this->m_curl, CURLOPT_CONNECTTIMEOUT, $this->m_connTimeout);

      if ($this->m_agent !== "")
      {
        curl_setopt($this->m_curl, CURLOPT_USERAGENT, $this->m_agent);
      }

      if ($this->m_cookie !== "")
      {
        curl_setopt($this->m_curl, CURLOPT_COOKIE, $this->m_cookie);
      }

      if ($this->m_responseHeaders === true)
      {
        curl_setopt($this->m_curl, CURLOPT_HEADER, true);
      }

      if ($this->m_cookieJar !== "")
      {
        curl_setopt($this->m_curl, CURLOPT_COOKIEJAR, $this->m_cookieJar);
      }

      /* Set optional headers */
      if (count($headers) > 0)
      {
        curl_setopt($this->m_curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($this->m_curl, CURLOPT_HTTPHEADER, $headers);
      }

      /* Force HTTP/GET */
      curl_setopt($this->m_curl, CURLOPT_POST, false);

      /* Force IPv4 */
      if (defined('CURLOPT_IPRESOLVE') && defined('CURL_IPRESOLVE_V4'))
      {
        curl_setopt($this->m_curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
      }

      /* Reset the method if specified */
      if ($method != "GET")
      {
        curl_setopt($this->m_curl, CURLOPT_CUSTOMREQUEST, $method); 
      }

      /* No verbose */
      curl_setopt($this->m_curl, CURLOPT_VERBOSE, false);

      /* Get the result */ 
      $response             = curl_exec($this->m_curl);
      $this->m_response     = curl_getinfo($this->m_curl);
      $this->m_responseCode = $this->m_response["http_code"];

      /* Errors */
      $this->m_hasTimeout   = (curl_errno($this->m_curl) == 28);
      $this->m_errors[]     = array("code" => curl_errno($this->m_curl), "message" => curl_error($this->m_curl));

      if ($this->m_responseHeaders === false)
      {
        return $response;
      }
      else
      {
        $parts = explode("\r\n\r\n", $response);

        if (count($parts) === 2)
        {
          $this->parseHeaders($parts[0]); 
          return $parts[1];
        }
        else
        {
          // No \r\n\r\n delimited part so assume no headers
          return $response;
        }

      }
    }


   /** 
    *  post
    *   
    *  Perform a HTTP/POST (or PUT/PATCH) on the specified url
    *
    *  @param string $url Url to post to
    *  @param array $postVars Array (fieldname => value) with fields to post
    *  @param boolean $includeHeaders Boolean specifying if we want the response headers in the result
    *  @return string Contents as returned from the HTTP/GET
    * 
    */
    public function post($url, $postVars=array(), $includeHeaders=false, $headers=array())
    {
      /* Url to post to */
      curl_setopt($this->m_curl, CURLOPT_URL, $url);

      /* Get the result from the HTTP/POST */
      curl_setopt($this->m_curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->m_curl, CURLOPT_POST, true );

      /* No SSL verification */
      curl_setopt($this->m_curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($this->m_curl, CURLOPT_SSL_VERIFYPEER, false);

      /* Set the headers */ 
      curl_setopt($this->m_curl, CURLOPT_HEADER, $includeHeaders);
      curl_setopt($this->m_curl, CURLINFO_HEADER_OUT, true );

      if ($this->m_agent !== "")
      {
        curl_setopt($this->m_curl, CURLOPT_USERAGENT, $this->m_agent);
      }

      if ($this->m_cookie !== "")
      {
        curl_setopt($this->m_curl, CURLOPT_COOKIE, $this->m_cookie);
      }

      $postHeaders = array_merge($headers, $this->m_defaultPostHeaders);
      curl_setopt($this->m_curl, CURLOPT_HTTPHEADER, $postHeaders); //array( "Content-type:application/x-www-form-urlencoded"));
   
      /* Set the POST fields */
      //$postString = "&".http_build_query($postVars, '', '&');
      //curl_setopt($this->m_curl, CURLOPT_POSTFIELDS, $postString);
      curl_setopt($this->m_curl, CURLOPT_POSTFIELDS, $postVars);

      /* Reset the method if specified */
      if ($this->m_defaultPostMethod != "POST")
      {
        curl_setopt($this->m_curl, CURLOPT_CUSTOMREQUEST, $this->m_defaultMethod); 
      }

      /* Timeout */
      curl_setopt($this->m_curl, CURLOPT_TIMEOUT, $this->m_dataTimeout);
      curl_setopt($this->m_curl, CURLOPT_CONNECTTIMEOUT, $this->m_connTimeout);

      curl_setopt($this->m_curl, CURLOPT_VERBOSE, true);

      /* Get the result */
      $response             = curl_exec($this->m_curl);
      $this->m_response     = curl_getinfo($this->m_curl);
      $this->m_responseCode = $this->m_response["http_code"];

      /* Errors */
      $this->m_hasTimeout   = (curl_errno($this->m_curl) == 28);
      $this->m_errors[]     = array("code" => curl_errno($this->m_curl), "message" => curl_error($this->m_curl));

      return $response;
    } 


   /**
    *  setSslVerification
    *
    *  Enable/disable SSL peer/host verification (cURL specific)
    *  
    *  @param integer $peerVerification 0=disable/1=enable
    *  @param integer $hostVerification 0=disable/1=enable
    *  @return void
    * 
    */
    public function setSslVerification($peerVerification=0, $hostVerification=0)
    {
      $this->m_peerVerification = $peerVerification;
      $this->m_hostVerification = $hostVerification;
    }


   /**
    *  setTimeout
    *
    *  Set the timeout
    *
    *  @param integer $connection Connection timeout in seconds
    *  @param integer $data Data-transfer timeout in seconds
    *  @return void
    * 
    */
    public function setTimeout($connection=3, $data=3)
    {
      $this->m_connTimeout = $connection;
      $this->m_dataTimeout = $data;
    }

   
   /**
    *  hasTimeout
    *
    *  Check if a timeout has occurred (cURL specific)
    *
    *  @param void
    *  @return boolean True if timeout occurred
    * 
    */
    public function hasTimeout()
    {
      return $this->m_hasTimeout;
    }


   /**
    *  getHttpResponse 
    *
    *  Get the info from cURL about the HTTP/GET call 
    *
    *  @param void 
    *  @return array Array with info about the HTTP/GET
    * 
    */
    public function getHttpResponse()
    {
      $this->m_response['last_curl_error'] = curl_error($this->m_curl);
      return $this->m_response;
    }
   

   /**
    *  getHttpResponseCode
    * 
    *  Get the HTTP response code, eg 200 
    *
    *  @param void 
    *  @return integer HTTP response code
    * 
    */
    public function getHttpResponseCode()
    {
      return (INT)$this->m_response["http_code"];
    }


   /**
    *  setCredentials
    *   
    *  Set the username and password in case of a HTTp/GET using HTTP authentication
    *  
    *  @param string $user Username to use
    *  @param string $pass Password to use 
    *  @return void
    * 
    */
    public function setCredentials($user, $password)
    {
      $this->m_user = $user;
      $this->m_pass = $password;
    }


   /**
    *  setHttpAuthentication 
    *
    *  Set the HTTP authentication method
    *
    *  @param constant Authentication method to use (CURLAUTH_BASIC, CURLAUTH_DIGEST, CURLAUTH_GSSNEGOTIATE, CURLAUTH_NTLM, CURLAUTH_ANY, and CURLAUTH_ANYSAFE)
    *  @return void
    * 
    */
    public function setHttpAuthentication($curlAuthentication)
    {
      $this->m_auth = $curlAuthentication;
    }

 
    public function header_callback($curl, $header_line)
    {
      $this->m_receivedHeaders[] = $header_line;
      return strlen($header_line);
    }


    public function setAgent($agent)
    {
      $this->m_agent = $agent;
    }


    public function setCookie($cookie)
    {
      $this->m_cookie = $cookie;
    }


    public function setCookieJar($file)
    {
      $this->m_cookieJar = $file;
    }


    public function setIncludeResponseHeaders($include)
    {
      $this->m_responseHeaders = $include;
    }


    public function setMethod($method)
    {
      $this->m_defaultMethod = $method;
    }


    public function parseHeaders($headers, $delimiter="\r\n")
    {
      $this->m_receivedHeaders = explode($delimiter, $headers);
    }


    public function getResponseHeaders()
    {
      return $this->m_receivedHeaders;
    }


    public function parseCookies($headers)
    {
      $cookies = array();

      foreach ($headers AS $header)
      {
        if (stripos($header, "Set-Cookie:") !== false)
        {
          $cookieName = substr($header, 11);
          $stripos    = stripos($cookieName, ";");
          $cookies[]  = trim(substr($cookieName, 0, $stripos));
        }
      }

      return $cookies;
    }


    public function getCookies()
    {
      //$this->m_receivedHeaders
    }



   /**
    *  getErrors 
    * 
    *  Get the errors 
    *
    *  @param void 
    *  @return array Array with errors
    * 
    */
    public function getErrors()
    {
      return $this->m_errors;
    }


   /**
    *  clearErrors 
    * 
    *  Clean up the array containing the errors 
    *
    *  @param void 
    *  @return void
    * 
    */
    public function clearErrors()
    {
      $this->m_errors = array();
    }

  }
