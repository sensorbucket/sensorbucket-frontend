<?php
   
/**
 * The Service_Interface, implement this to access an external
 * service.
 *
 * @author Dennis Luitwieler <d.luitwieler@zeeland.nl>
 *
 * @version $Revision: 1832 $
 *
 * $Id: class.service_interface.inc 1832 2013-04-09 07:09:43Z dennis $
 */
interface Service_Interface
{    
  /**
   * Check the availability of the Service
   *
   * @return bool True if available, false otherwise
   */
  public function is_available();

  /**
   * Initialise the Service
   */
  public function initialise();

  /**
   * Get any errors of the Service
   *
   * @return array A list of errors.
   */
  public function getErrors();

  /**
   * Clear the current errors of the Service.
   */
  public function clearErrors();
}

