<?php

  class Simple_HTTP
  {

    public function __construct()
    {
    }


    public function get($url)
    {
      $meta = array("http_response" => 0, "error" => "");
      $data = "";

      try
      { 
        $stream = @fopen($url, 'r');
        $meta   = $this->parseMeta($stream);

        try
        {
          $data = @stream_get_contents($stream, -1, 0);
        }
        catch (Exception $ex)
        {
          $meta["error"] = $ex->getMessage();
        }

        fclose($stream);
      }
      catch (Exception $ex)
      {
        $meta["error"] = $ex->getMessage();
      }

      return array("metadata" => $meta, "response" => $data);
    }


    private function parseMeta($stream)
    {
      $meta = array();

      try
      {
        $meta                  = stream_get_meta_data($stream);
        $httpResponse          = explode(" ", $meta["wrapper_data"][0]);
        $meta["http_response"] = (int)end($httpResponse);
      }
      catch (Exception $ex)
      {
        $meta["error"] = $ex->getMessage();
      }
      return $meta;
    }
  }
