<?php

  /**
   *  Admin_model
   *
   *  Model for validating login's
   *
   *  @author	Wim Kosten <w.kosten@zeeland.nl>
   *
   */
  class Admin_model extends CI_Model
  {
      private $m_accounts;

      /**
       *  Public constructor
       *
       *  Init accounts
       *
       *  @param void
       *  @return void
       *
       */
      public function __construct()
      {
        // ol_admin#4711!
        parent::__construct();
        $this->getAccounts();
      }


      /**
       *  getAccounts
       *
       *  Read the accounts from the password file
       *
       *  @param void
       *  @return void
       *
       */
      private function getAccounts()
      {
        $this->m_accounts = array();
        $passFile         = AUTH_FILE; //"/data/www/kaarten/.passwd";
        
        if (is_file($passFile))
        {
          $accounts = file($passFile);
          
          foreach ($accounts AS $account)
          {
            $account = trim($account);
            
            if ($account !== "")
            { 
              list($user, $pass, $name) = explode("|", trim($account));
              $user                     = trim($user);
              $this->m_accounts[$user]  = array("name" => $name, "pass" => $pass);
            }  
          }
        }
      }


      /**
       *  isValidatedUser
       *
       *  Check if we have a user session / valid user if is session
       *
       *  @param void
       *  @return boolean
       *
       */
      public function isValidatedUser()
      {
        $user = $this->getUser();

        // Got a session ?
        if ($user === false)
        {
          return false;
        }
        
        // We got a session, check if the user actually exists
        if (!isset($this->m_accounts[$user["uid"]]))
        {
          return false;
        }
        
        return true;
      }


      /**
       *  getUser
       *
       *  Get the userdata from the session (if any)
       *
       *  @param void
       *  @return array|bool
       *
       */
      public function getUser()
      {
        $userData = $this->session->all_userdata();

        if (isset($userData["user"]))
        {
          return $userData["user"];
        }

        return false;
      }


      /**
       *  validateUser
       *
       *  Validate a user using posted uid/pwd
       *
       *  @param string $uid Userid
       *  @param string $pass Password
       *  @return bool
       *
       */
      public function validateUser($uid, $pwd)
      {
        $valid = false;
        
        if (isset($this->m_accounts[$uid]) && $this->m_accounts[$uid]["pass"] === md5($pwd))
        {
          $authUser = array("uid" => $uid, "name" => $this->m_accounts[$uid]["name"]);
          $valid    = true;
          $this->session->set_userdata("user", $authUser);
        }

        return $valid;
      }


      /**
       *  signOff
       *
       *  Destroy login session / logoff
       *
       *  @param void
       *  @return void
       *
       */
      public function signOff()
      {
        $this->session->unset_userdata('user');
        //$this->session->sess_destroy();
      }
  }