<?php

require APPPATH.'third_party/guzzle/vendor/autoload.php';
use GuzzleHttp\Client;


/**
 *  Sensorbucket_model
 * 
 *  Handle sensorbucket API requests
 * 
 */
class Sensorbucket_model extends CI_Model
{
    private $m_apiEndPoint = "http://sensorbucket.nl/v1/";
    private $m_token       = "";
    private $m_http        = NULL;
    private $m_ready       = false;
    private $m_error       = "";


    /**
     *  Constructor
     * 
     *  Init object, init HTTP and get JWT token
     * 
     *  @param void
     *  @return void
     */
    public function __construct()
    {
        parent::__construct();  

        // Init Guzzle and set base URI
        $this->m_client = new Client(['base_uri' => $this->m_apiEndPoint]); 

        // Get the JWT token (if any)
        $this->m_token = authentication::getToken();
    }


    /**
     *  getLastError
     *
     *  Get last (probably HTTP) error
     *
     *  @param void
     *  @return string
     *
     */    
    public function getLastError()
    {
      return $this->m_error;
    }


    /**
     *  getLocations
     *
     *  Get all locations
     *
     *  @param void
     *  @return array
     *
     */
    public function getLocations()
    {
      return $this->httpGET('locations', array());
    }


    /**
     *  getLocation
     *
     *  Get location configuration
     *
     *  @param integer $locationID
     *  @return array
     *
     */
    public function getLocation($locationID, $extendedData=false)
    {
      return $this->httpGET("locations/{$locationID}", array());
    }


    /**
     *  addLocation
     *
     *  Add a location
     *
     *  @param array $postData
     *  @return array
     *
     */
    public function addLocation($postData)
    {
      $json = array("name"      => $postData["location_name"],
                    "latitude"  => $postData["map_location_latlon_lat_field"],
                    "longitude" => $postData["map_location_latlon_lon_field"]);

      return $this->httpPost("locations", json_encode($json));
    }


    /**
     *  deleteLocation
     *
     *  Delete a location
     *
     *  @param integer $locationID
     *  @return array
     *
     */
    public function deleteLocation($locationID)
    {
      return $this->httpDelete("locations/{$locationID}");
    }


    /**
     *  getSources
     *
     *  Get all sources
     *
     *  @param void
     *  @return array
     *
     */
    public function getSources()
    {
      return $this->httpGET("sources?expand=sourceType", array());
    }


    /**
     *  getDevices
     *
     *  Get all devices
     *
     *  @param void
     *  @return array
     *
     */
    public function getDevices()
    {
      return $this->httpGET("devices?expand=source,deviceType,location", array());
    }


    /**
     *  getDevice
     *
     *  Get data for a specific device (TODO: API does not yet support filtering oid)
     *
     *  @param integer $deviceID ID of the device
     *  @return array
     *
     */
    public function getDevice($deviceID)
    {
      return $this->httpGET("devices/{$deviceID}?expand=source,location,deviceType", array());
    }


    /**
     *  getDevicesByLocation
     *
     *  Get all devices grouped by location
     *
     *  @param integer $deviceID ID of the device
     *  @return array
     *
     */    
    public function getDevicesByLocation($locationID=-1)
    {
      $locations = array();
      $devices   = $this->httpGET("devices?expand=source,deviceType,location", array());

      foreach ($devices AS $device)
      {
        $locID               = (isset($device->location->id)) ? $device->location->id:0;
        $locations[$locID][] = $device;
      }

      if ($locationID > 0)
      {
        if (isset($locations[$locationID]))
        {
          return $locations[$locationID];
        }
        else
        {
          return array();
        }
      }
      else
      {
        ksort($locations);
        return $locations;
      }
    }


    /**
     *  addDevice
     *
     *  Add a new device
     *
     *  @param array $postData
     *  @return array
     *
     */
    public function addDevice($postData)
    {
      $json = array("name"                 => $postData["device_name"],
                    "deviceType"           => $postData["device_type"],
                    "source"               => $postData["source_type"],
                    "location"             => $postData["location"],
                    "deviceConfiguration"  => array(),
                    "sourceConfiguration"  => array("dev_eui" => $postData["dev_eui"]));

      return $this->httpPost("devices", json_encode($json));
    }


    /**
     *  deleteDevice
     * 
     *  Delete a device
     * 
     *  @param integer $deviceID
     *  @return 
     * 
     */
    public function deleteDevice($deviceID)
    {
      return $this->httpDelete("devices/{$deviceID}");
    }


    /**
     *  getDeviceTypes
     *
     *  Get all deviceTypes
     *
     *  @param void
     *  @return array
     *
     */
    public function getDeviceTypes()
    {
      return $this->httpGET("deviceTypes", array());
    }


    /**
     *  getDeviceType
     *
     *  Get devicetype
     *
     *  @param integer $deviceID
     *  @return array
     *
     */
    public function getDeviceType($deviceID)
    {
      return $this->httpGET("deviceTypes/{$deviceID}", array());
    }


    public function addDeviceType($postData)
    {
      $json = array("name"                => $postData["device_name"],
                    "version"             => $postData["device_version"],
                    "configurationSchema" => $postData["device_configuration"]);

      return $this->httpPost("devicetypes", json_encode($json));
    }


    /**
     *  httpGet
     *
     *  Wrapper for Guzzle HTTP/GET requests
     *
     *  @param string $request Request to do (without base url)
     *  @param mixed $default Default value to return if 404
     *  @return mixed
     *
     */
    private function httpGET($request, $default=array())
    {
      try
      {
        $response = $this->m_client->request('GET', $request, ['headers' => ['X-Foo' => $this->m_token]]);

        if ($response->getStatusCode() === 200)
        {
          $body = $response->getBody();
          return $this->parseApiData($body->getContents());
        } 
  
        return $default;
      }
      catch (Exception $ex)
      {
        $this->m_error = $ex->getMessage();
        return false;
      }
    }


    /**
     *  httpDelete
     *
     *  Wrapper for Guzzle HTTP/DELETE requests
     *
     *  @param string $request Request to do (without base url)
     *  @return boolean
     *
     */    
    private function httpDelete($request)
    {
      try
      {
        $response = $this->m_client->request('DELETE', $request, ['headers' => ['X-Foo' => $this->m_token]]);
        return ($response->getStatusCode() === 200);
      }
      catch (Exception $ex)
      {
        $this->m_error = $ex->getMessage();
        return false;
      }
    }


    /**
     *  httpPost
     *
     *  Wrapper for Guzzle HTTP/POST, PUT & PATCH requests
     *
     *  @param string $url URL to POST to
     *  @param mixed $postData Data to send
     *  @param string $method Optional method if not POST
     *  @return mixed
     *
     */
    private function httpPost($url, $postData, $method="POST")
    {
      try
      {
        $response = $this->m_client->request('POST', $url, ['body' => $postData, 'headers' => ['X-Foo' => $this->m_token, "Content-Type" => "application/json"]]);
        return ($response->getStatusCode() === 200);
      }
      catch (Exception $ex)
      {
        $this->m_error = $ex->getMessage();
        return false;
      }
    } 


    /**
     *  parseApiData
     *
     *  Parse the JSON data
     *
     *  @param object $data JSON object
     *  @param multiple $default
     *  @return any
     * 
     */
    private function parseApiData($data, $default=array())
    {
        try
        {
            return json_decode($data);
        }
        catch (Exception $ex)
        {
            return $default;
        }
    }

}
