<?php

  // Include our interface
  require_once(APPPATH."/search_handlers/interface.base_search_handler.inc");

  class Search_Handler_Perceellijst Implements Base_Search_Handler
  {
      private $m_params;

      public function __construct()
      {
        $this->m_params = array();
      }


      public function setParams($params)
      {
        $this->m_params = $params;
      }


      public function fetch()
      {
        // Response data
        $response = array("response" => array("form"     => $this->m_params["form"],
                                              "item"     => $this->m_params["item"],
                                              "numFound" => 0,
                                              "start"    => 0,
                                              "maxScore" => 0,
                                              "html"     => "",
                                              "docs"     => array()));


        header("Content-Type:application/json");
        echo $this->asJSON($response);
      }


      public function asHTML($data)
      {
        $ci = &get_instance();
        return $ci->load->themeContentView("map_advanced_search_result_straat", array("data" => $data), true);
      }


      public function asJSON($data)
      {
        return json_encode($data);
      }


      public function was($item)
      {
        return html_escape($item);
      }

      private function getFromCache($plaats, $sectie, $perceel)
      {
          $list = Map_Metadata::getParcelCache();
          $data = array("params" => array("g" => "", "s" => "", "p" => ""), "gemeentes" => "", "sections" => array(), "parcels" => array(), "parcel" => array());

          if (trim($plaats) === "")
          {
              $data["gemeentes"] = array_keys($list);
          }
          else
          {
              $data["gemeentes"]   = array_keys($list);
              $data["params"]["g"] = $plaats;

              if (isset($list[$plaats]))
              {
                  $data["sections"] = array_keys($list[$plaats]);

                  if (trim($sectie) !== "" &&  $list[$plaats][$sectie])
                  {
                      $data["parcels"]     = array_values($list[$plaats][$sectie]);
                      $data["params"]["s"] = $sectie;

                      if (trim($perceel) !== "")
                      {
                          $url                 = "https://geodata.nationaalgeoregister.nl/locatieserver/v3/free?q=&wt=json&fq=type:perceel&rows=1&fq=kadastrale_gemeentenaam:{$plaats}&fq=kadastrale_sectie:{$sectie}&fq=perceelnummer:{$perceel}";
                          $data["params"]["p"] = $perceel;
                          $curl                = array();
                          $parcelInfo          = Map_Support::httpGetRequest($url, $curl);

                          if ((int)$curl["http_code"] === 200)
                          {
                              $json = json_decode($parcelInfo);

                              if ($json !== false)
                              {
                                  $data["parcel"] = $json->response->docs[0];
                              }
                          }
                      }
                  }
              }
          }
      }
  }
