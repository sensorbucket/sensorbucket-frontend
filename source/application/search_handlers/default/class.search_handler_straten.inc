<?php

  // Include our interface
  require_once(APPPATH."/search_handlers/interface.base_search_handler.inc");

  class Search_Handler_Straten Implements Base_Search_Handler
  {
      private $m_params;

      public function __construct()
      {
        $this->m_params = array();
      }


      public function setParams($params)
      {
        $this->m_params = $params;
      }


      public function fetch()
      {
        // Response data
        $response = array("response" => array("form"     => $this->m_params["form"],
                                              "item"     => $this->m_params["item"],
                                              "numFound" => 0,
                                              "start"    => 0,
                                              "maxScore" => 0,
                                              "html"     => "",
                                              "docs"     => array()));

        // Get the streetnames from our cache
        $list    = Map_Metadata::getRoadCache();
        $streets = $list["straten"];
        $found   = array();
        $count   = 0;

        // Loop the cacheitems
        foreach ($streets AS $streetName => $streetInfo)
        {
          if (stripos($streetName, $this->m_params["what"]) !== false)
          {
            foreach ($streetInfo AS $id => $info)
            {
              $gem    = $info["gemeente"];
              $plaats = $info["plaats"];
              $straat = $info["straat"];
              $found[$gem][$plaats][$straat] = $info;
              $count++;
            }
           }
        }

        // Sort by key
        ksort($found);

        // Set JSON object properties
        $response["response"]["numFound"] = $count;
        $response["response"]["docs"]     = $found;

        // If HTML format render template
        if ($this->m_params["form"] === 'html')
        {
          $data = array("docs" => $found, "count" => $count);
          $response["response"]["html"] = $this->asHTML($data);
        }

        header("Content-Type:application/json");
        echo $this->asJSON($response);
      }


      public function asHTML($data)
      {
        $ci = &get_instance();
        return $ci->load->themeContentView("map_advanced_search_result_straat", array("data" => $data), true);
      }


      public function asJSON($data)
      {
        return json_encode($data);
      }


      public function was($item)
      {
        return html_escape($item);
      }
  }
