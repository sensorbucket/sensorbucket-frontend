<?php

  // Include our interface
  require_once(APPPATH."/search_handlers/interface.base_search_handler.inc");

  class Search_Handler_Wegenlijst Implements Base_Search_Handler
  {
      private $m_params;

      public function __construct()
      {
        $this->m_params = array();
      }


      public function setParams($params)
      {
        $this->m_params = $params;
      }


      public function fetch()
      {
        // Response data
        $response = array("response" => array("form"     => $this->m_params["form"],
                                              "item"     => $this->m_params["item"],
                                              "numFound" => 0,
                                              "start"    => 0,
                                              "maxScore" => 0,
                                              "html"     => "",
                                              "docs"     => array()));


        header("Content-Type:application/json");
        echo $this->asJSON($response);
      }


      public function asHTML($data)
      {
        $ci = &get_instance();
        return $ci->load->themeContentView("map_advanced_search_result_straat", array("data" => $data), true);
      }


      public function asJSON($data)
      {
        return json_encode($data);
      }


      public function was($item)
      {
        return html_escape($item);
      }


      private function getFromCache($gemeente="", $plaats="", $weg="")
      {
          $list = Map_Metadata::getRoadCache();
          $data = array("params" => array("g" => "", "s" => "", "r" => "", "u"), "gemeentes" => "", "places" => array(), "roads" => array(), "road" => array());

          if (trim($gemeente) === "")
          {
              $gemeentes = array_keys($list["gemeentes"]);
              ksort($gemeentes);
              $data["gemeentes"] = $gemeentes;
          }
          else
          {
              $data["g"]         = $gemeente;
              $data["gemeentes"] = array_keys($list["gemeentes"]);

              if (isset($list["gemeentes"][$gemeente]))
              {
                  $data["places"] = array_keys($list["gemeentes"][$gemeente]);

                  if (trim($plaats) !== "")
                  {
                      $data["s"]     = $plaats;
                      $data["roads"] = array_flip($list["gemeentes"][$gemeente][$plaats]);

                      if (trim($weg) !== "")
                      {
                          $url                 = "https://geodata.nationaalgeoregister.nl/locatieserver/v3/free?q=&wt=json&fq=type:weg&fq=bron:NWB&fq=provincienaam:Zeeland&rows=1&start=0&fq=id:{$weg}";
                          $data["params"]["u"] = $url;
                          $data["params"]["r"] = $weg;
                          $curl                = array();
                          $roadInfo            = Map_Support::httpGetRequest($url, $curl);

                          if ((int)$curl["http_code"] === 200)
                          {
                              $json = json_decode($roadInfo);

                              if ($json !== false)
                              {
                                  $data["road"] = $json->response->docs[0];
                              }
                          }
                      }
                  }
              }
          }
      }
  }
