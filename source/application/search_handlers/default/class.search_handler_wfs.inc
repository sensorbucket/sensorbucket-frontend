<?php

  // Include our interface
  require_once(APPPATH."/search_handlers/interface.base_search_handler.inc");
  require_once(APPPATH."/libraries/map_tools/class.wfs_data.inc");


  class Search_Handler_WFS Implements Base_Search_Handler
  {
      private $m_params;

      public function __construct()
      {
        $this->m_params = array();
      }


      public function setParams($params)
      {
        $this->m_params = $params;
      }


      public function fetch()
      {
        // Response data
        $response = array("response" => array("form"     => $this->m_params["form"],
                                              "item"     => $this->m_params["item"],
                                              "numFound" => 0,
                                              "start"    => 0,
                                              "maxScore" => 0,
                                              "html"     => "",
                                              "docs"     => array()));

        // Parse the WFS params
        $params                       = urldecode($this->m_params["params"]);
        list($server, $layer, $field) = explode("|", $params);

        // Init, configure WFS object
        $curl = array();
        $wfs  = new WFS_Data($layer);
        $wfs->init();
        $wfs->setCount(10);
        $wfs->setRequestMethod("cql");
        $wfs->addFilter("strToLowerCase({$field})", strtolower($this->m_params["what"]));
        $url = $wfs->getUrl();

        $result = Map_Support::httpGetRequest($url, $curl);

        if ($result !== false)
        {
          try
          {
            $json                             = json_decode($result);
            $response["response"]["numFound"] = (int)$json->totalFeatures;
            $response["response"]["docs"]     = $json->features;

            if ($this->m_params["form"] === 'html')
            {
              // Check if this field has any field display settings
              require_once(APPPATH."/libraries/map_tools/class.map_featureinfo.inc");

              $servers     = Map_Metadata::getServers();
              $featureInfo = new map_featureInfo($layer, $servers[$server]["server"]."?");
              $fields      = $featureInfo->getFeatureMetadata();
              $settings    = Map_Support::getCheckedFeatureFields($layer);

              $data = array("docs" => $json->features, "settings" => $settings);
              $response["response"]["html"] = $this->asHTML($data);
            }
          }
          catch (Exception $ex)
          {

          }
        }

        header("Content-Type:application/json");
        echo $this->asJSON($response);
      }


      public function asHTML($data)
      {
        $ci = &get_instance();
        return $ci->load->themeContentView("map_advanced_search_result_wfs", array("data" => $data), true);
      }


      public function asJSON($data)
      {
        return json_encode($data);
      }


      public function was($item)
      {
        return html_escape($item);
      }
  }
