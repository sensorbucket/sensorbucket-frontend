<?php

  class Service_Search_Handler_PDOK
  {
    // https://github.com/PDOK/locatieserver/wiki/API-Locatieserver
    private $m_endPoint = "https://geodata.nationaalgeoregister.nl/locatieserver/v3/free?";
    private $m_lookup   = "https://geodata.nationaalgeoregister.nl/locatieserver/v3/lookup?";
    private $m_format   = "json";
    private $m_type     = "adres";
    private $m_sort     = "score%20desc";
    private $m_latLon   = "lat=51.5003&lon=3.6148";


    public function __construct()
    {
    }


    public function searchAddress($address, $numResults=15)
    {
      $address = str_replace("%20", "+", $address);
      $url     = $this->m_endPoint."q={$address}";
      $url    .= "&wt={$this->m_format}";
      $url    .= "&fq=type:{$this->m_type}";
      $url    .= "&sort={$this->m_sort}";
      $url    .= "&{$this->m_latLon}";
      //$url    .= "&fq=type:(gemeente%20OR%20woonplaats%20OR%20postcode%20OR%20adres)";
      $url    .= "&fq=provincienaam:Zeeland";
      $url    .= "&rows={$numResults}";
      $info    = array();
      $result  = Map_Support::httpGetRequest($url, $info);

      return $result;
    }


    public function lookupDetails($id)
    {
      // https://geodata.nationaalgeoregister.nl/locatieserver/v3/lookup?id=adr-1b37ff237749b88ea1eb9becfad52ddc
      $url    = $this->m_lookup."id={$id}";
      $info   = array();
      $result = Map_Support::httpGetRequest($url, $info);
      return $result;
    }


    public function searchPerceel($perceel, $numResults=15)
    {
      $perceel = str_replace("%20", "+", $perceel);
      $url     = $this->m_endPoint."q={$perceel}";
      $url    .= "&wt={$this->m_format}";
      $url    .= "&fq=type:perceel";
      $url    .= "&sort={$this->m_sort}";
      $url    .= "&{$this->m_latLon}";
      $url    .= "&rows={$numResults}";
      $info    = array();
      return Map_Support::httpGetRequest($url, $info);
    }


    public function reverseGeoCoding($x, $y, $rows=1, $type="adres")
    {
      // https://github.com/PDOK/locatieserver/wiki/API-Reverse-Geocoder
      $url    = "http://geodata.nationaalgeoregister.nl/locatieserver/revgeo?X={$x}&Y={$y}&rows={$rows}&type={$type}&distance=100&wt=json";
      $info   = array();
      $result = Map_Support::httpGetRequest($url, $info);

      if ($result === false)
      {
        $result = json_encode(array('response' => false));
      }

      echo $result;
    }


    public function getParcelsForCity($city)
    {
      /**
       *
       * PERCELEN:
       *
       *  Percelen in Middelburg
       *  https://geodata.nationaalgeoregister.nl/locatieserver/v3/free?q=&wt=json&fq=type:perceel&rows=10&fq=kadastrale_gemeentenaam:Middelburg
       *  https://geodata.nationaalgeoregister.nl/locatieserver/v3/free?q=&wt=json&fq=type:perceel&rows=10&fq=kadastrale_gemeentenaam:Veere&start=10&fl=kadastrale_sectie
       *
       *  // Percelen met sectie L in Veere
       *  https://geodata.nationaalgeoregister.nl/locatieserver/v3/free?q=&wt=json&fq=type:perceel&rows=10&fq=kadastrale_gemeentenaam:Veere&fq=kadastrale_sectie:L
       *  https://geodata.nationaalgeoregister.nl/locatieserver/v3/free?q=&wt=json&fq=type:perceel&rows=451&fq=kadastrale_gemeentenaam:Veere&fq=kadastrale_sectie:L&fl=perceelnummer
       *
       *  // Perceel info
       *  https://geodata.nationaalgeoregister.nl/locatieserver/v3/free?q=&wt=json&fq=type:perceel&rows=451&fq=kadastrale_gemeentenaam:Veere&fq=kadastrale_sectie:L&fq=perceelnummer:11
       *  https://geodata.nationaalgeoregister.nl/locatieserver/v3/lookup?id=pcl-f3780b0280ce6fe0bef8ea238f1a13f5
       *
       *
       *
       */

      $url   = "https://geodata.nationaalgeoregister.nl/locatieserver/v3/free?q=&wt=json&fq=type:perceel&rows=1&fq=kadastrale_gemeentenaam:{$city}&start=0&fl=kadastrale_sectie";
      $info  = array();
      $json  = Map_Support::httpGetRequest($url, $info);
      $items = array();
      $step  = 1000;

      if ((int)$info["http_code"] === 200)
      {
        $data = json_decode($json);

        if ($data !== false)
        {
          $amount = (int)$data->response->numFound;
          $pages  = ceil($amount/$step)-1;

          for ($i=0; $i<=$pages; $i++)
          {
            $start = ($i*$step);
            $info  = array();
            $url   = "https://geodata.nationaalgeoregister.nl/locatieserver/v3/free?q=&wt=json&fq=type:perceel&rows={$step}&fq=kadastrale_gemeentenaam:{$city}&start={$start}&fl=kadastrale_sectie&fl=perceelnummer";
            $json  = Map_Support::httpGetRequest($url, $info);

            if ((int)$info["http_code"] === 200)
            {
              $data = json_decode($json);

              if ($data !== false)
              {
                foreach ($data->response->docs AS $doc)
                {
                  $section = (string)$doc->kadastrale_sectie;
                  $parcel  = (string)$doc->perceelnummer;

                  if (!isset($items[$section]))
                  {
                    $items[$section] = array($parcel);
                  }
                  else
                  {
                    $items[$section][] = $parcel;
                  }
                }
              }
            }
          }
        }
      }

      return $items;
    }


    public function getRoadsFromWFS($locations=array())
    {
      //  http://geodata.nationaalgeoregister.nl/nwbwegen/wfs?service=wfs&version=2.0.0&request=GetFeature&typeName=wegvakken&propertyName=wvk_id,wegnummer,stt_naam,wpsnaamnen,gme_id,gme_naam&filter=<filter>
      $filter  = "<Filter><Or>";

      if (count($locations) === 0)
      {
        $locs = array("Terneuzen", "Kapelle", "Reimerswaal", "Sluis Z", "Schouwen-Duiveland", "Tholen",
                      "Goeree-Overflakkee", "Vlissingen", "Noord Beveland", "Woensdrecht", "Borsele",
                      "Hulst", "Veere", "Middelburg", "Goes", "Bergen op Zoom", "Steenbergen");
      }
      else
      {
        $locs = $locations;
      }

      foreach ($locs AS $loc)
      {
        $filter .= "<PropertyIsEqualTo><PropertyName>nwbwegen:gme_naam</PropertyName><Literal>{$loc}</Literal></PropertyIsEqualTo>";
      }

      $filter .= "</Or></Filter>";


      $properties = "&propertyName=wvk_id,wegnummer,stt_naam,wpsnaamnen,gme_id,gme_naam,wegbehsrt,geom";
      $format     = "outputFormat=application/json";
      $initUrl    = "https://geodata.nationaalgeoregister.nl/nwbwegen/wfs?service=wfs&version=2.0.0&request=GetFeature&typeName=wegvakken{$properties}&filter=";
      $initUrl   .= urlencode($filter)."&{$format}";
      $info       = array();
      $data       = Map_Support::httpGetRequest($initUrl, $info);
      $items      = array();
      $cache      = array();

      if ($data !== false)
      {
        $json = json_decode($data);

        foreach ($json->features AS $feature)
        {
          $id         = $feature->id;
          $wegNummer  = trim((string)$feature->properties->wegnummer);
          $straatNaam = trim((string)$feature->properties->stt_naam);
          $plaats     = trim((string)$feature->properties->wpsnaamnen);

          $cache["items"][$id]                             = (array)$feature->geometry->coordinates;
          $cache["wegnummers"][$wegNummer][$id]            = array($straatNaam, $plaats);
          $cache["straatnamen"][$plaats][$id]              = $straatNaam;
          $cache["hashtree"][$wegNummer][$straatNaam][$id] = $feature->properties;
        }

//        ksort($cache["wegnummers"]);
//        ksort($cache["straatnamen"]);
      }

      return $cache;
    }


    public function getSelectedRoadsFromWFS($roadNo, $streetName)
    {
      $filter  = "<Filter><And>";
      $filter .= "<PropertyIsEqualTo><PropertyName>nwbwegen:wegnummer</PropertyName><Literal>{$roadNo}</Literal></PropertyIsEqualTo>";
      $filter .= "<PropertyIsEqualTo><PropertyName>nwbwegen:stt_naam</PropertyName><Literal>{$streetName}</Literal></PropertyIsEqualTo>";
      $filter .= "</And></Filter>";
      $format  = "outputFormat=application/json";
      $url     = "https://geodata.nationaalgeoregister.nl/nwbwegen/wfs?service=wfs&version=2.0.0&request=GetFeature&typeName=wegvakken&filter=";
      //$url    .= urlencode($filter)."&{$format}";
      $url    .= $filter."&{$format}";
      return $url;
    }


    public function getSelectedRoad($id)
    {
      // &propertyName=wvk_id,geom
      $list  = explode("|", trim($id));
      $parts = array();

      foreach ($list AS $item)
      {
        $parts[] = "wegvakken.{$item}";
      }

      $url   = "https://geodata.nationaalgeoregister.nl/nwbwegen/wfs?service=wfs&version=2.0.0&request=GetFeature&typeName=wegvakken&outputFormat=application/json";
      $url  .= "&featureID=".join(",", $parts);
      $info  = array();
      $data  = Map_Support::httpGetRequest($url, $info);

      return array("url" => $url, "data" => $data);
    }


  }
