<?php

  Interface Base_Search_Handler
  {
     public function setParams($params);
     public function fetch();
     public function asHTML($data);
     public function asJSON($data);
     public function was($item);
  }
