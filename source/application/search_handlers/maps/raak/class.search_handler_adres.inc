<?php

  // Include our interface
  require_once(APPPATH."search_handlers/interface.base_search_handler.inc");
  require_once(APPPATH."libraries/map_tools/class.wfs_data.inc");
  require_once(APPPATH."search_handlers/default/class.service_search_handler_pdok.inc");


  class Search_Handler_Adres Implements Base_Search_Handler
  {  
     private $m_params;

     public function __construct()
     {
       $this->m_params = array();
     }

     public function setParams($params)
     {
       $this->m_params = $params;
     }

     public function fetch()
     {
       // Response data
       $response = array("response" => array("form"     => $this->m_params["form"],
                                             "item"     => $this->m_params["item"],
                                             "numFound" => 0,
                                             "start"    => 0,
                                             "maxScore" => 0,
                                             "html"     => "",
                                             "docs"     => array()));      
       // Require some authentication
       require_once(APPPATH."libraries/authentication/class.authentication.inc");

       if (authentication::getMapUser('raak'))
       {
         $pdok  = new Service_Search_Handler_PDOK();
         $adres = $this->m_params["what"];
         $data  = $pdok->searchAddress($adres);

         if ($data !== false)
         {
           $json = json_decode($data);

           if ($json !== false && (int)$json->response->numFound > 0)
           {
             $response["response"]["docs"]     = $json->response->docs;
             $response["response"]["numFound"] = (int)$json->response->numFound;
           }
         }
       }

       if ($this->m_params["form"] === 'html')
       {
         $response["response"]["html"] = $this->asHTML($json);
       }

       header("Content-Type:application/json");
       echo $this->asJSON($response);
     }


     public function asHTML($data)
     {
       $ci = &get_instance();
       return $ci->load->themeMapView("raak_search_address", array("data" => $data));
     }

     public function asJSON($data)
     {
       return json_encode($data);
     }


     public function was($item)
     {
       return html_escape($item);
     }
  }
