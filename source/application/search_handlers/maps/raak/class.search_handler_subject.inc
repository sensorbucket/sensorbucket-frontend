<?php

  // Include our interface
  require_once(APPPATH."/search_handlers/interface.base_search_handler.inc");
  require_once(APPPATH."/libraries/map_tools/class.wfs_data.inc");


  class Search_Handler_Subject Implements Base_Search_Handler
  {  
     private $m_params;

     public function __construct()
     {
       $this->m_params = array();
     }

     public function setParams($params)
     {
       $this->m_params = $params;
     }

     public function fetch()
     {
       // Response data
       $response = array("response" => array("form"     => $this->m_params["form"],
                                             "item"     => $this->m_params["item"],
                                             "numFound" => 0,
                                             "start"    => 0,
                                             "maxScore" => 0,
                                             "html"     => "",
                                             "docs"     => array()));

       if (trim($this->m_params["what"]) !== "")
       {
         // Require some authentication
         require_once(APPPATH."libraries/authentication/class.authentication.inc");

         if (authentication::getMapUser('raak'))
         {
           require_once(APPPATH.'/libraries/map_specific/class.raak_wfs_data.inc');

           $data                             = Raak_WFS_Data::getParcelsForOwner($this->m_params["what"], false);
           $response["response"]["numFound"] = count($data);;
           $response["response"]["docs"]     = $data;

           if ($this->m_params["form"] === 'html')
           {
             $response["response"]["html"] = $this->asHTML($data);
           }
         }
       }

       header("Content-Type:application/json");
       echo $this->asJSON($response);
     }


     public function asHTML($data)
     {
       $ci = &get_instance();
       return $ci->load->themeMapView("raak_search_sectie", array("data" => $data));
     }


     public function asJSON($data)
     {
       return json_encode($data);
     }


     public function was($item)
     {
       return html_escape($item);
     }
  }
