            <div class="panel-login-form-container panel-login text-center active">
               <div class="login-realm">
                <h3 class="login-realm-title"><?php echo $realm; ?></h3>
              </div>
              <div class="row">
                <div class="col-xs-12 col-sm-12">
                
                  <form name="pz_login_form" method="post" action="/auth/validate">
                    <input type="hidden" name="item" value="<?php echo $item; ?>"/>
                    <input type="hidden" name="map" value=""/>
                    
                    <div class="form-group">
                      <input type="text" class="form-control" name="username" placeholder="Gebruikersnaam">
                    </div>
                    
                    <div class="form-group">
                      <input type="password" class="form-control password" name="pass" placeholder="Wachtwoord">
                    </div>
                    
                    <div class="form-group form-submit">
                      <button name='login_submit' id='login_submit' class="btn btn-lg btn-primary btn-block" type="submit">Inloggen</button>
                    </div>
                    
                  </form>
                  
                </div>
              </div>
            </div>
