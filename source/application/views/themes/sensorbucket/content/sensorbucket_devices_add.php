<?php

 $deviceTypes = array();
 $sourceTypes = array();
 $locations   = array();

 foreach ($meta["dev_types"] AS $devType)  {$deviceTypes[$devType->id]    = $devType->name;}
 foreach ($meta["sources"] AS $sourceType) {$sourceTypes[$sourceType->id] = $sourceType->name;}
 foreach ($meta["locations"] AS $location) {$locations[$location->id]     = $location->name;}

 $form = new Form_Generator("sensorbucket_devices_add", "/submit/dyn_ajax_form");
 $form->addHiddenField("action", "sensorbucket_devices_add");
 $form->addTextField("device_name", "Naam", "", array("req" => true, "subclass" => "text"));
 $form->addSelectField("location", "Locatie", $locations, 1, array("req" => true, "subclass" => "select", "maxsize" => 400));
 $form->addSelectField("device_type", "Type", $deviceTypes, 1, array("req" => true, "subclass" => "select", "maxsize" => 400));
 $form->addSelectField("source_type", "Bron", $sourceTypes, 1, array("req" => true, "subclass" => "select", "maxsize" => 400));

 // TODO: make this dynamic depending on device / source config schemes
 $form->addRowDevider("dev1", "<hr>");
 $form->addTextField("dev_eui", "dev_eui", "", array("req" => true, "subclass" => "text", "maxsize" => 185));

 echo $form->renderForm(false);
?>
