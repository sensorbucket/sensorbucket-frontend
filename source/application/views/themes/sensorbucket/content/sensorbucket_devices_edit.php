<?php

 $deviceTypes = array();
 $sourceTypes = array();
 $locations   = array();

 foreach ($meta["dev_types"] AS $devType)  {$deviceTypes[$devType->id] = $devType->name;}
 foreach ($meta["sources"] AS $sourceType) {$sourceTypes[$sourceType->id] = $sourceType->name;}
 foreach ($meta["locations"] AS $location) {$locations[$location->id] = $location->name;}

 $form = new Form_Generator("sensorbucket_devices_edit", "/submit/dyn_ajax_form");
 $form->addHiddenField("device_id" , $data->id);
 $form->addHiddenField("action", "sensorbucket_devices_edit");
 $form->addTextField("device_name", "Naam", $data->name, array("req" => true, "subclass" => "text"));
 $form->addSelectField("location", "Locatie", $locations, (int)$data->location->id, array("req" => true, "subclass" => "select"));
 $form->addSelectField("device_type", "Type", $deviceTypes, (int)$data->deviceType->id, array("req" => true, "subclass" => "select"));
 $form->addSelectField("source_type", "Bron", $sourceTypes, (int)$data->source->id, array("req" => true, "subclass" => "select"));

 $config  = (array)$data->sourceConfiguration;
 $dev_eui = (isset($config['dev_eui'])) ? $config['dev_eui']:"";

 // TODO: make this dynamic depending on device / source config schemes
 $form->addRowDevider("dev1", "<hr>");
 $form->addTextField("dev_eui", "dev_eui", $dev_eui, array("req" => true, "subclass" => "text", "maxsize" => 200));

 echo $form->renderForm(); ?>
