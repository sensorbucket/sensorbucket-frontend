<?php

  $table = new Table_Generator();
  $table->setHeaderFields(array("Sensor ID", "Sensortype", "Versie", "Netwerk", "Locatie"));
  $table->setSearchableOptions(array("searchable" => true, "placeholder" => "Zoek op sensor ID", "field" => 0));

  foreach ($items AS $device)
  {
    $actions   = array();
    $actions[] = "<a class='action_href' title='Bekijken' href='javascript:loadExtendedDataIntoModal(\"/devices/view/{$device->id}\");'><span class='fas fa-eye'></span></a>";
    $actions[] = "<a class='action_href' title='Aanpassen' href='javascript:loadExtendedDataIntoModal(\"/devices/edit/{$device->id}\");'><span class='fas fa-edit'></span></a>";
    $actions[] = "<a class='action_href' title='Verwijder' href='/devices/delete/{$device->id}'><span class='fas fa-trash-alt'></span></a>";

    $actionHTML = Admin_Tools::renderActions($actions);

    $table->addDataRow(array($actionHTML." ".$device->name, $device->deviceType->name, $device->deviceType->version, $device->source->name, $device->location->name));
  }
?>

<div class="table-responsive">
    <?php echo $table->renderTable(); ?>
</div>