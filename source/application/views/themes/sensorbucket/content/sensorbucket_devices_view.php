<?php

  // Render the location
  $map = new OL_Map($data->location->latitude, $data->location->longitude);
  $map->addLocation($data->id, $data->location->latitude, $data->location->longitude, $data->deviceType->name, $data->name);

  // Render table for data
  $table = new Table_Generator();
  $table->setCaptionFieldSettings("25%", "caption");

  $table->addDataRow(array("Intern ID", $data->id));
  $table->addDataRow(array("Naam", $data->name));
  $table->addDataRow(array("Type", $data->deviceType->name));
  $table->addDataRow(array("Versie", $data->deviceType->version));
  $table->addDataRow(array("Locatie", $data->location->name));

  if (isset($data->deviceConfiguration) && !is_null($data->deviceConfiguration))
  {
    $table->addFullRow("Sensor configuratie", 2);
    $config = (array)$data->deviceConfiguration;

    foreach ($config AS $key => $value)
    {
      $table->addDataRow(array($key, $value));
    }
  }

  if (isset($data->sourceConfiguration) && !is_null($data->sourceConfiguration))
  {
    $table->addFullRow("Bron configuratie", 2);
    $config = (array)$data->sourceConfiguration;

    foreach ($config AS $key => $value)
    {
      $table->addDataRow(array($key, $value));
    }
  }

?>
<div id="map" style="height:200px; width:100%; border:1px solid black;"></div>
<!-- Note: the timeout is needed when using an OL map in a bootstrap modal -->
<script type='text/javascript'>setTimeout(() => {<?php echo $map->render(15) ?>}, 250);</script>
<hr>
<div class="table-responsive">
 <?php echo $table->renderTable(); ?>
</div>
