<?php

 $form = new Form_Generator("sensorbucket_devicetypes_add", "/submit/dyn_ajax_form");
 $form->addHiddenField("action", "sensorbucket_devicetypes_add");
 $form->addTextField("device_name", "Sensor naam", "", array("req" => true, "subclass" => "text"));
 $form->addTextField("device_version", "Versie", "",  array("req" => true, "subclass" => "select", "maxsize" => 100));
 $form->addTextArea("device_configuration", "Config (JSON)", "", array("req" => false, "rows" => 5));
 echo $form->renderForm(); 
?> 
