<?php

 $config = (is_null($data->configurationSchema)) ? "":$data->configurationSchema; 

 $form = new Form_Generator("sensorbucket_devicetypes_edit", "/submit/dyn_ajax_form");
 $form->addHiddenField("id", $data->id);
 $form->addHiddenField("action", "sensorbucket_devicetypes_edit");
 $form->addTextField("device_name", "Sensor naam", $data->name, array("req" => true, "subclass" => "text"));
 $form->addTextField("device_version", "Versie", $data->version,  array("req" => true, "subclass" => "select", "maxsize" => 100));
 $form->addTextArea("device_configuration", "Config (JSON)", $config, array("req" => false, "rows" => 5));
 echo $form->renderForm(); 

?> 
