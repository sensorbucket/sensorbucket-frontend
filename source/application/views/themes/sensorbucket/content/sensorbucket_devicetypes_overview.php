<?php

  $action = new Admin_Actions("devicetypes");
  $table  = new Table_Generator();
  $table->setHeaderFields(array("Sensortype", "Versie", "Configuratie"));
  $table->setSearchableOptions(array("searchable" => true, "placeholder" => "Zoek op sensortype", "field" => 0));

  foreach ($items AS $deviceType)
  {
    $action->addAction("view", $deviceType->id);
    $action->addAction("edit", $deviceType->id);
    //$action->addAction("delete", $deviceType->id);

    $config = (is_null($deviceType->configurationSchema)) ? "":"";
    $table->addDataRow(array($action->render()." ".$deviceType->name, $deviceType->version, $config));
  }
?>

<div class="table-responsive">
    <?php echo $table->renderTable(); ?>
</div>