<?php

  // $deviceType->name, $deviceType->version, $config
  // Render table for data
  $table = new Table_Generator();
  $table->setCaptionFieldSettings("25%", "caption");

  $table->addDataRow(array("Intern ID", $data->id));
  $table->addDataRow(array("Naam", $data->name));
  $table->addDataRow(array("Versie", $data->version));
  $table->addDataRow(array("Configuratie", $data->configurationSchema));

?>
<div class="table-responsive">
 <?php echo $table->renderTable(); ?>
</div>
