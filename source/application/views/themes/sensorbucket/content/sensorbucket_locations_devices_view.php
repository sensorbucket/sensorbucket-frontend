<?php

  $table = new Table_Generator();
  $table->setHeaderFields(array("Sensor ID", "Sensortype", "Versie", "Netwerk"));

  foreach ($devices AS $device)
  {
    $table->addDataRow(array($device->name, $device->deviceType->name, $device->deviceType->version, $device->source->name));
  }
?>

<div class="table-responsive">
    <?php echo $table->renderTable(); ?>
</div>