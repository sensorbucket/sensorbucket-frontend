<?php

 $form = new Form_Generator("sensorbucket_locations_edit", "/submit/dyn_ajax_form");
 $form->addHiddenField("action", "sensorbucket_locations_edit");
 $form->addHiddenField("owner", 0);
 $form->addHiddenField("id", $data->id);
 $form->addTextField("location_name", "Naam", $data->name, array("req" => true, "subclass" => "text"));
 $form->addLocationField("location_latlon", "Positie (lat/lon)", array("lat" => $data->latitude, "lon" => $data->longitude, "name" => $data->name, "callback" => "mapCallback"), array("req" => true));

 echo $form->renderForm(false);

?>
<script>
  function mapCallback(latlon, evt)
  {
      var lat = $('#map_location_latlon_lat');
      var lon = $('#map_location_latlon_lon');

      lat.text(latlon[1]);
      lon.text(latlon[0]);

      $('#map_location_latlon_lat_field').val(latlon[1]);
      $('#map_location_latlon_lon_field').val(latlon[0]);

      console.log('called mapCallback');
      console.log(latlon);
  }
</script>