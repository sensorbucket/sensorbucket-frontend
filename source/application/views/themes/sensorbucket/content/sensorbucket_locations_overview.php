<?php

  // Use ABIJ 6 as center
  $map = new OL_Map(51.5006462, 3.6152966);

  $action = new Admin_Actions("locations");
  $table  = new Table_Generator();
  $table->setHeaderFields(array("Naam", "Locatie", "# sensoren"));
  //$table->setSearchableOptions(array("searchable" => true, "placeholder" => "Zoek op Locatienaam", "field" => 0));

  foreach ($items AS $location)
  {
    $actions   = array();
    $count     = (isset($devices[$location->id])) ? count((array)$devices[$location->id]):0;
    $countHTML = "";

    $action->addAction("edit", $location->id);

    if ($count === 0)
    {
      $action->addAction("delete", $location->id);
    }
    else
    {
      $countHTML = "&nbsp;<a class='action_href' title='Sensorenlijst' href='javascript:loadExtendedDataIntoModal(\"/locations/deviceview/{$location->id}\");'><span class='fa fa-info'></span></a>";
    }

    $table->addDataRow(array($action->render()." ".$location->name, $location->latitude." / ".$location->longitude, $count.$countHTML));
    $map->addLocation($location->id, $location->latitude, $location->longitude, "location", $location->name);
  }
?>


<div id="map" style="height:400px; width:100%; border:1px solid black;"></div>
<script type='text/javascript'>setTimeout(() => {<?php echo $map->render(10) ?>}, 250);</script>
<hr>
<div class="table-responsive">
    <?php echo $table->renderTable(); ?>
</div>

