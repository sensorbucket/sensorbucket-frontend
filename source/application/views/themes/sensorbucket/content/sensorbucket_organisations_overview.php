<?php

 // Render the location
 //$map = new OL_Map($data->location->latitude, $data->location->longitude);
 $map = new OL_Map(51.5006462, 3.6152966);
 //$map->addLocation($data->id, $data->location->latitude, $data->location->longitude, $data->deviceType->name, $data->name);
?>
<?php

  // Use ABIJ 6 as center
  $map = new OL_Map(51.5006462, 3.6152966);

  $table = new Table_Generator();
  $table->setHeaderFields(array("Naam", "Adres", "Postcode / Plaats"));
  //$table->setSearchableOptions(array("searchable" => true, "placeholder" => "Zoek op Locatienaam", "field" => 0));

  foreach ($data AS $organisation)
  {
    $actions    = array();
    $actions[]  = "<a class='action_href' title='Aanpassen' href='javascript:loadExtendedDataIntoModal(\"/organisations/edit/{$organisation->id}\");'><span class='fas fa-edit'></span></a>";
    $actionHTML = Admin_Tools::renderActions($actions);
    $location   = $organisation->geom->coordinates;

    $table->addDataRow(array($actionHTML." ".$organisation->name, $organisation->address, $organisation->zipcode." ".$organisation->city));
    $map->addLocation($organisation->id, $location[0], $location[1], "location", $organisation->name);
  }
?>


<div id="map" style="height:400px; width:100%; border:1px solid black;"></div>
<script type='text/javascript'>setTimeout(() => {<?php echo $map->render(10) ?>}, 250);</script>
<hr>
<div class="table-responsive">
    <?php echo $table->renderTable(); ?>
</div>

