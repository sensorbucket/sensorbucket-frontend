<?php

  $table = new Table_Generator();
  $table->setHeaderFields(array("Naam", "Brontype"));
  $table->setSearchableOptions(array("searchable" => true, "placeholder" => "Zoek op netwerknaam", "field" => 0));

  foreach ($items AS $source)
  {
    $actions    = array();
    $actions[]  = "<a class='action_href' title='Bekijken' href='javascript:loadExtendedDataIntoModal(\"/sources/view/{$source->id}\");'><span class='fas fa-eye'></span></a>";
    $actions[]  = "<a class='action_href' title='Aanpassen' href='javascript:loadExtendedDataIntoModal(\"/sources/edit/{$source->id}\");'><span class='fas fa-edit'></span></a>";
    $actions[]  = "<a class='action_href' title='Verwijder' href=''><span class='fas fa-trash-alt'></span></a>";
    $actionHTML = Admin_Tools::renderActions($actions);
    $table->addDataRow(array($actionHTML." ".$source->name, $source->sourceType->name));
  }
?>

<div class="table-responsive">
    <?php echo $table->renderTable(); ?>
</div>
