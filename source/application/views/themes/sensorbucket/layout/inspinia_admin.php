<!DOCTYPE html>
<html>
<head>
    [HEAD]
    <meta name="theme" content="https://github.com/Chuibility/inspinia|https://chuibility.github.io/inspinia/dashboard_4_1.html">
    <link href="/loader/gcss/inspinia:bootstrap.min.css/inspinia:style.css/inspinia:prov-zeeland.css" rel="stylesheet">
    <link href="/assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />

    <!--  jquery-3.1.1.min.js/popper.min.js/bootstrap.js/plugins:metis:jquery.metisMenu.js/plugins:slimscroll:jquery.slimscroll.min.js/inspinia.js/plugins:pace:pace.min.js/plugins:jquery-ui:jquery-ui.min.js -->
    <script src="/loader/gjs/inspinia:inspinia_complete.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js" type="text/javascript"></script>    
    <script src="/loader/gjs/admin:shadowbox-base.js/admin:shadowbox.js/admin:admin.js/admin:dropzone.js/admin:forms.js/admin:fastclick.js/admin:jquery.blockui.js"></script>
</head>

<body>
    <div id="wrapper">
    <?php
      
       $menu = Page_Menu::getInstance();
       $html = $menu->render();
       
       echo $html;
    ?>
    
    <?php if (isset($regions["topnav"])) {echo $regions["topnav"];} ?>

        <div class="wrapper wrapper-content">
            <div class="row justify-content-md-center">
            <?php if (isset($regions["content"])) {echo $regions["content"];} ?>        
            </div>
        </div>

        <div class="footer">
            <div class="float-right">
                Provincie Zeeland 2020 [theme: Inspinia]
            </div>
        </div>

        </div>

        <div id="right-sidebar">
        </div>
    </div>
    
    [END_BODY]

<div class="modal fade" id="dyn_modal_extended" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-pz-lg" role="document">
        <form method='post' action='/submit_ajax/dyn_modal_form' id='dyn_modal_extended_form'>
            <div id="dyn_modal_content_extended" class="modal-content">
                Data laden ...
            </div>
        </form>
        <script>
            var ajaxForm = $('#dyn_modal_extended_form');

            function sleep(ms)
            {
              return(new Promise(function(resolve, reject) {setTimeout(function() { resolve(); }, ms);}));
            }


            ajaxForm.submit(function (e)
            {
                e.preventDefault();

                $.ajax({type    : ajaxForm.attr('method'),
                        url     : ajaxForm.attr('action'),
                        data    : ajaxForm.serialize(),
                        success : function (data)
                        {
                          console.log('Submission was successful.');
                          console.log(data);

                          if (data.form_error_message !== undefined || data.status === 'ERROR')
                          {
                            alert(data.form_error_message);
                          }
                          else
                          {
                            // $('#dyn_modal_extended').modal('hide');

                            if (data.post_eval !== undefined && data.post_eval !== "")
                            {
                              console.log('running eval on '+data.post_eval);
                              eval(data.post_eval);

                              sleep(1250).then(function() {$('#dyn_modal_extended').modal('hide');});
                            }
                            else
                            {
                              $('#dyn_modal_extended').modal('hide');
                            }
                          }
                        },
                        error   : function (data)
                        {
                          console.log('An error occurred.');
                          console.log(data);
                          $('#dyn_modal_extended').modal('hide');
                        },
                       });
            });
        </script>
    </div>
</div>

    <script>
        $(document).ready(function() 
        {

        });
    </script>
</body>
</html>
