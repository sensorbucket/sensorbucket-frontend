<!DOCTYPE html>
<html>
<head>
    [HEAD]
    <meta name="theme" content="https://github.com/Chuibility/inspinia|https://chuibility.github.io/inspinia/dashboard_4_1.html">
    <link href="/loader/gcss/inspinia:bootstrap.min.css/inspinia:style.css/inspinia:prov-zeeland.css" rel="stylesheet">
    <link href="/assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
</head>

<body>
    <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <img alt="image" class="rounded-circle" src="https://www.zeeland.nl/sites/zl-zeeland/themes/zeeland/images/logo-zeeland-kleur.svg"/>
                    </div>
                    <div class="logo-element">
                        PZ
                    </div>
                </li>
                <li class="active">
                    <a href=""><i class="fa fa-th-large"></i> <span class="nav-label">Kaarten</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li class="active"><a href="">Standaard kaarten</a></li>
                        <li><a href="">Storymaps</a></li>
                        <li><a href="">Statische kaarten</a></li>
                    </ul>
                </li>

                <li>
                    <a href=""><i class="fa fa-cogs"></i> <span class="nav-label">Configuratie</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="">Frontend gebruikers</a></li>
                        <li><a href="">Frameworks</a></li>
                        <li><a href="">WMS servers</a></li>
                        <li><a href="">Basislagen</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-zeeland-red " href="#"><i class="fa fa-bars"></i> </a>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a href="login.html">
                        <span class="fa fa-sign-out"></span> Log out
                    </a>
                </li>
            </ul>
        </nav>
        </div>


        <div class="wrapper wrapper-content">
        
        <div class="row justify-content-md-center">

        <div class="col-lg-12">
        <div class="ibox ">
        <div class="ibox-title">
            <h5>Overzicht kaarten</h5>
            <div class="ibox-tools">
                <button class="dropdown-toggle btn btn-sm btn-zeeland-blue btn-bitbucket">Kaart toevoegen</button>
            </div>
        </div>

        <div class="ibox-content">
        
            <div class="row">
                <div class="col-sm-9 m-b-xs">
                </div>
                <div class="col-sm-3">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control form-control-sm" placeholder="Zoeken op alias">
                        <div class="input-group-append">
                            <button class="btn btn-sm btn-zeeland-blue" type="button"><span class="fa fa-search"></span></button>
                        </div>
                    </div>
                </div>
            </div>
        
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Kaart</th>
                        <th>Gewijzigd</th>
                        <th>Lijstweergave</th>
                        <th>Toegang</th>
                        <th>Framework</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><span class="list-title">impuls_wonen</span><br><small>Gesubsidieerde projecten Provinciale Impuls Wonen</small></td>
                        <td>30-01-2020 15:41:27</td>
                        <td><span class="text-danger">Nee</span></td>
                        <td>Publiek</td>
                        <td><span class="badge badge-plain">4.6.5</span></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><span class="list-title">impuls_wonen</span><br><small>Gesubsidieerde projecten Provinciale Impuls Wonen</small></td>
                        <td>30-01-2020 15:41:27</td>
                        <td>Ja</td>
                        <td><span class="text-danger">Beperkt</span></td>
                        <td><span class="badge badge-plain">4.6.5</span></td>
                        <td></td>
                    </tr>
                    
                    </tbody>
                </table>
            </div>

        </div>
        </div>
        </div>

        </div>


        </div>


        <div class="footer">
            <div class="float-right">
                Provincie Zeeland 2020 [theme: Inspinia]
            </div>
        </div>

        </div>

        <div id="right-sidebar">
        </div>
    </div>

    <!--  jquery-3.1.1.min.js/popper.min.js/bootstrap.js/plugins:metis:jquery.metisMenu.js/plugins:slimscroll:jquery.slimscroll.min.js/inspinia.js/plugins:pace:pace.min.js/plugins:jquery-ui:jquery-ui.min.js -->
    <script src="/loader/gjs/inspinia:inspinia_complete.js"></script>
    
    [END_BODY]

    <script>
        $(document).ready(function() 
        {

        });
    </script>
</body>
</html>
