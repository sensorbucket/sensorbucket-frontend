<?php

  $menuItems = "";
  
  foreach ($items AS $id => $itemData)
  {
    $itemIsActive = ($id === $active[0]) ? " class='active'":"";
    
    $menuItems .= "<li{$itemIsActive}>\n";
    
    if (isset($itemData["items"]))
    {
      $menuItems .= "<a href='#'><span class='{$itemData["icon"]}'></span><span class='nav-label'>{$itemData["title"]}</span><span class='fa arrow'></span></a>\n";
      $menuItems .= "<ul class='nav nav-second-level' aria-expanded='true'>\n";
      
      foreach ($itemData["items"] AS $subID => $subItemData)
      {
        $subItemIsActive = (trim($subID) === trim($active[1])) ? " class='active'":"";
        $menuItems .= "<li{$subItemIsActive}><a href='{$subItemData["url"]}'>{$subItemData["title"]}</a></li>\n";
      }
      
      $menuItems .= "</ul>\n";
    }
    elseif (isset($itemData["href"]))
    {
      $menuItems .= "<a href='{$itemData["href"]}'><span class='{$itemData["icon"]}'></span><span class='nav-label'>{$itemData["title"]}</span></a>\n";
    }
    
    $menuItems .= "</li>\n\n";
  }

?>

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <?php if ($logo !== "") { ?>
                        <img alt="Logo" class="rounded-circle" src="<?php echo $logo; ?>"/>
                        <?php } ?>
                    </div>
                    <div class="logo-element"><img alt="Logo" class="rounded-circle" src="<?php echo $logo; ?>"/></div>
                </li>
                <?php echo $menuItems; ?>                
            </ul>
        </div>
    </nav>
