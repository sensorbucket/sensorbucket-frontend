<?php

 $logout = "";
  
 try
 {
   $user = Authentication::getUser();
   
   if (isset($user["uid"]) && isset($user["name"]))
   {
     $name   = strtolower($user["name"]);
     $logout = "<a href='/auth/logout/admin'><span class='fa fa-sign-out'></span> Logout ({$name})</a>";
   }
 }
 catch (Exception $ex)
 {
 
 }

?>
<div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-zeeland-red " href="#"><i class="fa fa-bars"></i> </a>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li><?php echo $logout; ?></li>
            </ul>
        </nav>
        </div>

