<!DOCTYPE html>
<html>
  <head>
  [HEAD]
  <link rel="stylesheet" href="/assets/css/inspinia/login.css">
  <link rel="stylesheet" href="/assets/fonts/font-awesome/css/font-awesome.min.css">
  </head>
  
  <body>
    <div class="container-fluid">
      <div class="row">
        
        <div class="panel-login-left col-lg-4 col-md-5 col-sm-7 col-xs-12">
          <div class="text-center pz-logo">
            <img src="https://kaarten.zeeland.nl/assets/images/logo-zeeland-kleur.svg" width="250" alt="Logo Provincie Zeeland">
          </div>
          <div class="panel-login-form">  <!-- authfy-login -->
          <?php if (isset($regions["content"])) {echo $regions["content"];} ?>
          </div>
        </div>
        
        <div class="panel-map-right col-lg-8 col-md-7 col-sm-5 hidden-xs">
        </div>
      </div>
    </div>
    
    <!-- Javascript Files -->
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="/assets/javascript/admin/bootstrap-3.3.7.js"></script>
  
  </body>    
</html>

