<?php

  $page                   = page::getInstance();
  $globalData             = $page->getGlobalPageData();
  $user                   = Authentication::getUser();
  $active                 = str_replace("/", "", $this->uri->slash_segment(1));
  $items                  = array();
  $menu                   = "";
  $items["dashboard"]     = array("title" => "Dashboard"     , "active" => false, "url" => "/dashboard");
  $items["organisations"] = array("title" => "Organisaties"  , "active" => false, "url" => "/organisations");
  $items["locations"]     = array("title" => "Locaties"      , "active" => false, "url" => "/locations/overview");
  $items["devicetypes"]   = array("title" => "Sensorsoorten" , "active" => false, "url" => "/devicetypes/overview");
  $items["sources"]       = array("title" => "Bronnen"       , "active" => false, "url" => "/sources/overview");
  $items["devices"]       = array("title" => "Sensoren"      , "active" => false, "url" => "/devices/overview");

  // <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">TITEL</h6>

  if (isset($items[$active]))
  {
    $items[$active]["active"] = true;
  }

  foreach ($items AS $alias => $itemData)
  {
    $active = ($itemData["active"] === true) ? " active":"";
    $menu  .= "<li class='nav-item'>";
    $menu  .= "<a class='nav-link{$active}' href='{$itemData["url"]}'>{$itemData["title"]}</a>";
    $menu  .= "</li>";
  }

?>
<!DOCTYPE html>
<html lang="en-GB">
<head>
    [HEAD]
  <!-- External stylesheets -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />  
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ol3/3.20.1/ol.css" />
  <!-- /External stylesheets -->

  <!-- local CSS -->
  <link rel="stylesheet" type="text/css" href="/loader/gcss/admin.css" />

  <!-- External JS files -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/ol3/3.20.1/ol.js"></script>
  <!-- /External JS files -->

</head>
<body>

<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="/">Sensorbucket</a>
<ul class="navbar-nav px-3">
    <li class="nav-item text-nowrap">
        <a class="nav-link" href="/auth/logout">Uitloggen (<?php echo $user["name"]; ?>)</a>
    </li>
</ul>
</nav>

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                    <?php echo $menu; ?>
                </ul>
            </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">

            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2"><?php if (isset($regions["title"])) {echo $regions["title"];} ?></h1>

                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        <?php
                        if (isset($globalData["page_actions"]) && count($globalData["page_actions"]) > 0)
                        {
                          foreach ($globalData["page_actions"] AS $pageAction)
                          {
                            echo "<button type='button' class='btn {$pageAction["button_type"]}' onclick='{$pageAction["action"]}'>{$pageAction["title"]}</button>\n";
                          }
                        }
                        ?>
                    </div>
                </div>

            </div>
            <?php if (isset($regions["content"])) {echo $regions["content"];} ?>
        </main>
    </div>
</div>

<!-- External javascript -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
<!-- /External javascript -->

<!-- Internal javascript -->
<script src="/loader/gjs/admin.js/tablesearch.js"></script>
<!-- /Internal javascript -->

<div class="modal fade" id="dyn_modal_extended" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method='post' action='/submit/dyn_modal_form' id='dyn_modal_extended_form'>
            <div id="dyn_modal_content_extended" class="modal-content">
            </div>
        </form>
        <script>
            var ajaxForm = $('#dyn_modal_extended_form');

            function sleep(ms)
            {
                return(new Promise(function(resolve, reject) {setTimeout(function() { resolve(); }, ms);}));
            }

            ajaxForm.submit(function (e)
            {
                e.preventDefault();

                $.ajax({type    : ajaxForm.attr('method'),
                    url     : ajaxForm.attr('action'),
                    data    : ajaxForm.serialize(),
                    success : function (data)
                    {
                        console.log('Submission was successful.');
                        console.log(data);

                        if (data.form_error_message !== undefined || data.status === 'ERROR')
                        {
                            alert(data.form_error_message);
                        }
                        else
                        {
                            //$('#dyn_modal_extended').modal('hide');

                            if (data.post_eval !== undefined && data.post_eval !== "")
                            {
                                console.log('running eval on '+data.post_eval);
                                eval(data.post_eval);
                            }

                            sleep(2000).then(function() {$('#dyn_modal_extended').modal('hide');});
                        }
                    },
                    error   : function (data)
                    {
                        console.log('An error occurred.');
                        console.log(data);
                        $('#dyn_modal_extended').modal('hide');
                    },
                });
            });

            var sideBarForm = $('#sidebar_form');

            sideBarForm.submit(function (e)
            {
                e.preventDefault();

                $.ajax({type    : sideBarForm.attr('method'),
                    url     : sideBarForm.attr('action'),
                    data    : sideBarForm.serialize(),
                    success : function (data)
                    {
                        console.log('Submission was successful.');
                        console.log(data);

                        if (data.form_error_message !== undefined || data.status === 'ERROR')
                        {
                            alert(data.form_error_message);
                        }
                        else
                        {
                            if (data.post_eval !== undefined && data.post_eval !== "")
                            {
                                console.log('running eval on '+data.post_eval);
                                eval(data.post_eval);
                            }
                        }
                    },
                    error   : function (data)
                    {
                        console.log('An error occurred.');
                        console.log(data);
//                    $('#dyn_modal_extended').modal('hide');
                    },
                });
            });
        </script>
    </div>
</div>


</body>
</html>
