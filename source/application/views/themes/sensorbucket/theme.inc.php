<?php

  /**
   *  theme
   *
   *  Define theme settings
   *
   *  @author    Wim Kosten <w.kosten@zeeland.nl>
   *  @copyright Provincie Zeeland
   *
   */

   // Init arrays
   $layout  = array();
   $regions = array();

   $adminV2CSS     = array();
   $adminV2Scripts = array("head" => array(), "endbody" => array());

   $mobileSettings = array();
   $icons          = array();

   // Icons path
   $favIcoPath = "[ASSETS_THEME_PATH]images/icons/";

   // Mobile settings
   $mobileSettings["viewport"]                              = "width=device-width, initial-scale=1.0, maximum-scale=1";
   $mobileSettings["apple-mobile-web-app-status-bar-style"] = "black";
   $mobileSettings["apple-mobile-web-app-capable"]          = "yes";
   $mobileSettings["msapplication-TileColor"]               = "#ffffff";
   //$mobileSettings["msapplication-TileImage"]               = "[ASSETS_THEME_PATH]images/icons/ms-icon-144x144.png";
   $mobileSettings["theme-color"]                           = "#ffffff"; 
   $layout["mobile"]                                        = $mobileSettings;   

   // Apple-touch-icon
   $_appleTouchIcons = array();
   $_appleTouchSizes = array("57x57", "60x60", "72x72", "76x76", "114x114", "120x120", "144x144", "152x152", "180x180");
   
   foreach ($_appleTouchSizes AS $aIconSize)
   {
     // <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
     $_appleTouchIcons[$aIconSize] = "{$favIcoPath}apple-icon-{$aIconSize}.png";
   }
   
   // Android icon
   // <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png"> 
   $androidIcon = array("192x192" => $favIcoPath."android-icon-192x192.png");
   
   // Favicons
   // <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png"> 
   $favIcons = array("32x32" => $favIcoPath."favicon-32x32.png",
                     "96x96" => $favIcoPath."favicon-96x96.png",
                     "16x16" => $favIcoPath."favicon-16x16.png");
   $favIcons = array();

   // Set icons
   $layout["icons"]["apple"]   = array(); //$_appleTouchIcons;
   $layout["icons"]["android"] = array(); //$androidIcon;
   $layout["icons"]["favicon"] = array(); //$favIcons;

   // Default metadata
   $layout["metadata"]["title"]       = "";
   $layout["metadata"]["description"] = "";
   $layout["metadata"]["keywords"]    = ""; 
   $layout["metadata"]["robots"]      = "index, follow";
   $layout["metadata"]["generator"]   = "";
   $layout["metadata"]["author"]      = "";
   
   // Set caching etc using the manifest param
   // $layout["manifest_file"] = "[ASSETS_THEME_PATH]theme_skel.appcache";

   $regions_login   = array("content"    => array("view" => ""                  , "data" => array(), "content_region" => true));
   $regions_content = array("topnav"     => array("view" => "topnav_content"    , "data" => array()),
                            "title"      => array("view" => ""                  , "data" => array()),
                            "menu"       => array("view" => ""                  , "data" => array()),
                            "content"    => array("view" => ""                  , "data" => array(), "content_region" => true),
                            "footer"     => array("view" => "footer_content"    , "data" => array()));

   $layout["templates"]["sensorbucket_login_page"] = array("regions" => $regions_login, "scripts" => $adminV2Scripts, "css" => $adminV2CSS);                         
   $layout["templates"]["sensorbucket_admin_page"] = array("regions" => $regions_content, "scripts" => $adminV2Scripts, "css" => $adminV2CSS);


   $regions_inspinia = array("topnav"    => array("view" => "inspinia_header", "data" => array()),
                             "title"     => array("view" => ""               , "data" => array()),
                             "content"   => array("view" => ""               , "data" => array(), "content_region" => true));                            
                             
   $regions_inspinia_login = array("topnav"    => array("view" => "", "data" => array()),
                                   "title"     => array("view" => "", "data" => array()),
                                   "content"   => array("view" => "", "data" => array(), "content_region" => true)); 

   $layout["templates"]["inspinia_admin"]       = array("regions" => $regions_inspinia, "scripts" => array("head" => array(), "endbody" => array()), "css" => array());
   $layout["templates"]["inspinia_login"]       = array("regions" => $regions_inspinia_login, "scripts" => array("head" => array(), "endbody" => array()), "css" => array());