// javascript:loadExtendedDataIntoModal(&quot;/calendar/item_view/1715&quot;);

function loadExtendedDataIntoModal(url)
{
    $.ajax({type    : "GET",
            url     : url,
            dataType: 'html',
            success: function (res)
            {
                // get the ajax response data
                // update modal content
                $("#dyn_modal_content_extended").html(res);

                // show modal
                $('#dyn_modal_extended').modal('show');
            },
            error: function (request, status, error)
            {
                alert("Fout tijdens het laden van de content");
                console.log("ajax call went wrong:" + request.responseText);
            }
        });
}
